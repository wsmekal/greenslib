#include "greens.h"

int main(void){
  int    l;              // momentum quantum number
  double E_T_nonrel, E,  // threshold energy and energy
         nonrel_wf;      // nonrelativistic result
spinor2_col rel_wf;      // and relativistic result

print("#Test of the Coulomb radial functions");

for (double Z = 1.0; Z<100.0; Z=Z+10.0){
greens_set_nuclear_charge(Z);          // set nuclear charge 
E_T_nonrel = -greens_energy(1);        // calculate nonrelativistic threshold
         l = 3;
	 E = E_T_nonrel * 10.0;
	 
  print("# r     nonrel_wf  rel_wf.LL");
  for (double r1=0.0; r1<120.0/Z; r1=r1+0.1/Z){
  nonrel_wf   = greens_radial_orbital(E, l, r1);
     rel_wf   = greens_radial_spinor (E, l, r1);
     
  printf("%E %E %E\n", r1, nonrel_wf, rel_wf.L); }}
return 0;}
