#include "greens.h"

int main(void){
double E, E_T,                        // 
       cs_nr_c, cs_nr_l,              // nonrelativistic results
       cs_r_c, cs_r_l;                // relativistic results

print("#Test of the two-photon ionisation cross sections");
warnlevel = NoPrint;  greens_set_Digits(6);

for(double Z=1.0; Z<93.0; Z=Z+91.0) {
  print();
  greens_set_nuclear_charge(Z);   // set nuclear charge 
  E_T = -greens_energy(1);        // calculate nonrelativistic threshold

  print("# E            cs_nr_c      cs_r_c       cs_nr_l      cs_r_l");

  for (int i=0; i<10; i++){
    E = E_T * 0.6 + (0.01 * i) * Z * Z;

     printf("%E ", E); fflush(stdout);
     cs_nr_c = greens_two_photon_cs("nonrelativistic","circular",E, 3);
     printf("%E ", cs_nr_c ); fflush(stdout); // * 1.896792e-50 to cgs
     cs_r_c  = greens_two_photon_cs("relativistic","circular",  E, 3);
     printf("%E ", cs_r_c); fflush(stdout);
     cs_nr_l = greens_two_photon_cs("nonrelativistic","linear", E, 3);
     printf("%E ", cs_nr_l); fflush(stdout);
     cs_r_l  = greens_two_photon_cs("relativistic","linear", E, 3);
     printf("%E\n", cs_r_l); fflush(stdout);  }}

     
return 0;}
