#include "greens.h"

int main(void){
int    n, l, kappa;              // quantum numbers
double r, rp, E, wf_nr, gf_nr;   // coordinates, energies, etc.
spinor2_col wf_r;                // relativistic spinor
matrix_2x2  gf_r;                // relativistic Green's matrix

print("#Test of the Coulomb radial functions");

for(double Z=1.0; Z<93.0; Z=Z+91.0) {
  print();
  greens_set_nuclear_charge(Z);        // set nuclear charge 
  E = -greens_energy(1) * 0.8;

  rp = 2.5/Z;  n = 4; l = 2; kappa = -3;
  write("# coord        wf_nr        wf_r.L "); 
  print("     gf_nr       gf_r.e[0][0]  gf_r.LL");
  for (r=0.0; r<25.0/Z; r=r+0.1/Z){
    wf_nr = greens_radial_orbital(n, l, r);
    wf_r  = greens_radial_spinor (n, l, r);
    gf_nr = greens_radial_function(-E, l, r, rp);
    gf_r  = greens_radial_matrix  (-E, l, r, rp);
     
  printf("%E %E %E %E %E %E\n", r, wf_nr, wf_r.L, 
                                gf_nr, gf_r.e[0][0], gf_r.LL); }}
return 0;}

//
//  LL, LS, SL, SS are the definitions, for instance: 
//  #define LL e[0][0]
//
