afile = greens

DA = ./*
DB = ./*/*
DC = ./*/*/*
DD = ./*/*/*/*
DE = ./*/*/*/*/*

default:
	make -C src/
	cp ./src/libgreens.a .

clear :
	$(RM) -R $(DA).so  $(DB).so  $(DC).so  $(DD).so  $(DE).so
	$(RM) -R $(DA).o   $(DB).o   $(DC).o   $(DD).o   $(DE).o
	$(RM) -R $(DA).zip $(DB).zip $(DC).zip $(DD).zip $(DE).zip
	$(RM) -R $(DA).out $(DB).out $(DC).out $(DD).out $(DE).out
	$(RM) -R $(DA).sum $(DB).sum $(DC).sum $(DD).sum $(DE).sum
	$(RM) -R $(DA).a $(DB).a $(DC).a $(DD).a $(DE).a
	$(RM) -R $(DA).tar $(DB).tar $(DC).tar $(DD).tar $(DE).tar	
	$(RM) -R man/html man/latex man/man
zip :
	cd .. && $(RM) $(afile).zip && zip -9r $(afile).zip $(afile)
	
from_a :
	if [test -f ../$(afile).zip]; then mv ../$(afile).zip ../$(afile).bak.zip; fi
	mcopy a:$(afile).zip ..
	unzip ../$(afile).zip

to_a : clear zip
	# insert floppy disk a: $(afile) to be saved
	
	ls ../$(afile).zip -l
	mcopy ../$(afile).zip a:

distribute_files = *.cpp *.hpp *.h *akefile*

tar :
	mkdir $(afile)
	mkdir $(afile)/src
	cd src/ && cp $(distribute_files) ../$(afile)/src/
	cp -r exam* $(afile)/
	#cp -r test* $(afile)/
	cp Makefile $(afile)/
	cp Read.me $(afile)/
	$(RM) -R $(afile)/exam*/a.out $(afile)/exam*/*.ex*
	$(RM) -R $(afile)/src/libgreens.a
	$(RM) -R $(afile)/test*/a.out $(afile)/test*/*.ex*
	tar -cvf $(afile).tar $(afile)/
	gzip $(afile).tar
	$(RM) -R $(afile)/
	ls -l $(afile).tar.gz 
