//
// CVECTOR with COMPLEX components
//
class CVECTOR 
{
  public : COMPLEX * e;
  public : size_t dim; 
  CVECTOR :: CVECTOR(size_t d){ 
  e = new COMPLEX[d];
  dim = d;}

  CVECTOR operator = (CVECTOR a) {
    if (this->dim!=a.dim) {
      fprintf(stderr, "wrong dimension \n"); fflush(stderr);
      if (errorbreak==Break) exit(1); return(*this);} 
    for(size_t i=0; i<this->dim; i++) this->e[i] = a.e[i];
    return(*this);}

  CVECTOR operator = (COMPLEX a) {
    for(size_t i=0; i<this->dim; i++) this->e[i] = a;
    return(*this);}

  CVECTOR operator = (REAL a) {
    for(size_t i=0; i<this->dim; i++) this->e[i] = a;
    return(*this);}

  CVECTOR operator = (VECTOR a) {
    if (this->dim!=a.dim) {
      fprintf(stderr, "wrong dimension \n"); fflush(stderr);
      if (errorbreak==Break) exit(1); return(*this);}  
    for(size_t i=0; i<this->dim; i++) this->e[i] = a.e[i];
    return(*this);}

};
//
// ----------------------- Operators -------------------------------------
//
bool operator ==(CVECTOR, CVECTOR);
bool operator ==(CVECTOR, COMPLEX);
bool operator ==(COMPLEX , CVECTOR);

bool operator ==(CVECTOR, VECTOR);
bool operator ==(VECTOR,  CVECTOR);
bool operator ==(REAL ,   CVECTOR);
bool operator ==(CVECTOR, REAL);
 
CVECTOR  operator +(CVECTOR, CVECTOR);
CVECTOR  operator +(CVECTOR, VECTOR);
CVECTOR  operator +(VECTOR, CVECTOR);

CVECTOR  operator +(COMPLEX, CVECTOR);
CVECTOR  operator +(REAL, CVECTOR);

CVECTOR  operator +(CVECTOR, COMPLEX);
CVECTOR  operator +(CVECTOR, REAL);

CVECTOR  operator -(CVECTOR, CVECTOR);
CVECTOR  operator -(VECTOR, CVECTOR);
CVECTOR  operator -(CVECTOR, VECTOR);

CVECTOR  operator -(COMPLEX, CVECTOR);
CVECTOR  operator -(REAL, CVECTOR);

CVECTOR  operator -(CVECTOR, COMPLEX);
CVECTOR  operator -(CVECTOR, REAL);

CVECTOR  operator -(CVECTOR a);

COMPLEX  operator *(CVECTOR a, CVECTOR b); // dotprod(a,b)
COMPLEX  operator *(CVECTOR a, VECTOR b); // dotprod(a,b)
COMPLEX  operator *(VECTOR a,  CVECTOR b); // dotprod(a,b)

CVECTOR  operator *(COMPLEX a, CVECTOR b);
CVECTOR  operator *(REAL a,    CVECTOR b);

CVECTOR  operator *(CVECTOR b, COMPLEX a);
CVECTOR  operator *(CVECTOR b, REAL a);

CVECTOR  operator /(COMPLEX a, CVECTOR b);
CVECTOR  operator /(REAL a, CVECTOR b);

CVECTOR  operator /(CVECTOR a, COMPLEX b);
CVECTOR  operator /(CVECTOR a, REAL b);

COMPLEX  dotprod (CVECTOR a, CVECTOR b); // dotprod(a,b)
COMPLEX  dotprod (CVECTOR a, VECTOR b); // dotprod(a,b)
COMPLEX  dotprod (VECTOR a,  CVECTOR b); // dotprod(a,b)

REAL     abs(CVECTOR a);
VECTOR   Re(CVECTOR a);
VECTOR   Im(CVECTOR a);
void     print(CVECTOR a);
