#include "greens_linewidthn.hpp"

//
// transitions probability A_{ki} from 
// Gordon W.F.Drake, Atomic & Molecule Opt Phys, Handbook, Chapter 10.
//[greens_A_ki]=1/sec. 
// Transition k -> i; greens_A_ki(2,1),  L_{\alpha} Lyman \alpha
// Transition k -> i; greens_A_ki(3,2),  H_{\alpha} Balmer \alpha

const double greens_transition_probability[3][5]=
{{4.699e00, 5.575e-1, 1.278e-1, 4.125e-2, 1.644e-2},
 {4.410e-1, 8.419e-2, 2.530e-2, 9.732e-3, 4.389e-3},
 {8.986e-2, 2.201e-2, 7.783e-3, 3.358e-3, 1.651e-3}};
//
//
double greens_A_ki(int k, int i)
{
  if ((i-1<0)||(i-1>2)||(k-i-1>4)||(k-i-1<0)) return(0.0);
  return(pow(greens_save_nuclear_charge, 4)*greens_transition_probability[i-1][k-i-1]*1.0e8);
}

//
// radiative lifetime tau_k 
// Gordon W.F.Drake, Atomic & Molecule Opt Phys, Handbook, Chapter 10.
// [tau_k]=sec
// greens_lifetime_H_non(k)=sum(greens_A_ki(k,i),i=1..k-1)
//
double greens_lifetime_H_non(int k)
{
  if (k<1) greens_ERROR(_str_greens_lifetime_H_non, _str_wrong_n);
  double s_A=0.0; last_derr=0.0; last_corr=true;
  for (long i=1; i<k; i++){s_A=s_A+greens_A_ki(k,i); last_derr=last_derr+1.0e-4;};
  
  if (s_A==0.0) {last_corr=false; last_derr=1.0; last_dres=1.0;} 
else {last_dres=1.0/s_A;}
  return(last_dres);
}

//
//
// width of energy level of Hydrogen (usually denoted as Gamma), atomic units  
// Gamma(k)=1/tau(k); [Gamma]=au
//
double greens_energy_width_H_non(int n)
{
 if (n<1) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_n);
 if (n==1) {last_dres=0.0; last_derr=0.0; last_corr=true;} 
 last_dres=1.0/(greens_lifetime_H_non(n)*4.1343e16);
 return(last_dres);
}

//
//
// width of energy level of Hydrogen (usually denoted as Gamma), atomic units  
// Gamma(n,l)=1/tau(n,l); [Gamma]=au
//
double greens_energy_width_H_non(int n, int l)
{
 if (n <  1) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_n);
 if (l <  0) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_l);
 if (n <= l) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_n_or_l);

 if (n == 1) {last_dres=0.0; last_derr=0.0; last_corr=true; return(last_dres);}
 if ((n == 4)&&(l == 1)) {last_dres=1.966834227679e-09; last_derr=1.0e-12; 
                          last_corr=true; return(last_dres);}
  
 double smd, sum; 
 
 sum=0.0;
 for (int n_f=n-1; n_f>0; n_f=n_f-1)
 { for(int l_f=0; l_f<n_f; l_f++)
   { for(int lambda = -1; lambda<2; lambda++)
     {
        smd=pow(greens_nonrel_bound_bound_me(n_f,l_f,lambda,lambda,n,l,0), 2.0);
        smd=smd*pow(greens_energy(n)-greens_energy(n_f), 3.0);
        sum=sum+smd;
   }}};
 last_dres=sum*4.0/3.0*greens_constant_powalpha3;
 return(last_dres);
}

//
//
// width of energy level of Hydrogen (usually denoted as Gamma), atomic units  
// Gamma(n,l)=1/tau(n,l); [Gamma]=au
// with numerical integration
//
double greens_energy_width_H_non(int n, int l, int digits)
{
 if (n <  1) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_n);
 if (l <  0) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_l);
 if (n <= l) greens_ERROR(_str_greens_energy_width_H_non, _str_wrong_n_or_l);

 if (n == 1) {last_dres=0.0; last_derr=0.0; last_corr=true; return(last_dres);}

 double smd, sum; 
 
 sum=0.0;
 for (int n_f=n-1; n_f>0; n_f=n_f-1)
 { for(int l_f=0; l_f<n_f; l_f++)
   { for(int lambda = -1; lambda<2; lambda++)
     {
        smd=pow(greens_nonrel_bound_bound_me(n_f,l_f,lambda,lambda,n,l,0,digits), 2.0);
        smd=smd*pow(greens_energy(n)-greens_energy(n_f), 3.0);
        sum=sum+smd;
   }}};
 last_dres=sum*4.0/3.0*greens_constant_powalpha3;
 return(last_dres);
}


//
// Transition probability by spontan decay 
// from (n2,l2) to (n1,l1) state (sum over m1 and average over m2)
// [A(n_f,l_f,n_i,l_i)] = 1/sec
//
double greens_A_n1l1n2l2(int n1, int l1, int n2, int l2)
{
  double E_p, coeff, sum, smd, abs_err, w3jw3j, mR;
  if (n2<1) greens_ERROR(_str_greens_A_n1l1n2l2, _str_wrong_n);
  if (n1<1) greens_ERROR(_str_greens_A_n1l1n2l2, _str_wrong_n);
  if (l1<0) greens_ERROR(_str_greens_A_n1l1n2l2, _str_wrong_l);
  if (l2<0) greens_ERROR(_str_greens_A_n1l1n2l2, _str_wrong_l);

  if (n1<=l1) greens_ERROR(_str_greens_A_n1l1n2l2, _str_wrong_n_or_l);
  if (n2<=l2) greens_ERROR(_str_greens_A_n1l1n2l2, _str_wrong_n_or_l);

  if (n2<n1) {greens_WARNING(_str_greens_A_n1l1n2l2, _str_final_state_lay_down); 
              return(0.0);}


  coeff=4.0*pow(greens_constant_cgs_e, 10)*greens_constant_cgs_me/
       (pow(greens_constant_cgs_hbar, 6)*pow(greens_constant_cgs_c, 3)*3);

  E_p=greens_energy(n2)-greens_energy(n1);
  
  sum=0.0; abs_err=0.0;
  for(int m_l=-1; m_l<2; m_l++)
  {

    w3jw3j=greens_w3j(l1,1,l2,0,0,0)*greens_w3j(l1,1,l2,-m_l,m_l,0);
    if (abs(w3jw3j)!=0.0) {mR=greens_nonrel_radial_bound_bound_me(n1,l1,n2,l2);} else mR=1.0;

    smd=(2*l1+1)*(2*l2+1)*pow(abs(w3jw3j*mR), 2);
    sum=sum+smd;
    abs_err=abs_err+last_derr*abs(smd);
  };

  last_dres=coeff*pow(E_p, 3)*sum; 
  if (sum==0.0) last_derr=greens_dexact; else last_derr=abs_err/abs(sum); 
  return(last_dres);

}

//
// stontan transition rate A_nl = sum_{nf,lf} A_{nf,lf,n,l} 
// [A_nl] = 1/sec
//
double greens_A_nl(int n, int l)
{
  double abs_err, sum, smd;

  if (n<1) greens_ERROR(_str_greens_A_nl, _str_wrong_n);
  if (n<2) greens_WARNING(_str_greens_A_nl, _str_wrong_n);
  if (l<0) greens_ERROR(_str_greens_A_nl, _str_wrong_l);
  if (n<=l) greens_ERROR(_str_greens_A_nl, _str_wrong_n_or_l);
  
  sum=0.0; abs_err=0.0;
  for(int nf=n-1; nf>0; nf=nf-1){
    for(int lf=0; lf<nf; lf++){
      smd=greens_A_n1l1n2l2(nf,lf,n,l);
      sum=sum+smd; abs_err=abs_err+last_derr*abs(smd);};}

  last_dres=sum; 
  if (sum==0.0) last_derr=greens_dexact; else last_derr=abs_err/abs(sum);  
  return(last_dres);
}

//
// radiative lifetime tau(n,l)=1/A_nl(n,l)
// [tau(n,l)] = sec
//
double greens_lifetime_H_non(int n, int l)
{

  if (n<1) greens_ERROR(_str_greens_lifetime_H_non, _str_wrong_n);
  if (n<2) greens_WARNING(_str_greens_lifetime_H_non, _str_wrong_n);
  if (l<0) greens_ERROR(_str_greens_lifetime_H_non, _str_wrong_l);
  if (n<=l) greens_ERROR(_str_greens_lifetime_H_non, _str_wrong_n_or_l);

  last_dres=1.0/greens_A_nl(n,l);  
  return(last_dres);
}

