#ifndef __GREENS_TPCSNL_HPP
#define __GREENS_TPCSNL_HPP

#include "greens_const.hpp"
#include "greens_service.hpp"
#include "greens_numint.hpp"
#include "greens_nonrelwavefunc.hpp"
#include "greens_nonrelgreenfunc.hpp"
/*!
 Two Photon Cross Section Nonrelativistic case
 Long-Wavelength approximation.
 The length form of matrix element is used.
 Polarization of light: both photons are right or
   both photons are left-polarised.

 [greens_TPCSN_2I_lw_circ]  = length^4 time (atomic Hartree units)   
*/
double greens_TPCSN_lw_circ(double E_p, int digits);

/*!
 Two Photon Cross Section Nonrelativistic case 2s initial state.
 Long-Wavelength approximation.
 The length form of matrix element is used.
 Polarization of light: both photons are right or
   both photons are left-polarised.

 [greens_TPCSN_2I_lw_circ]  = length^4 time (atomic Hartree units)   
*/

double greens_TPCSN_2s_lw_circ(double E_p, int digits);

/*!
 Two Photon Cross Section Nonrelativistic case
 Long-Wavelength approximation.
 The length form of matrix element is used.
 Polarization of light: both photons are linear polarized
 along z-axes.

 [greens_TPCSN_2I_lw_circ]  = cm^4 s    
*/
double greens_TPCSN_lw_lin(double E_p, int digits);

/*!
 So-called Weighted Two Photon Cross Section Nonrelativistic case
 Long-Wavelength approximation.
 The length form of matrix element is used.
 Polarization of light: both photons are right or
   both photons are left-polarised.

 [greens_TPCSN_2I_lw_circ] = cm^4 W^{-1}   
*/
double greens_TPCSN_2I_lw_circ(double E_p, int digits);

/*!
 So-called Weighted Two Photon Cross Section Nonrelativistic case
 Long-Wavelength approximation.
 The length form of matrix element is used.
 Polarization of light: both photons are linear polarized
 along z-axes.
 
 [greens_TPCSN_2I_lw_lin]  = cm^4 W^{-1}
*/
double greens_TPCSN_2I_lw_lin(double E_p, int digits);


/*!
 Two Photon Cross Section Nonrelativistic case
 Long-Wavelength approximation.
 The length form of matrix element is used.
 Polarization of light: both photons are right or
   both photons are left-polarised.

 atomic units for output
 [greens_TPCSN_2I_lw_circ]  = aul^4 aut = au(cm^4 s)
*/
double greens_TPCSN_lw_circ_au(double E_p, int digits);

#endif
