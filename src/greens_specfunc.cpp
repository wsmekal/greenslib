#include "greens_specfunc.hpp"
#define DebugKummerU
//
//
double pow_minus(int m) {if (m&1) {return(-1.0);} return(+1.0);}
double pow_minus(long m) {if (m&1) {return(-1.0);} return(+1.0);}
dcomplex pow_minus(double m) {return(pow(-1.0,dcomplex(m,0.0)));}
dcomplex pow_minus(dcomplex m) {return(pow((double)(-1.0),m));}

//
//--------------- double Psi function --------------------------------------------
//
#define SIZE_Psi 20
#define EDGE 15.0
const char *_str_Psi="Psi";

double dasympt_Psi[SIZE_Psi]=
{
 .8333333333333333333333333333333333333334e-1, 
-.8333333333333333333333333333333333333334e-2, 
 .3968253968253968253968253968253968253968e-2, 
-.4166666666666666666666666666666666666667e-2, 
 .7575757575757575757575757575757575757576e-2, 
-.2109279609279609279609279609279609279609e-1, 
 .8333333333333333333333333333333333333334e-1, 
-.4432598039215686274509803921568627450980, 
3.053954330270119743803954330270119743804, 
-26.45621212121212121212121212121212121212, 
 281.4601449275362318840579710144927536232, 
-3607.510546398046398046398046398046398046, 
 54827.58333333333333333333333333333333334, 
-974936.8238505747126436781609195402298851, 
 20052695.79668807894614346227249453055905, 
-472384867.7216299019607843137254901960784, 
 12635724795.91666666666666666666666666667, 
-380879311252.4536881155302207933786881155, 
 12850850499305.08333333333333333333333333, 
-482414483548501.7037158167036215816703622};
//
//
//
double Psi_p_rect(double z)
{ double z2k = z*z; 
  double smd = dasympt_Psi[0]/z2k; 
  double s   = 0.5/z+smd;
  int i = 2; 
  do { z2k=z2k*z*z; 
    smd=dasympt_Psi[i-1]/z2k; i=i+1; s=s+smd; 
    if (abs(smd/s)<double_exact) break;
  } while (i<20); return(log(z)-s);
}
//
//
//
double Psi_pos(double z)
{
  if (z<EDGE) {
    double smd = 1.0/(z+1.0); 
    double s   = 1.0/z+smd; int k = 2;
  do { smd=1.0/(z+k); 
       k=k+1; s=s+smd; 
  } while(k<EDGE); return(Psi_p_rect(z+EDGE)-s);} 
else {return(Psi_p_rect(z));};
}
//
//
//
double Psi(double z)
{
  if (greens_IsNonPosInteger(z)) greens_ERROR(_str_Psi, _str_wrong_args);
  if (z<0.0) return(Psi_pos(-z)-1.0/z-Pi/tan(Pi*z)); 
  return(Psi_pos(z));
}
//
//------------- dcomplex Psi -------------------------------------------------
//
dcomplex Psi_p_rect(dcomplex z)
{ dcomplex z2k = z*z; 
  dcomplex smd = dasympt_Psi[0]/z2k; dcomplex s = 0.5/z+smd;  
  int i = 2; 
  do { z2k=z2k*z*z; 
       smd=dasympt_Psi[i-1]/z2k; i=i+1; s=s+smd; 
       if (abs(smd/s)<double_exact) break;
  } while (i<20); return(log(z)-s);
}
//
//
//
dcomplex Psi_pos(dcomplex z)
{ 
  if (z.re<EDGE) {dcomplex smd = 1.0/(z+1.0); 
    dcomplex s = 1.0/z+smd; int k = 2;
    do{ smd=1.0/(z+k); k=k+1; s=s+smd; } while(k<EDGE); 
    return(Psi_p_rect(z+EDGE)-s);} 
else {return(Psi_p_rect(z));}
}
//
//
//
dcomplex Psi(dcomplex z)
{ 
  if (z.im==0.0) return(Psi(z.re));
  if (z.re<0.0) return(Psi_pos(-z)-1.0/z-Pi/tan(Pi*z)); 
  return(Psi_pos(z));
}

//
//
// ------------------------ GAMMA ----------------------------
//
#define SIZE_GAMMA 20

double dasympt_GAMMA[SIZE_GAMMA]=
{
 .8333333333333333333333333333333333333335e-1, 
-.2777777777777777777777777777777777777779e-2, 
 .7936507936507936507936507936507936507935e-3, 
-.5952380952380952380952380952380952380950e-3, 
 .8417508417508417508417508417508417508415e-3, 
-.1917526917526917526917526917526917526918e-2, 
 .6410256410256410256410256410256410256410e-2, 
-.2955065359477124183006535947712418300654e-1, 
 .1796443723688305731649384900158893966944, 
-1.392432216905901116427432216905901116428, 
 13.40286404416839199447895100069013112492, 
-156.8482846260020173063651324520889738281, 
 2193.103333333333333333333333333333333334, 
-36108.77125372498935717326521924223073648, 
 691472.2688513130671083952507756734675535, 
-15238221.53940741619228336495888678051866, 
 382900751.3914141414141414141414141414141, 
-10882266035.78439108901514916552510537473, 
 347320283765.0022522522522522522522522523, 
-12369602142269.27445425171034927132488108
};


const char* _str_GAMMA="GAMMA";
const char* _str_lngamma="lngamma";
//
//------------------- double GAMMA -----------------------------------
//
double lngamma_p_rect(double x)
{
  double smd; 
  double s   = (log(x)-1.0)*x+lnsqrtTwoPi-0.5*log(x);
  double xn  = x; 
     int i   = 0;
  do {
    smd=dasympt_GAMMA[i]/xn; xn=xn*(x*x); i++;
    s=s+smd; 
    if (abs(smd/s)<double_exact) break;
  } while(i<SIZE_GAMMA);
  return(s);
}
//
//
//
double lngamma_p_band(double x)
{
  if (x<EDGE) return(lngamma_p_band(x+1.0)-log(x));
  return(lngamma_p_rect(x));
}
//
//
//
double GAMMA(double x)
{
  if (greens_IsNonPosInteger(x)) {greens_ERROR(_str_GAMMA, _str_singularity);};

  if (x<0.0) {return((-Pi)/sin(Pi*x)/exp(lngamma_p_band(-x))/x);};
  return(exp(lngamma_p_band(x)));
}
//
// lngamma=ln(GAMMA(x)) <> Maple lnGAMMA() !!!!!
//
dcomplex lngamma(double x)
{
  if (greens_IsNonPosInteger(x)) {greens_ERROR(_str_lngamma, _str_singularity);};
  if (x<0) {return(log(dcomplex(-Pi/sin(Pi*x)/x, 0.0))-lngamma_p_band(-x));};
  return(lngamma_p_band(x));
}
//
//
//
dcomplex lnGAMMA(double x) {return(lngamma(x));}
//
// -------------------- dcomplex GAMMA ------------------------
//
dcomplex lngamma_p_rect(dcomplex x)
{
  dcomplex smd; dcomplex s = (log(x)-1.0)*x+lnsqrtTwoPi-0.5*log(x);
  dcomplex xn = x;  int i = 0;
  do { 
    smd=dasympt_GAMMA[i]/xn; 
    xn=xn*(x*x); i++; s=s+smd; if (abs(smd/s)<double_exact){break;};
  } while(i<SIZE_GAMMA);

return(s);
}
//
dcomplex lngamma_p_band(dcomplex x)
{
  if (x.re<EDGE) return(lngamma_p_band(x+1.0)-log(x));
  return(lngamma_p_rect(x));
}
//   
dcomplex GAMMA(dcomplex x)
{
  if (x.im==0.0) return(GAMMA(x.re));
  if (x.re<0.0) {return((-Pi)/sin(Pi*x)/exp(lngamma_p_band(-x))/x);};
  return(exp(lngamma_p_band(x)));
}

dcomplex lngamma(dcomplex x)
{
  if (x.im==0.0) return(lngamma(x.re));
  if (x.re<0) {return(log((-Pi)/sin(Pi*x)/x)-lngamma_p_band(-x));};
  return(lngamma_p_band(x));
}

dcomplex lnGAMMA(dcomplex x) {return(lngamma(x));}

//
const char *_str_factorial="factorial";
//
// ------------- int argument --------------
//
double factorial(int l)
{
  if (l<0) greens_ERROR(_str_factorial, _str_wrong_args);
  if ((l==0)||(l==1)) return(1); double res=1.0; 
  for(int i=2; i<=l; i++) res=res*i; 
  return(res);
}
//
// ------------- double argument ----------------
//
double factorial(double l){return(GAMMA(l + 1.0));}

//
// ------------- dcomplex argument -------------
//
dcomplex factorial(dcomplex l){return(GAMMA(l + 1.0));}

//
// factorial without checking
//
double _factorial(int l)
{
  if ((l==0)||(l==1)) return(1); 
  double res=1.0; 
  for(int i=2; i<=l; i++) res=res*i; 
  return(res);
}


/*! 
  Laguerre Polynom "double" precision
*/
double greens_L(long int n, double alpha, double z)
{
  if (n<0) greens_ERROR(_str_greens_L, _str_wrong_n);
 
  double smd = GAMMA(n+1.0+alpha)/GAMMA((double)(n+1.0))/GAMMA(alpha+1.0);
  double s   = smd;
  if (z==0.0) {return(smd);};
  for (long int m=0; m<=n; m++){
    smd = smd*(-n+m)*z/((alpha+m+1)*(m+1)); s=s+smd;}
  return(s);
}


bool limit_asymp;

/*!
  The function G(a,b,z) from "Theoretical Physic" Landau V 3. Appendix. 
  Asymptotic expansion of a hypergeometric function
  _2F_0(a,b;1/z)=hypergeom([a,b],[],1/z)
  (double) version
*/
double greens_G(double a, double b, double z)
{
  double smd1, g1, api, bpi; 
  double smd2 = 1.0; double s = 1.0; 
  
  for(long i=0; ;)  { 
    smd1=smd2; api=a+i; bpi=b+i; i=i+1;
    smd2=smd2*api*bpi/i/z; limit_asymp=abs(smd1)<abs(smd2);
    if ((api==0)||(bpi==0)) {last_corr=true; last_derr=greens_dexact; return(s);};
    if (limit_asymp) {
      if (i>2) {last_corr=true; last_derr=abs(smd1/s);} 
    else {last_corr=false; last_derr=1.0;} s=s-smd1; return(s);}
       s=s+smd2; g1=abs(smd2/s);  
    if (g1<greens_dexact){last_corr=true; last_derr=g1; return(s);}
  }
}

//
//---------- full dcomplex  G with Re(smd) limiting using when result approximately=1-----
//
dcomplex greens_G_rlim(dcomplex a, dcomplex b, dcomplex z)
{
  long i = 0; dcomplex smd1; dcomplex smd2 = 1.0; dcomplex s = 1.0;
  do
  { smd1=smd2; smd2=smd2*(a+i)*(b+i)/(i+1)/z; limit_asymp=abs(Re(smd1))<abs(Re(smd2));
    if (limit_asymp)
    { if (i>0) {last_corr=true; last_derr=abs(smd2/s); last_cdres=s; return(s); }
    else {last_corr=false; last_derr=1.0; last_cdres=s; return(s);}};

    i=i+1; s=s+smd2; last_derr=abs(smd2/s);
    if (last_derr<greens_dexact){last_corr=true; last_cdres=s; return(last_cdres);};
  } while(true);
}

//
//--------------------------- full dcomplex  G ---------------------------------------
//
dcomplex greens_G(dcomplex a, dcomplex b, dcomplex z)
{
  long i = 0; dcomplex smd1; dcomplex smd2 = 1.0; dcomplex s = 1.0;
  
  do
  { smd1=smd2; smd2=smd2*(a+i)*(b+i)/(i+1)/z; limit_asymp=abs(smd1)<abs(smd2);

    if (limit_asymp)
    { if (i==0){return(greens_G_rlim(a,b,z));}
    else {last_corr=true; last_derr=abs(smd2/s); last_cdres=s; return(s);};
    };
        
    i=i+1; s=s+smd2; last_derr=abs(smd2/s); 
    if (last_derr<greens_dexact){last_corr=true; last_cdres=s; return(last_cdres);};
  } while(true);
}

  double last_asymp_dres, last_asymp_derr; 
    bool last_asymp_corr;
dcomplex last_asymp_cdres;

// -------------- Confluent hypergeometric function M(a,b,z) or _1F_1(a,b,z)--------------
//
//-------------------- double KummerM as usual sum --------------------------
//
const char* _str_KummerM = "KummerM";
const char* _str_rKummerM = "rKummerM";

/*!
 The defined sum for KummerM function of double arguments
 check for available precision. maximal precision reachable with double double
*/
double KummerM_sum_max(double a, double b, double z)
{
  long n;
  double sn, ds, apn, bpn, fct, dsum, sum, ed, es, dz, dapn, dbpn, res_min_es, es_min; 
    
  sum = 1.0; sn = 1.0; ds = deviation(sn); 
  dsum = 0.0; dz = deviation(z); es_min = 1.0;
  n=0;
  for(;;){ 
    apn = (a+n); bpn = (b+n); fct = apn/bpn*z/(n+1);
    dbpn = deviation(bpn); dapn = deviation(apn);
    ds  = abs(ds*fct) + abs(sn/(n+1)/(bpn*bpn)*(abs(z*apn*dbpn)+abs(z*bpn*dapn) 
	                       + abs(apn*bpn*dz)));
    sn   = sn * fct; dsum = dsum + ds;  sum  = sum + sn;
    
    es = abs(sn/sum); ed = abs(dsum/sum);
    if ((greens_isnan(es)!=0 ) || (greens_isnan(ed)!=0 )) {sum=0.0; ed = 1.0; es = 1.0; break;}
     
    if (es<es_min) {
      res_min_es = sum; es_min=es; }; //    print *, ed, es, sum
    if (ed>es) break; n=n+1; 
   };  
  last_dres = sum; last_derr = max(es, ed);
  last_corr = last_derr < 0.1;
  if (!last_corr ) last_dres = res_min_es;
  return(last_dres);
}


//
// ---------- double KummerM as defined sum with Kummer transformation -
// maximum terms are summed
//
double KummerM_sum_Kt_max(double a, double b, double x)
{ 
  if (b-a<a) {last_dres=exp(x)*KummerM_sum_max(b-a, b, -x); }
else {last_dres=KummerM_sum_max(a, b, x);}

   return(last_dres);}

//
// -------------- double KummerM as asymptotic expansion -----------------------------------
// 
double KummerM_asymp(double a, double b, double z)
{
  
  greens_save_dexact=greens_dexact; 
  double g1, g2, DG1, DG2, c1, c2, s; bool br1, br2;
  limit_asymp=false; bool lim1, lim2;
  c1 = Re(exp(lngamma(b)-lngamma(b-a))*Cpow(-z, -a));
  c2 = Re(exp(lngamma(b)-lngamma(a))*exp(z)*Cpow(z, a-b));
  
  do{
    DG1=greens_G(a, a-b+1.0, -z); g1=last_derr; br1 = last_corr; lim1=limit_asymp;
    DG2=greens_G(b-a, 1.0-a,  z); g2=last_derr; br2 = last_corr; lim2=limit_asymp;
  
    s =c1*DG1+c2*DG2; g1=(abs(g1*c1*DG1)+abs(g2*c2*DG2))/abs(s);
    last_corr=(br1&&br2); greens_dexact=greens_dexact/10.0;
  } while((!(lim1||lim2))&&(g1>greens_save_dexact));
  greens_dexact=greens_save_dexact; 
    
  last_dres=s; last_asymp_dres=s;
  if (!last_corr) {last_derr=1.0;} else last_derr=g1;
  last_asymp_corr=last_corr; last_asymp_derr=last_derr;
  return(s);
}

//
// double KummerM ---
//
double KummerM_asymp_sum_Kt(double a, double b, double z)
{
  double res[3], err[3];
  bool corr[3]; int i, nm=0;

  last_dres=KummerM_asymp(a,b,z); if (last_derr<=greens_dexact) goto RETURN;
  res[nm]=last_dres; corr[nm] = last_corr; err[nm]=last_derr; nm=nm+1;
   
  last_dres=KummerM_sum_Kt_max(a,b,z); if (last_derr<=greens_dexact) goto RETURN; 
  res[nm]=last_dres; corr[nm] = last_corr; err[nm]=last_derr; nm=nm+1;

  for(i=0; i<nm; i++){ 
    last_corr=corr[i]; 
    if (last_corr) {last_dres=res[i]; last_derr=err[i]; break;}};
  //
  if (last_corr) {
    for(i=0; i<nm; i++){
      if ((last_derr>err[i])&&corr[i]) {
        last_derr=err[i]; last_dres=res[i];}}}
else {
    for(i=0; i<nm; i++){
      if (last_derr>err[i]) {
        last_derr=err[i]; last_dres=res[i];}}};
  
RETURN: return(last_dres);
}


/*
  This apropriate to the user routine calculates the
  confluent hypergeometric function _1F_1(a,b;z) = hypergeom([a],[b],z) = M(a,b,z) 
  double precision
*/
double KummerM(double a, double b, double z)
{     
  if (a==b) {last_derr=double_exact; last_dres=exp(z); return(last_dres);};
  if (z==0.0) {last_derr=double_exact; last_dres=1.0; return(last_dres);};
  if (greens_IsNonPosInteger(a)){ 
    if (greens_IsNonPosInteger(b)&&(b>a)) 
      greens_ERROR(_str_KummerM, _str_singularity);
    last_derr=greens_dexact; 
    last_dres = GAMMA(-a+1.0)*GAMMA(b)/GAMMA(b-a)*greens_L(-long(a),b-1.0,z);
    return(last_dres);}
else {
    if (greens_IsNonPosInteger(b)) greens_ERROR(_str_KummerM, _str_singularity);};

  last_dres=KummerM_asymp_sum_Kt(a,b,z); 
   
  if (warningbreak==Break)
  { print(_str_rKummerM); 
    write(a);        write(b);        writeln(z);
    write(last_dres); write(last_derr); writeln(last_corr);}  
  
  if (last_derr>greens_dexact) greens_WARNING(_str_rKummerM, _str_precision_not_reached);
  if (!last_corr) greens_WARNING(_str_rKummerM, _str_all_methods_wrong);

  return(last_dres);
}

int Max_cTerm    = 100;
int Max_cTerm_ra = 70;
const char* _str_cKummerM = "cKummerM";
/*!
  The function returns a complex number with modules of real and
  imaginary parts of a given complex number z
*/
dcomplex abs_reim(dcomplex z){return(dcomplex(abs(z.re),abs(z.im)));}

/*!
 The defined sum for KummerM function of dcomplex a,z and double b arguments
 check for available precision. maximal precision reachable with double
*/
dcomplex KummerM_sum_max(dcomplex a, double b, dcomplex z)
{
  long n;
  dcomplex sn, dsn, apn, bpn, fct, dsum, sum, 
           dz, dapn, dbpn, res_min_es; 
  double np1, dnp1, ed, es, es_min;
    
  sum  = 1.0; sn = 1.0; dsn = deviation(sn); 
  dsum = 0.0; dz = deviation(z); es_min = 1.0;
  n=0;
  for(;;){ 
    apn  = a+n; bpn = b+n; np1=n+1.0; fct = apn/bpn*z/np1;
    dbpn = deviation(bpn); dapn = deviation(apn); dnp1 = deviation(np1);

    dsn  = abs_reim(dsn * fct) + 
           abs_reim(sn  * dapn*bpn/(bpn*bpn)*z/np1)+
           abs_reim(sn  * dbpn*apn/(bpn*bpn)*z/np1)+
           abs_reim(sn  * apn/bpn*dz*np1/(np1*np1))+
           abs_reim(sn  * apn/bpn*z*dnp1/(np1*np1));

    sn   =  sn * fct;
    dsum = dsum + dsn;  sum  = sum + sn;

    es = max(abs(sn.re/sum.re)  , abs(sn.im/sum.im)); 
    ed = max(abs(dsum.re/sum.re), abs(dsum.im/sum.im));

    if (es<es_min) {
      res_min_es = sum; es_min=es; }; 
    if (ed>es) break; n=n+1;          };

  last_cdres = sum; last_derr = max(es, ed);
  last_corr  = last_derr < 0.1;
  if (!last_corr ) last_cdres = res_min_es;
  return(last_cdres);
}

//
// ---------- halb lcomplex KummerM as defined sum with Kummer transformation -
//
dcomplex KummerM_sum_Kt_max(dcomplex a, double b, dcomplex z)
{ 
  dcomplex da, dz;
  da = dcomplex(a.re, a.im); 
  double db = double(b);
  dz = dcomplex(z.re, z.im);

  if (abs((b-a)/b*z)<abs(a/b*z)) {
    last_cdres= (exp(dz)*KummerM_sum_max(db-da,db,-dz)); }
else {
    last_cdres= (KummerM_sum_max(da,db,dz)); }

   return(last_dres);
}
//
// --------------- halb lcomplex KummerM as continued fraction --------------------------
//
dcomplex KummerM_cf(dcomplex a, double b, dcomplex x)
{
  dcomplex Ai; dcomplex Aiminus1 = 1.0+x*(a/b); dcomplex Aiminus2 = 1.0;
  dcomplex ri; long i=2;
  do{ri=(a+(i-1))/(i*(b+(i-1)));
  Ai=Aiminus1+(Aiminus1-Aiminus2)*ri*x; 
  Aiminus2=Aiminus1; Aiminus1=Ai; i=i+1;
  } while ((abs((Ai-Aiminus2)/Ai)>greens_dexact)&&(abs(Ai)>greens_dexact));
  
  if (last_asymp_corr)
  {if (abs((Ai-last_asymp_cdres)/last_asymp_cdres)<=(last_asymp_derr+greens_dexact))
    {last_corr=true; last_derr=greens_dexact;} 
 else {last_corr=false; last_derr=1.0;};} 
else {if (i>Max_cTerm) {last_corr=false; last_derr=1.0;} 
    else {last_corr=true; last_derr=greens_dexact;}};
  last_cdres=Ai; return(last_cdres);
}

//
// ---------- half lcomplex KummerM as continued fraction with Kummer transformation ---------
//
dcomplex KummerM_cf_Kt(dcomplex a, double b, dcomplex x)
{
  if (abs((b-a)/b*x)<abs(a/b*x)) {last_cdres=exp(x)*KummerM_cf(b-a,b,-x); return(last_cdres);}
else return(KummerM_cf(a,b,x));  
}

/*!
 half lcomplex KummerM as rational approximation 
\bibitem{Luke} Yudell~L.~Luke, {\it Algorithms for the Computation of
Mathematical Functions}, (Academic Press, New York 1977).
*/
dcomplex KummerM_ra(dcomplex a, double c, dcomplex z)
{
dcomplex Bim3, Bim2, Bim1, B, Aim3, Aim2, Aim1, A, F1, F2, F3, 
        E, powz2, powz3, Mim1, M, FA1, FB1, npcm2, npcm1, npam1; 
long n=3; long twon;
 
 powz2=z*z; powz3=-powz2*z; Bim3=1; Bim2=1-(a+1)*z/2/c; 
 Bim1=1-(a+2)*z/2/(c+1)+(a+1)*(a+2)*powz2/12/c/(c+1);
 
 Aim3=1; Aim2=Bim2+z*a/c;
 Aim1=Bim1+a*z/c*(1-(a+2)*z/2/(c+1))+a*(a+1)*powz2/2/c/(c+1);
 
 Mim1=Aim1/Bim1;
 
 FA1=n-a-2; FB1=2*(2*n-3)*(n+c-1); F1=FA1/FB1;
 
 for(;n<Max_cTerm_ra;n++)
 {
 twon=2*n; npcm2=n+c-2; npcm1=npcm2+1; npam1=n+a-1;
 F2=(n+a)*npam1/4/(twon-1)/(twon-3)/npcm2/npcm1;
 F3=-(n+a-2)*npam1*(n-a-2)/
     (8*pow(twon-3.0, 2.0)*(twon-5)*(n+c-3)*npcm2*npcm1);
 E=-npam1*(n-c-1)/(2*(twon-3)*npcm2*npcm1);
 
 A=(1-F1*z)*Aim1-(E-F2*z)*z*Aim2+F3*powz3*Aim3; 
 B=(1-F1*z)*Bim1-(E-F2*z)*z*Bim2+F3*powz3*Bim3;

 Aim3=Aim2; Aim2=Aim1; Aim1=A; Bim3=Bim2; Bim2=Bim1; Bim1=B;
 M=A/B; last_derr=abs((Mim1-M)/M); Mim1=M;
  
 if (last_derr<greens_dexact) break; 
 FA1=FA1+1; FB1=(4*n-2)*(n+c); F1=FA1/FB1;
 }
  last_corr=true; last_cdres=M; return(last_cdres);
}
//
// ------ half lcomplex KummerM as rational approximation with Kummer' transform---- 
//
dcomplex KummerM_ra_Kt(dcomplex a, double b, dcomplex x)
{
  if (abs((b-a)/b*x)<abs(a/b*x)) {last_cdres=exp(x)*KummerM_ra(b-a,b,-x); return(last_cdres);}
else return(KummerM_ra(a,b,x));  
}


//
dcomplex KummerM_asymp(dcomplex a, double b, dcomplex z)
{ 
  greens_save_dexact=greens_dexact; 
  dcomplex DG1, DG2, c1, c2, s; double g1, g2; bool br1, br2;
  limit_asymp=true;
  
  c1= (exp(lngamma(b)-lngamma(b-a)))*Cpow(-z, -a);
  c2= (exp(lngamma(b)-lngamma(a)))*exp(z)*Cpow(z, a-b); 
  do{
    DG1 = greens_G(a, a-b+1.0, -z); g1 = last_derr; br1=last_corr;
    DG2 = greens_G(b-a, 1.0-a,  z); g2 = last_derr; br2=last_corr; 
  
    s   = c1*DG1+c2*DG2; g1=(abs(g1*c1*DG1)+abs(g2*c2*DG2))/abs(s);
    last_corr=(br1&&br2); greens_dexact=greens_dexact/10.0;
  } while((!limit_asymp)&&(g1>greens_save_dexact));
  greens_dexact=greens_save_dexact;

  
  last_cdres=s; last_asymp_cdres=s;
  if (last_corr) {last_derr=g1;} else last_derr=1.0;
  last_asymp_corr=last_corr; last_asymp_derr=last_derr; 
  return(last_cdres);
}
//
//
//--------------- halb lcomplex KummerM with mirror or as usuall sum -------
//
#define cKummerMNumOfMeth 3

dcomplex KummerM(dcomplex a, double b, dcomplex z)
{
  if (greens_IsNonPosInteger(b)) greens_ERROR(_str_cKummerM, _str_wrong_args);
  double err[cKummerMNumOfMeth]; 
      dcomplex res[cKummerMNumOfMeth]; bool corr[cKummerMNumOfMeth];
  if (z==0.0) {last_derr=double_exact; last_corr=true; last_cdres=1.0; return(last_cdres);} 
  
  int nm=0; int i; //number of methods and cicle variable
  
  KummerM_asymp (a, b, z); if ((last_derr<=greens_dexact)&last_corr) goto RETURN;
  res[nm]=last_cdres; corr[nm] = last_corr; err[nm]=last_derr; nm=nm+1;
  
  KummerM_sum_Kt_max(a, b, z); if ((last_derr<=greens_dexact)&last_corr) goto RETURN;
  res[nm]=last_cdres; corr[nm] = last_corr; err[nm]=last_derr; nm=nm+1;
  
  KummerM_ra_Kt(a, b, z); if ((last_derr<=greens_dexact)&last_corr) goto RETURN; 
  res[nm]=last_cdres; corr[nm] = last_corr; err[nm]=last_derr; nm=nm+1;
  
  for(i=0; i<nm; i++){ last_corr=corr[i]; 
    if (last_corr) {last_cdres=res[i]; last_derr=err[i]; break;};};
 
  if (last_corr)
  { for(i=0; i<nm; i++){ 
      if ((last_derr>err[i])&corr[i]) { last_derr=err[i]; last_cdres=res[i];}}}
else 
  { for(i=0; i<nm; i++){ 
      if (last_derr>err[i]) { last_derr=err[i]; last_cdres=res[i];}}};
  
  if (warningbreak==Break) 
  {
   writeln(a); writeln(b); writeln(z); print();
   for(i=0; i<nm; i++){}; print();
   for(i=0; i<nm-1; i++){write(err[i]);}; print(err[nm-1]);
   for(i=0; i<nm-1; i++){write(corr[i]);}; print(corr[nm-1]);
   write(last_cdres); write(last_derr); writeln(last_corr);
   print();
   writeln(abs(a*z/b));
  }
                  greens_WARNING(_str_cKummerM, _str_precision_not_reached);
  if (!last_corr) greens_WARNING(_str_cKummerM, _str_all_methods_wrong);

RETURN:
   
  return(last_cdres);
}

//
// ---------------- Kummer's U -----------------------------------------------
//
const char *_str_KummerU = "KummerU"; 
//
//---------------- double KummerU as asympt ------------------------------------
//
dcomplex KummerU_asymp(double a, double b, double z)
{double Pia = Pi*a; double Pib = Pi*b;
 if (z>=0.0) {return(pow(z,-a)*greens_G(a, 1+a-b, -z));} else 
  {return((Cpow(-z,-a)*(sin(Pib-Pia)+Cpow(z,-b)*Cpow(-z,b)*sin(Pia))/sin(Pib))*
          greens_G(a, 1+a-b, -z));}
}
//
// --------------- double KummerU as Logarithmic solution for positiv z ------------ 
//
dcomplex KummerU_Psi(double a, long b, double z)
{  
  if (b<1) return(Cpow(z,1-b)*KummerU_Psi(1+a-b, 2-b, z));
 
  double smd1 = Psi(a)+gamma_Euler-Psi(b);
  double smd2 = 1.0; dcomplex smd   = smd1;
   dcomplex s = smd; long k = 0; dcomplex s1;

do {
  smd2=smd2*(a+k)/(b+k)/(k+1)*z; smd1=smd1+1.0/(a+k)-1.0/(1+k)-1.0/(b+k); 
  smd =smd1*smd2;  s=s+smd; k=k+1;
   } while(abs(smd/s)>greens_dexact);
    s1=s;
double arg1; long arg2;
dcomplex res;
long       u;
  
  if (b==1){res=-1.0/GAMMA(a)*(s1+Clog(z)*KummerM(a,b,z)); } 
else {smd=1.0; s=1.0; u=b-1; arg1=a-b+1; arg2=2-b;
      for (k=1; k<u; k++){smd=smd*(arg1+k-1)/(arg2+k-1)/k*z; s=s+smd;};
      res=pow_minus(b)/GAMMA(a-b+1)/
          GAMMA(b)*(s1+Clog(z)*KummerM(a,b,z))+
          Cpow(z, 1-b)*(exp(lngamma(b-1)-lngamma(a)))*s;}; last_corr=true;
  return(res);
}
//
// --------------- double KummerU as defined series ---------------------------------
//
dcomplex KummerU_sum(double a, double b, double z)
{
  double g1, g2; bool b1, b2; //double g3; 
  if (abs(ceil(b)-b) < double_exact) return(KummerU_Psi(a, long(b), z)); 

    double KM1 = KummerM(a,b,z);           g1 = last_derr; b1 = last_corr;
    double KM2 = KummerM(1.0+a-b,2.0-b,z); g2 = last_derr; b2 = last_corr;
    
  double PiDivSinPib = Pi/sin(Pi*b);
  dcomplex c1  = (1.0/exp(lngamma(1.0+a-b)+lngamma(b)))*KM1;  
  dcomplex c2  = (Cpow(z,1.0-b)/exp(lngamma(a)+lngamma(2.0-b)))*KM2;
  dcomplex s   = c1-c2; s = s * PiDivSinPib; 
  
//  g3  = greens_dexact/abs(s/(c1+c2)/PiDivSinPib);
//  if (g3>0.1){
//    last_derr=1.0; last_corr=false; last_cdres = s; return(last_cdres);}
//
//  if (z>0.0) {c1.im=0.0; c2.im=0.0; s.im=0.0;};
//  g1 = (abs(c1)*g1+abs(c2)*g2)/abs(s); 
//  
//  last_derr=g1; last_corr=last_derr<0.1; last_cdres = s; 
//    //  if (last_derr<greens_dexact) last_derr = greens_dexact; last_corr=(b1 && b2); 
    
  return(s);
}

//
// ---------------- double Kummer's U -----------------------------------
//
#define KummerUNumOfMeth 3

bool KummerU_debug=false;

dcomplex KummerU(double a, double b, double z)
{ 
  if (z==0.0) {if (1-b<=0.0) {greens_ERROR(_str_KummerU, _str_singularity);} 
else {last_derr=greens_dexact; 
      last_corr=true; 
      return(Pi/sin(Pi*b)/Re(exp(lngamma(b)+lngamma(1+a-b))));}};
  
  if (greens_IsNonPosInteger(    a  )) 
    greens_ERROR(_str_KummerU, _str_singularity);
  if (greens_IsNonPosInteger(1.0+a-b)) 
    greens_ERROR(_str_KummerU, _str_singularity); 

  dcomplex res[KummerUNumOfMeth];  double err[KummerUNumOfMeth]; 
  bool     corr[KummerUNumOfMeth]; int NOfMeth=2;
  
  last_cdres=KummerU_asymp(a,b,z); 
  if (last_derr<=greens_dexact) goto RETURN;
  res[0]=last_cdres; err[0]=last_derr; corr[0]=last_corr;

  last_cdres=KummerU_sum(a,b,z);  
  if (last_derr<=greens_dexact) goto RETURN; 
  res[1]=last_cdres; err[1]=last_derr; corr[1]=last_corr;


  for(int i = 0; i<NOfMeth; i++)
  {if (!last_corr) {
     if (corr[i]) {
       last_corr=true; last_cdres=res[i]; last_derr=err[i];} 
   else {
       if (last_derr>err[i]) {last_cdres=res[i]; last_derr=err[i];}}}
 else {
     if (corr[i]) {
       if (last_derr>err[i]) {last_cdres=res[i]; last_derr=err[i];}}}}; 
  

  greens_WARNING(_str_KummerU, _str_precision_not_reached);

  if (!last_corr) greens_WARNING(_str_KummerU, _str_all_methods_wrong);
RETURN:

//#ifdef DebugKummerU
//    print(_str_KummerU); 
//    write(a);       write(b);       writeln(z);
//    write(res[0]);  write(res[1]);  writeln(last_cdres);
//    write(err[0]);  write(err[1]);  writeln(last_derr);
//    write(corr[0]); write(corr[1]); writeln(last_corr);
//#endif

  return(last_cdres);
}

//
// ------------   double WhittakerM ---------------------------------------------------------
//
double WhittakerM(double a, double b, double z)
{return(exp(-z*0.5)*pow(z,b+0.5)*KummerM(0.5+b-a, 1.0+b+b, z));}

//
// ------------ dcomplex WhittakerW -------------------------------------------------------------
//
dcomplex WhittakerW(double a, double b, double z)
{return(exp(-z*0.5)*Cpow(z,b+0.5)*KummerU(0.5+b-a, 1.0+b+b, z));}

//
// ------------ double Bessel Functions J -----------------------------------
//
double BesselJ_defser(double nu, double x)
{
  double s, smd; double mx2d4=-x*x/4.0;
  long k; smd=1.0/GAMMA(nu+1); s=smd; 
  for (k=1; ; k++){ smd=smd*mx2d4/k/(nu+k); s=s+smd; last_derr=abs(smd/s); 
   if (last_derr<double_exact) break;}; 
   last_dres=s*pow(x*0.5, nu); last_corr=true;
return(last_dres);
}

//
// ------------ double j(n,z) --------------------------------------------------------------
//
double      _spherical_f(long int n, double z)
  {
  
  if (n>4) return((2*n-1)/z*_spherical_f(n-1, z)-_spherical_f(n-2, z));
  if (n<-3) return((2*n+3)/z*_spherical_f(n+1, z)-_spherical_f(n+2, z));
  
  double powz2=z*z;
  switch(n){
   case 4: return(((105.0/powz2-45.0)/powz2+1.0)/z);
   case 3: return((15.0/powz2-6.0)/powz2);
   case 2: return((3.0/powz2-1.0)/z);
   case 1: return(1/powz2);
   case 0: return(1/z);
   case -1: return(0.0);
   case -2: return(-1.0/z);
   case -3: return(3.0/powz2);}
   
  return(0);
}
//
//
//
double greens_spherical_j(long int n, double z)
{
  if (z==0.0) {
    if (n==0) return(1.0);
    if (n<0) {greens_ERROR(_str_greens_spherical_j, _str_wrong_n);} else return(0.0);}
    
  if (z<=2) {return(sqrt(Pi/(z+z))*BesselJ_defser(n+0.5, z));}
else {return(_spherical_f(n,z)*sin(z)+pow_minus(n+1)*_spherical_f(-n-1,z)*cos(z));}
}
//
// ------------------ Heaviside(x) -------------------------------------------------
//
double Heaviside(double x){if (x>0.0) return(1.0); if (x<0.0) return(0.0); return(0.5);}

/*!
 A hypergeometric function of several variables (two variables) Gradshteyn and Ryzhik
 all variable are double
*/
double greens_F2(double alpha, double beta1, double beta2, double gamma1, 
                double gamma2, double x, double y)
{
  double beta1m, beta2n, gamma1m, gamma2n, xm, yn, alpham, alphamn, 
         mf, nf, sn, smn, smd;

  if (greens_IsNonPosInteger(beta1) & 
      greens_IsNonPosInteger(gamma1) & (gamma1>beta1)) 
    greens_WARNING(_str_greens_F2, _str_wrong_args);

  if (greens_IsNonPosInteger(beta2) & 
      greens_IsNonPosInteger(gamma2) & (gamma2>beta2)) 
    greens_WARNING(_str_greens_F2, _str_wrong_args);

  if (((abs(x)+abs(y))>1.0) & !greens_IsNonPosInteger(beta1) & !greens_IsNonPosInteger(beta1)) 
    greens_WARNING(_str_greens_F2, _str_perhaps_series_not_convergent);

  beta1m=1; gamma1m=1; xm=1; alpham=1; mf=1; smn=0;

  for(long m=0; ; m++)
  { beta2n=1; gamma2n=1; alphamn=alpham; yn=1; nf=1; sn=0.0;
    for(long n=0; ;n++)
    {smd=alphamn*beta2n*yn/gamma2n/nf; 
     sn=sn+smd;
      
     if (abs(sn)==0.0) break;
     if (abs(smd/sn)<greens_dexact) break;

     yn=yn*y; alphamn=alphamn*(alpha+m+n); nf=nf*(n+1); 
     gamma2n=gamma2n*(gamma2+n); beta2n=beta2n*(beta2+n);
    }
    smd=sn*beta1m*xm/gamma1m/mf; 
    smn=smn+smd; 

    if (abs(smd/smn)<greens_dexact) break;
    xm=xm*x; beta1m=beta1m*(beta1+m); mf=mf*(m+1); 
    gamma1m=gamma1m*(gamma1+m); alpham=alpham*(alpha+m);
  };

  last_dres=smn; last_derr=greens_dexact; last_corr=true;
  return(last_dres);
}

//
// A hypergeometric function of several variables (two variables) Gradshteyn and Ryzhik
// variable are double and lcomplex
//
dcomplex greens_F2(double alpha, dcomplex beta1, double beta2, double gamma1, 
                  double gamma2, dcomplex x, dcomplex y)
{
  double beta2n, gamma1m, gamma2n, alpham, alphamn, mf, nf;
  dcomplex smn, smd, yn, xm, sn, beta1m;

  if (greens_IsNonPosInteger(beta2) & greens_IsNonPosInteger(gamma2) & (gamma2>beta2)) 
    greens_WARNING(_str_greens_F2, _str_wrong_args);

  if (((abs(x)+abs(y))>1.0) & !greens_IsNonPosInteger(beta2)) 
    greens_WARNING(_str_greens_F2, _str_perhaps_series_not_convergent);

  beta1m=1; gamma1m=1; xm=1; alpham=1; mf=1; smn=0;

  for(long m=0; ; m++)
  { beta2n=1.0; gamma2n=1; alphamn=alpham; yn=1; nf=1; sn=0.0;
    for(long n=0; ;n++)
    {smd=alphamn*beta2n*yn/gamma2n/nf; 
     sn=sn+smd;
      
     if (abs(sn)==0.0) break;
     if (abs(smd/sn)<greens_dexact) break;
     
     yn=yn*y; alphamn=alphamn*(alpha+m+n); nf=nf*(n+1); 
     gamma2n=gamma2n*(gamma2+n); beta2n=beta2n*(beta2+n);
     
    }
    smd=sn*beta1m*xm/gamma1m/mf; 
    smn=smn+smd; 

    if (abs(smd/smn)<greens_dexact) break;
    xm=xm*x; beta1m=beta1m*(beta1+m); mf=mf*(m+1); 
    gamma1m=gamma1m*(gamma1+m); alpham=alpham*(alpha+m);
  };

  last_cdres=smn; last_derr=greens_dexact; last_corr=true;
  return(last_cdres);
}
