#include  "greens_nearest.hpp"


/*!
 There is an analog of the FORTRAN function  nearest(x,direction)
 
 simple equivalent, of course. How to hack the double
 arithmetic I did not understand
 mailto:kovalp@mail.ru
*/
double nearest(register double num, register float dir)
{
  register double res;  
  if (num != 0.0) {
    res = abs(num * double_exact);
    if (dir>=0) {res=num+res;}
  else {res=num-res;}} 
else {return(0.0);}; 
  return(res);
}

/*!
  The function represents the absolute deviation of a "double" number
*/
double deviation(register double num){return(abs(num*double_exact));}

/*!
  The function represents the absolute deviation of a "double complex" number
*/
dcomplex deviation(dcomplex num){
  return(dcomplex(abs(num.re*double_exact), abs(num.im*double_exact)));}

