#include "greens_relgreenfunc.hpp"
//
// ----------- relativistic radial Green's function -------------------------- 
//   g(E; r1,r2)   multielectron definition  atomic units
//

matrix_2x2 greens_radial_matrix(double E_non, int kappa, 
				    double r1, double r2)
{
  if (kappa==0) greens_ERROR(_str_radial_Green_matrix, _str_wrong_kappa);
//  if (E_non>=0.0) greens_ERROR(_str_radial_Green_matrix, _str_wrong_energy);
  if ((r1<0.0)||(r2<0.0)) 
    greens_ERROR(_str_radial_Green_matrix, _str_wrong_coords);
  double rl, rs, E; 
  if (r1>r2) {rl=r1; rs=r2;} else {rl=r2; rs=r1;};
  
  E = E_non + greens_constant_au_E0;

  double Z        = greens_save_nuclear_charge;
  if (rs==0.0) return(0.0); 
  if (rl==0.0) return(0.0);

  double alpha     = greens_constant_alpha;
  double alphaZ    = alpha*Z;
  double gamma     = sqrt(kappa*kappa-alphaZ*alphaZ);
  double TwoGamma  = gamma+gamma;
  double TwoGammaPUnit = TwoGamma+1.0;
  double KTwo      = TwoGamma*TwoGammaPUnit;
  double gammam05  = gamma - 0.5;
  double gammap05  = gamma + 0.5;
  double X         = (-kappa+gamma)/alphaZ;
  double SqrX      = X*X;
  double UnitMSqrX = 1.0-SqrX;
  double epsilon   = E/greens_constant_au_c;
  double epsilon0  = greens_constant_au_c;
  double omega     = sqrt(epsilon0*epsilon0-epsilon*epsilon);
  double nu        = alphaZ*epsilon/omega;
  double GammaMNu  = gamma-nu;
  double GammaPNu  = gamma+nu;
    
         epsilon   = epsilon*kappa;  
         epsilon0  = epsilon0*gamma;

  double M1, M2, M3, M4, W1, W2, W3, W4, TwoOmegaR1, TwoOmegaR2,
         err[8], right[8];

  TwoOmegaR1=omega+omega; 
  TwoOmegaR2=TwoOmegaR1*rs; TwoOmegaR1=TwoOmegaR1*rl;
  
  M1 = WhittakerM(nu, gammam05, TwoOmegaR1); right[0]=last_corr; 
  err[0]=last_derr;
  M2 = WhittakerM(nu, gammam05, TwoOmegaR2); right[1]=last_corr; 
  err[1]=last_derr;
  M3 = WhittakerM(nu, gammap05, TwoOmegaR1); right[2]=last_corr; 
  err[2]=last_derr;
  M4 = WhittakerM(nu, gammap05, TwoOmegaR2); right[3]=last_corr; 
  err[3]=last_derr;
  
  
  
  W1 = Re(WhittakerW(nu, gammam05, TwoOmegaR1)); 
    right[4]=last_corr; err[4]=last_derr; 
  W2 = Re(WhittakerW(nu, gammam05, TwoOmegaR2)); 
    right[5]=last_corr; err[5]=last_derr;
  W3 = Re(WhittakerW(nu, gammap05, TwoOmegaR1)); 
    right[6]=last_corr; err[6]=last_derr;  
  W4 = Re(WhittakerW(nu, gammap05, TwoOmegaR2)); 
    right[7]=last_corr; err[7]=last_derr;
  
  
//  write(M1); write(M2); write(M3); writeln(M4);  
//  write(W1); write(W2); write(W3); writeln(W4); 
//  write(nu); write(gammam05); writeln(TwoOmegaR1); 

  
  double c21    = UnitMSqrX*alpha/(gamma+gamma);
  double c22    = c21/omega;
  double KGAMMA = Re(exp(lngamma(GammaMNu)-lngamma(TwoGamma)));
  
  double h22    = (epsilon-epsilon0)*c22*KGAMMA*M2*W1;
         KGAMMA = KGAMMA*GammaMNu/KTwo;

  double h11    = (epsilon+epsilon0)*c22*KGAMMA*M4*W3;
  
  double HeavisideOfR2MR1 = Heaviside(rs-rl);
  double HeavisideOfR1MR2 = Heaviside(rl-rs);
                   KGAMMA = c21*KGAMMA;
  double h21    = KGAMMA*(KTwo*HeavisideOfR2MR1*M1*W4-
                          GammaPNu*HeavisideOfR1MR2*W1*M4);
  double h12    = KGAMMA*(KTwo*HeavisideOfR1MR2*M2*W3-
                          GammaPNu*HeavisideOfR2MR1*W2*M3);
     
  matrix_2x2 g;  double SqrUnitMSqrX = UnitMSqrX*UnitMSqrX;
  double XMulH12PH21  = X*(h12+h21);
  double XMulH11PH22  = X*(h11+h22);

  g.e[0][0]=(h11-XMulH12PH21+SqrX*h22)/SqrUnitMSqrX;

  if (r1>r2)
  {g.e[0][1]=(-XMulH11PH22+h12+SqrX*h21)/SqrUnitMSqrX; 
   g.e[1][0]=(-XMulH11PH22+h21+SqrX*h12)/SqrUnitMSqrX;}
else
  {g.e[0][1]=(-XMulH11PH22+h21+SqrX*h12)/SqrUnitMSqrX; 
   g.e[1][0]=(-XMulH11PH22+h12+SqrX*h21)/SqrUnitMSqrX;};
       
   g.e[1][1]=(SqrX*h11-XMulH12PH21+h22)/SqrUnitMSqrX;

/*   
   write(g.e[0][0]); write(g.e[0][1]); write(g.e[1][0]);
   writeln(g.e[1][1]); exit(1);
*/
  last_mdres = g; 
  last_derr=err[0]; last_corr=right[0];
  for(int i=1; i<8; i++){
    if (last_derr<err[i]) 
      last_derr=err[i]; last_corr=last_corr && right[i];};
  
  return(g);
}


//
// ----------- relativistic radial Green's function ----------------------------- 
//   g(E; r1,r2)/(r1 r2)  normal (Drake) definition  atomic units
//
matrix_2x2 greens_radial_matrix_Drake(double E, int kappa, 
					  double r1, double r2)
{
  if ((r1==0.0) || (r2==0.0))  return(matrix_2x2(0.0,0.0,0.0,0.0));
  return(greens_radial_matrix(E-greens_constant_au_E0, kappa, 
                                   r1, r2)/(r1*r2));
}
