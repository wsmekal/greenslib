#ifndef __GREENS_NONRELGREENFUNC_HPP
#define __GREENS_NONRELGREENFUNC_HPP

#include "greens_specfunc.hpp"

/*!
  The function returns a Coulomb radial Green's function 
  Energy must be less than zero.

  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function    
*/
double greens_radial_function(double E, int l, double r1, double r2);

/*!
  The function returns a Coulomb radial Green's function 
  Energy must be below zero
  There is Drake definition
  \bibitem{Drake:91:2} R.~A.~Swainson and G.~W.~F.~Drake,
  J. Phys. A {\bf 24} (1991) 95.

  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function  
*/
double greens_radial_function_Drake(double E, int l, double r1, 
                                          double r2);

#endif
