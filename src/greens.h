#include <math.h>
#include <stdio.h>

#include "greens_const.hpp"           // Physical and mathematical constant
#include "greens_ieeeftn.hpp"         // the analyse of the floating point numbers
#include "greens_dcomplex.hpp"        // double complex: dcomplex class
#include "greens_service.hpp"         // print(), greens_set_nuclear_charge() greens_set_Digits()
#include "greens_nearest.hpp"         // nearest() and deviation() of a number
#include "greens_specfunc.hpp"        // special functions KummerM, KummerU, WhittakerM, WhittakerM
#include "greens_numint.hpp"          // procedures for numerical integration
#include "greens_convert.hpp"         // converting of physical quantities
#include "greens_mdclass.hpp"         // matrix_2x2, spinor2v, spinor2h  
#include "greens_relwavefunc.hpp"     // relativistic radial wave function
#include "greens_relgreenfunc.hpp"    // relativistic radial Coulomb Green's function
#include "greens_nonrelwavefunc.hpp"  // nonrelativistic radial wave function
#include "greens_nonrelgreenfunc.hpp" // nonrelativistic radial Coulomb Green's function
#include "greens_tpcs.hpp"            // dummy procedure which calls other tpcs
#include "greens_tpcsnl.hpp"          // nonrelativisitic two photon cross sections
#include "greens_tpcsrl.hpp"          // relativistic two photon cross sections 
                                      // long wave approx
				      
#include "greens_wnj.hpp"             // Wigner 3j-, 6j-, 9j- coefficients
#include "greens_ylm.hpp"             // Y_{lm}(theta,phi) spherical harmonic
#include "greens_nonrelme.hpp"        // nonrelativistic matrix elements
#include "greens_linewidthn.hpp"      // nonrelativistic line widths
#include "greens_opcsnl.hpp"          // one photon cross section long wave approx
#include "greens_tpcsnlds.hpp"        // nonrelativ. two photon cross sections direct summation
#include "greens_tpcsrm.hpp"          // two photon cross section Green's function relativistic multipol approx
#include "greens_tpadrm.hpp"          // two photon angular distr (differential cross section)
#include "greens_tpcsrtm.hpp"         // two photon cross section "exact" calculation
#include "dvector.hpp"                // vector of double real components
#include "cdvector.hpp"               // vector of dcomplex complex components
#include "phys_value.hpp"             // physical quantities with a dimension

#undef COMPLEX
#undef REAL
#undef SPINOR2V
#undef SPINOR2H
#undef MATRIX2X2

#define COMPLEX dcomplex
#define REAL    double
#define SPINOR2V spinor2v
#define SPINOR2H spinor2h
#define MATRIX2X2 matrix_2x2

#define LL e[0][0]
#define LS e[0][1]
#define SL e[1][0]
#define SS e[1][1]
