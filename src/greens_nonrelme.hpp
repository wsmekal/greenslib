#ifndef __GREENS_NONRELME_HPP
#define __GREENS_NONRELME_HPP

#include "greens_nonrelwavefunc.hpp"
#include "greens_specfunc.hpp"
#include "greens_service.hpp"
#include "greens_numint.hpp"
#include "greens_wnj.hpp"

//
// radial matrix element due to the Bethe and Solpiter
// int(R(n1,l1,r)*r*R(n2,l2,r)*r^2, r=0..infinity) or 
// int(P(n1,l1,r)*r*P(n2,l2,r)    , r=0..infinity)
//
double greens_nonrel_radial_bound_bound_me (int n1, int l1, int n2, int l2);
//
// radial matrix element due to the Bethe and Solpiter
// int(R(n1,l1,r)*r*R(n2,l2,r)*r^2, r=0..infinity) or 
// int(P(n1,l1,r)*r*P(n2,l2,r)    , r=0..infinity)
//
double greens_nonrel_radial_bound_bound_me (int n1, int l1, int n2, int l2, int digits);

//
// <\psi_f| \bveps_{\lambda} \br | \psi_i>
// 
double greens_nonrel_bound_bound_me(int n_f, int l_f, int m_f, int lambda, 
                                   int n_i, int l_i, int m_i);
//
// <\psi_f| \bveps_{\lambda} \br | \psi_i>
// 
double greens_nonrel_bound_bound_me(int n_f, int l_f, int m_f, int lambda, 
                                   int n_i, int l_i, int m_i, int digits);
                                        
//
// radial matrix element due to the Bethe and Solpiter
// int(R(E1,l1,r)*r*R(n2,l2,r)*r^2, r=0..infinity) or 
// int(P(E1,l1,r)*r*P(n2,l2,r)    , r=0..infinity)
// bound - free, now only numerically, because the analytic matrix element
// is not converged with today greens_F2()
//

double greens_nonrel_radial_free_bound_me (double n1, int l1, 
                                             int n2, int l2, int digits);

//
// <\psi_{E_f}| \bveps_{\lambda} \br | \psi_i>
//
double greens_nonrel_free_bound_me(double E_f, int l_f, int m_f, int lambda, 
                                     int n_i, int l_i, int m_i, int digits);

#endif
