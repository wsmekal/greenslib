#include <string.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>

#include "tpcs.h"

#include "greens_complex.cpp"
#include "greens_service.cpp"
#include "greens_mclass.cpp"
#include "greens_maplefunc.cpp"
#include "greens_numint.cpp"
#include "greens_specfunc.cpp"
#include "greens_convert.cpp"
#include "greens_relwavefunc.cpp"
#include "greens_relgreenfunc.cpp"
#include "greens_nonrelwavefunc.cpp"
#include "greens_nonrelgreenfunc.cpp"  //nonrelativistic radial Coulomb Green's function
#include "greens_wnj.cpp"              //wnj coefficients
#include "greens_nonrelme.cpp"         //nonrelativistic matrix elements
#include "greens_linewidthn.cpp"       //nonrelativistic line widths
#include "greens_opcsnl.cpp"           //one photon cross section long wave approx
#include "greens_tpcsnl.cpp"           //nonrelativisitic two photon cross sections
#include "greens_tpcsnlds.cpp"         //nonrelativ. two photon cross sections direct summation
#include "greens_tpcsrl.cpp"           //relativistic two photon cross sections long wave approx
#include "greens_tpcsrm.cpp"           //two photon cross section Green's function relativistic multipol approx

//
// to maple 6 and 7 dKummerM_sum__Fddd
// in the maple use float[8] as float variable
//
double dKummerM_sum_Kt(double a, double c, double z){ if (a==c) return(double(expl(z)));
return(double(KummerM_sum_Kt(a,c,z)));}
//
//
double dKummerM_ra_Kt(double a, double c, double z){ if (a==c) return(double(expl(z)));
return(double(KummerM_ra_Kt(a,c,z)));}
//
//
double dKummerM(double a, double c, double z){return(double(KummerM(a,c,z)));}
//
//
//dcomplex cKummerM(double a_re, double a_im, double c, double z_re, double z_im)
//{ return(dcomplex(KummerM(complex(a_re,a_im),c,complex(z_re,z_im))));}
//

//
// return last_* variabels
//

double greens_Get_last_err(void){return(double(greens_get_last_err()));}
//
//
double greens_Get_last_res(void){return(double(greens_get_last_res()));}
//
//
  bool greens_Get_last_corr(void){return(greens_get_last_corr());}
//
//
long   greens_Get_Digits(void){return(greens_get_Digits());}  
long   greens_Set_Digits(long digits){return(greens_set_Digits(digits));}  

//------------------------------- Physical functions ----------------------------
int dgreens_set_nuclear_charge(double Z){greens_set_nuclear_charge(Z);}

double dgreens_radial_orbital(double E, long l, double r)
{return(double(greens_radial_orbital(E, l, r, continuum)));}

double dgreens_radial_orbital(long n, long l, double r)
{return(double(greens_radial_orbital(n, l, r, bound)));}

