#include "greens_tpadrm.hpp"

//
// reduced matrix element  <(-1)^i kappa_a | sigma T_{L Lambda}| (-1)^(i+1) kappa_b>
//
double greens_RME_T(int kappa_a, int kappa_b, int Lambda, int L, int i)
{

  if ((kappa_a==0)||(kappa_b==0)) greens_ERROR(_str_greens_reduced_me, _str_wrong_kappa);
  if ((L<0)||(Lambda<0)) greens_ERROR(_str_greens_reduced_me, _str_wrong_args);
  
  int  l_a, l_b;
  double result;
 
  l_a = greens_angular_l(int(pow_minus(i)   * kappa_a));
  l_b = greens_angular_l(int(pow_minus(i+1) * kappa_b));
  
  result = greens_ClebschGordan(l_b,0.0,Lambda,0.0,l_a,0.0);
  if (result==0.0) return(result);

  double j_a = abs(kappa_a)-0.5, 
         j_b = abs(kappa_b)-0.5;
    
  result = result * greens_w9j(l_b, 0.5, j_b, 
                           Lambda, 1.0, L, 
		              l_a, 0.5, j_a);
  if (result==0.0) return(result);
   
  result = result 
         * sqrt(3.0/Pi*0.5)
	 * sqrt((j_a+j_a+1.0)*(j_b+j_b+1.0)
	 * (L+L+1)*(Lambda+Lambda+1)*(l_b+l_b+1.0));

  return(result);
}

//
// reduced matrix element  without checking
//
double _greens_RME_T(int kappa_a, int kappa_b, int Lambda, int L, int i)
{
 
  int  l_a, l_b;
  double result;
 
  l_a=greens_angular_l(int(pow_minus(i)  * kappa_a));
  l_b=greens_angular_l(int(pow_minus(i+1)* kappa_b));
  
  result = _greens_ClebschGordan(l_b,0.0,Lambda,0.0,l_a,0.0);
  if (result==0.0) return(result);

  double j_a = abs(kappa_a)-0.5, 
        j_b = abs(kappa_b)-0.5;
    
  result = result * _greens_w9j(l_b, 0.5, j_b, 
                            Lambda, 1.0, L, 
		               l_a, 0.5, j_a);
  if (result==0.0) return(result);
   
  result = result 
         * sqrt(3.0/Pi*0.5)
	 * sqrt((j_a+j_a+1.0)*(j_b+j_b+1.0)
	 * (L+L+1)*(Lambda+Lambda+1)*(l_b+l_b+1.0));

  return(result);
}
//
//
//
/*!
   general reduced matrix element  <kappa_b l_b | sigma T_{L Lambda}| kappa_a l_a>
*/
double greens_RME_T(int kappa_b, int l_b, int L, int Lambda, int kappa_a, int l_a)
{
  if ((kappa_a==0)||(kappa_b==0)) greens_ERROR(_str_greens_reduced_me, _str_wrong_kappa);
  if ((l_a<0)||(l_b<0)) greens_ERROR(_str_greens_reduced_me, _str_wrong_l);
  if ((L<0)||(Lambda<0)) greens_ERROR(_str_greens_reduced_me, _str_wrong_args);
  
  double result;
 
  result = greens_ClebschGordan(l_a,double(0.0),Lambda,0.0,l_b,0.0);
  if (result==0.0) return(result);

  double j_a = abs(kappa_a)-0.5, 
        j_b = abs(kappa_b)-0.5;
    
  result = result * greens_w9j(double(l_a), double(0.5), double(j_a), 
                           double(Lambda), double(1.0),   double(L), 
		              double(l_b), double(0.5), double(j_b));
  if (result==0.0) return(result);
   
  result = result *
           sqrt(1.5/Pi) *
	   sqrt((j_a+j_a+1.0)*(j_b+j_b+1.0) *
	   (L+L+1.0)*(Lambda+Lambda+1.0)*(l_a+l_a+1.0));
  return(result);
}

/*!
  The calculation of some phase factor in the multipole expansion of
  electromagnetic field. 
*/
dcomplex greens_xi(int L, int Lambda, int lambda, int electric, int magnetic)
{
  if ((electric!=0) && (electric!=1)) greens_ERROR(_str_greens_xi, _str_wrong_electric);
  if ((magnetic!=0) && (magnetic!=1)) greens_ERROR(_str_greens_xi, _str_wrong_magnetic);
  if ((Lambda<0) || (L<0)) greens_ERROR(_str_greens_xi, _str_wrong_args);
  if ((lambda!=-1)  && (lambda!=+1)) greens_ERROR(_str_greens_xi, _str_wrong_lambda);
  
  if (Lambda==L) return(dcomplex(magnetic*1.0, 0.0));
  if (Lambda==(L-1)) return( electric * cdI *lambda*sqrt(1.0*(L+1.0)/(L+L+1.0)));
  if (Lambda==(L+1)) return(-electric * cdI *lambda*sqrt((1.0*L  )/(L+L+1.0)));

  greens_ERROR(_str_greens_xi, _str_wrong_args);
  return(0.0);
}

//
//
//
// Procedure, which saves the needed (for a defined approximation) radial integrals mU
// to a file   _str_mU_file
//
  int   founded_number = 0;
  int * founded_Lambda_1= NULL; //
  int * founded_Lambda_2= NULL; // There are 
  int * founded_kappa_1 = NULL; // pointers for 
  int * founded_kappa   = NULL; // quantum numbers and
  int * founded_kappa_0 = NULL; //
  matrix_2x2 * founded_mU = NULL; // radial integrals
//
//
//
int greens_TPADRM_save_TP_integrals(double E_p, int LMAX, int electric, int magnetic, 
                                   int lambda_1, int lambda_2, 
                                   int digits, char * _str_mU_file)
{
  dcomplex TPME, smd; 
  double STPME;
  int  l, l_1, kappa_0, number, kappa_1_max, kappa_max;
  double j, j_0, j_1, m, mu_1;
  matrix_2x2 mU;
  
  number = 0;
  kappa_0 = -1;
  j_0 = abs(kappa_0)-0.5;
  kappa_1_max = LMAX+LMAX+1;
  kappa_max = LMAX+1;

  _mU_n2 = 1; // initial main quantum number (actually n_0) and n_0=1 (ground state)
  
  FILE* mU_file = fopen(_str_mU_file, "w+");
  
  fprintf(mU_file, "#relativistic hydrogen radial integrals mU\n\n"); 
  
  fprintf(mU_file, "__E_p:=         %#.10e; #%s\n", E_p, "photon energy, au");
  fprintf(mU_file, "__LMAX:=        %d;     #%s\n", LMAX, "number of multipols");
  fprintf(mU_file, "__kappa_max:=   %d;    #%s\n", kappa_max, "kappa_max");  
  fprintf(mU_file, "__kappa_1_max:= %d;    #%s\n", kappa_1_max, "kappa_1_max");
  fprintf(mU_file, "__electric:=    %d;      #%s\n", electric, "electric multipols included if 1");
  fprintf(mU_file, "__magnetic:=    %d;      #%s\n", magnetic, "magnetic multipols included if 1");
  fprintf(mU_file, "__digits:=      %d;      #%s\n", digits, "precision of numer. int.");
  fprintf(mU_file, "__Digits:=      %d;      #%s\n", int(greens_get_Digits()), "prec.of spec. funct.");
  fprintf(mU_file, "__Z:=           %#.5e;  #%s\n", greens_get_nuclear_charge(), "nuclear charge");
  fprintf(mU_file, "__kappa_0:=     %d;      #%s\n", kappa_0, "initial number kappa_0");
  fprintf(mU_file, "__n_0:=         %d;      #%s\n\n", _mU_n2, "initial number n_0");
  
  fprintf(mU_file, "#number Lambda1 Lambda2 kappa_1 kappa kappa_0 U^{LL} U^{LS} U^{SL} U^{SS}\n");
  fprintf(mU_file, "__mU_table:= [\n");  
  fflush(mU_file);
  

  bool b_mU_not_yet_appeared;  
/*  
  founded_Lambda_1 = (int *)calloc(number+1, sizeof(int));
  founded_Lambda_2 = (int *)calloc(number+1, sizeof(int));
  founded_kappa_1  = (int *)calloc(number+1, sizeof(int));
  founded_kappa    = (int *)calloc(number+1, sizeof(int));
  founded_kappa_0  = (int *)calloc(number+1, sizeof(int));
*/
	  
  STPME = 0.0;  
  for (double mu_0 = -(abs(kappa_0)-.5); mu_0 <= (abs(kappa_0)-.5); mu_0 = mu_0 + 1.){
    
    m = lambda_2 + mu_0;
    mu_1 = lambda_1 + lambda_2 + mu_0;
    
  for (double m_s  = -.5; m_s <= .5; m_s = m_s  + 1.){
  
  TPME  = (double)(0.0);

  for (int kappa_1 = -kappa_1_max; kappa_1 <= kappa_1_max; kappa_1++){ if (kappa_1==0) continue;
    l_1 = greens_angular_l(kappa_1); j_1 = abs(kappa_1)-0.5;
    
  for (int kappa   = -kappa_max; kappa <= kappa_max; kappa++){ if (kappa==0) continue;
    l   = greens_angular_l(kappa);   j = abs(kappa)-0.5;
    
  for (int L_1    = 1; L_1<=LMAX; L_1++){
  for (int L_2    = 1; L_2<=LMAX; L_2++){
  
    for(int Lambda_1 = L_1-1; Lambda_1 <= L_1+1; Lambda_1++){
    for(int Lambda_2 = L_2-1; Lambda_2 <= L_2+1; Lambda_2++){
      
      for(int i1=1; i1<=2; i1++){
      for(int i2=1; i2<=2; i2++){
        
        smd =   pow(cdI, -l_1+L_1+L_2)*sqrt(L_1+L_1+1.0)*sqrt(L_2+L_2+1.0)
	      * 1.0/sqrt(j_1+j_1+1.0)/sqrt(j+j +1.0)
	      * greens_ClebschGordan(l_1, mu_1 - m_s, 0.5, m_s, j_1, mu_1)
	      * greens_ClebschGordan(j, m, L_1, lambda_1, j_1, mu_1)
	      * greens_ClebschGordan(j_0, mu_0, L_2, lambda_2, j, m)
	      * greens_xi(L_1,Lambda_1,lambda_1, electric, magnetic)
              * greens_xi(L_2,Lambda_2,lambda_2, electric, magnetic)
              * greens_RME_T(kappa_1, kappa, Lambda_1, L_1, i1) 
              * greens_RME_T(kappa, kappa_0, Lambda_2, L_2, i2+1);
              
	if (!(smd==(double)(0.0))) {
	  
	  b_mU_not_yet_appeared = true;
          for (int n=0; n<number; n++){
	    if ((founded_Lambda_1[n]==Lambda_1)
	      &&(founded_Lambda_2[n]==Lambda_2)
	      &&(founded_kappa_1[n] ==kappa_1)
	      &&(founded_kappa[n]   ==kappa)
	      &&(founded_kappa_0[n] ==kappa_0)) b_mU_not_yet_appeared=false;}
	  	  
	  if (b_mU_not_yet_appeared){
            
	    number++;
            founded_Lambda_1 = (int *)realloc(founded_Lambda_1, number*sizeof(int));
            founded_Lambda_2 = (int *)realloc(founded_Lambda_2, number*sizeof(int));
            founded_kappa_1  = (int *)realloc(founded_kappa_1,  number*sizeof(int));
            founded_kappa    = (int *)realloc(founded_kappa,    number*sizeof(int));
            founded_kappa_0  = (int *)realloc(founded_kappa_0,  number*sizeof(int));
            
	    founded_Lambda_1[number-1] = Lambda_1;
	    founded_Lambda_2[number-1] = Lambda_2;
	    founded_kappa_1[number-1]  = kappa_1;
	    founded_kappa[number-1]    = kappa;
	    founded_kappa_0[number-1]  = kappa_0;
	    
	  write(number); write(").");
	  
	  write(Lambda_1); write(Lambda_2); 
	  write(kappa_1); write(kappa); write(kappa_0); print(Re(smd));
          	  
          mU = greens_mU(Lambda_1, Lambda_2, E_p, kappa_1, kappa, kappa_0, digits);
	  print(mU); print();
	  
	  fprintf(mU_file, "[%d,  %d,  %d,  %d,  %d,  %d,  %e,  %e,  %e,  %e],\n", 
	                               int(number), 
				       int(Lambda_1), int(Lambda_2), 
	                               int(kappa_1), int(kappa), int(kappa_0),
				       mU.e[0][0], mU.e[0][1], mU.e[1][0], mU.e[1][1]);
          fflush(mU_file);
	  
	  }}
	 //          TPME = TPME + smd;

      }}}}}}}}

//      STPME = STPME + pow(abs(TPME), 2);
      }}
      
      fseek(mU_file, -2, SEEK_CUR);
      fprintf(mU_file, "];\n\n");
      fprintf(mU_file, "__mU_number:= %d; #%s\n\n", number, "number of lines in table");
      
      fclose(mU_file);
      
      free(founded_Lambda_1);  
      free(founded_Lambda_2);
      free(founded_kappa_1);   
      free(founded_kappa); 
      free(founded_kappa_0);
return(1);
}
//
// Load the parameters and radial integrals from file
//
const char* _str_greens_TPADRM_load_TP_integrals = "greens_TPADRM_load_TP_integrals";
const char* _str_cant_fopen = "can't fopen()";
const char* _str_cant_find_E_p = "can't find E_p";
const char* _str_cant_find_LMAX = "can't find LMAX";
const char* _str_cant_find_kappa_max = "can't find kappa_max";
const char* _str_cant_find_kappa_1_max = "can't find kappa_1_max";
const char* _str_cant_find_electric = "can't find electric";
const char* _str_cant_find_magnetic = "can't find magnetic";
const char* _str_cant_find_digits = "can't find digits";
const char* _str_cant_find_Digits = "can't find Digits";
const char* _str_cant_find_Z = "can't find Z";
const char* _str_cant_find_mU_table = "can't find mU_table";
const char* _str_cant_read_mU_table = "can't read mU_table";
const char* _str_rereading = "rereading";
//
//

  double E_p, __nuclear_charge;
  int n_1, LMAX, kappa_max, kappa_1_max, number, Lambda_1, Lambda_2;
  int electric, magnetic, __digits, __Digits;
//
//
int greens_TPADRM_load_TP_integrals(char* _str_mU_file)
{

  bool b__E_p, b__LMAX, b__kappa_max, b__kappa_1_max, 
       b__electric, b__magnetic, b__digits, b__Digits, b__Z, b__mU_table;
       b__E_p = false; b__LMAX = false; b__kappa_max = false;
       b__kappa_1_max = false; b__electric = false; b__magnetic = false;
       b__digits = false; b__Digits = false; b__Z = false, b__mU_table = false;
    
  FILE* mU_file = fopen(_str_mU_file, "r");
  if (mU_file==NULL) {greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_fopen);
                      return(1);}
  
  char* _str;
  _str =  (char*) malloc(200);
  _str[199] = char(0);

  while (!feof(mU_file)) { 
    fscanf(mU_file, "%s", _str); 
    
    if (strncmp(_str, "__E_p", 5)==0) 
      {b__E_p= (fscanf(mU_file, "%le", &E_p)==1); }
    if (strncmp(_str, "__LMAX", 6)==0) 
      {b__LMAX=(fscanf(mU_file, "%d", &LMAX)==1); }
    if (strncmp(_str, "__kappa_max", 11)==0) 
      {b__kappa_max=(fscanf(mU_file, "%d", &kappa_max)==1); }
    if (strncmp(_str, "__kappa_1_max", 13)==0) 
      {b__kappa_1_max=(fscanf(mU_file, "%d", &kappa_1_max)==1); }
    if (strncmp(_str, "__electric", 10)==0) 
      {b__electric=(fscanf(mU_file, "%d", &electric)==1); }
    if (strncmp(_str, "__magnetic", 10)==0) 
      {b__magnetic=(fscanf(mU_file, "%d", &magnetic)==1); }
    if (strncmp(_str, "__digits", 8)==0) 
      {b__digits=(fscanf(mU_file, "%d", &__digits)==1); }
    if (strncmp(_str, "__Digits", 8)==0) 
      {b__Digits=(fscanf(mU_file, "%d", &__Digits)==1); }
    if (strncmp(_str, "__Z", 3)==0) 
      {b__Z=(fscanf(mU_file, "%le", &__nuclear_charge)==1); }
    if (strncmp(_str, "__mU_table", 10)==0) 
      {b__mU_table=true; break;}                      };
  
    if (!b__E_p) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_E_p);
    if (!b__LMAX) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_LMAX);
    if (!b__kappa_max) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_kappa_max);
    if (!b__kappa_1_max) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_kappa_1_max);
    if (!b__electric) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_electric);
    if (!b__magnetic) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_magnetic);
    if (!b__digits) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_digits);
    if (!b__Digits) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_Digits);
    if (!b__Z) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_Z);
    if (!b__mU_table) greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_find_mU_table);

/*    
    print(E_p);
    print(LMAX);
    print(kappa_max);
    print(kappa_1_max);
    print(electric);
    print(magnetic);
    print(__digits);
    print(__Digits);
    print(Z);
*/  
    if (founded_number!=0)
     { 
      greens_WARNING(_str_greens_TPADRM_load_TP_integrals, _str_rereading);
      founded_number = 0;
      
      if (founded_Lambda_1!=NULL) free(founded_Lambda_1); //
      if (founded_Lambda_2!=NULL) free(founded_Lambda_2); // There are 
      if (founded_kappa_1!=NULL) free(founded_kappa_1); // pointers for 
      if (founded_kappa != NULL) free(founded_kappa); // quantum numbers and
      if (founded_kappa_0 != NULL) free(founded_kappa_0); //
      if (founded_mU != NULL) free(founded_mU); // radial integrals
      
     }
    
    founded_Lambda_1= NULL; //
    founded_Lambda_2= NULL; // There are 
    founded_kappa_1 = NULL; // pointers for 
    founded_kappa   = NULL; // quantum numbers and
    founded_kappa_0 = NULL; //
    founded_mU = NULL; // radial integrals
    
    fscanf(mU_file, "%s\n", _str);

    for(int i=0; !feof(mU_file) ; i++)
    {
    founded_Lambda_1 = (int *) realloc(founded_Lambda_1, (i+1)*sizeof(int));
    founded_Lambda_2 = (int *) realloc(founded_Lambda_2, (i+1)*sizeof(int));
    founded_kappa_1  = (int *) realloc(founded_kappa_1, (i+1)*sizeof(int));
    founded_kappa    = (int *) realloc(founded_kappa, (i+1)*sizeof(int));
    founded_kappa_0  = (int *) realloc(founded_kappa_0, (i+1)*sizeof(int));
    founded_mU       = (matrix_2x2 *) realloc(founded_mU, (i+1)*sizeof(matrix_2x2));

    if ((fscanf(mU_file, "[%d,%d,%d,%d,%d,%d,%le,%le,%le,%le\n", 
      &number, &founded_Lambda_1[i], 
      &founded_Lambda_2[i],
      &founded_kappa_1[i], 
      &founded_kappa[i], 
      &founded_kappa_0[i],
      &founded_mU[i].e[0][0], 
      &founded_mU[i].e[0][1], 
      &founded_mU[i].e[1][0],
      &founded_mU[i].e[1][1])!=10))
      greens_ERROR(_str_greens_TPADRM_load_TP_integrals, _str_cant_read_mU_table);
    fscanf(mU_file, "%s\n", _str);
/*  
    printf("%d %d, %d, %d, %d, %d, %e, %e, %e, %e\n", 
                         number, founded_Lambda_1[i], founded_Lambda_2[i], 
                         founded_kappa_1[i], founded_kappa[i], founded_kappa_0[i],
			 founded_mU[i].e[0][0], founded_mU[i].e[0][1], 
			 founded_mU[i].e[1][0], founded_mU[i].e[1][1]);
*/
    if (strncmp(_str, "]];", 3)==0) {founded_number=(i+1); break;} }
    write("number_of_integrals_readed"); print(founded_number);
    fclose(mU_file);
/*
    free(founded_Lambda_1); //
    free(founded_Lambda_2); // There are 
    free(founded_kappa_1); // pointers for 
    free(founded_kappa); // quantum numbers and
    free(founded_kappa_0); //
    free(founded_mU); // radial integrals*/
    
    free(_str);
    
  return(0);
}

//
// Table of radial integrals as a function
//

const char* _str_greens_mU = "greens_mU";
const char* _str_radial_integral_not_found = "radial_integral_not_found";

matrix_2x2 greens_mU(int Lambda_1, int Lambda_2, int kappa_1, int kappa, int kappa_0)
{
  for (int i=0; i<founded_number; i++){
    if ((founded_Lambda_1[i]==Lambda_1) &&
        (founded_Lambda_2[i]==Lambda_2) &&
	(founded_kappa_1[i]==kappa_1) &&
	(founded_kappa[i]==kappa) &&
	(founded_kappa_0[i]==kappa_0)) return(founded_mU[i]);}
  greens_WARNING(_str_greens_mU, _str_radial_integral_not_found);
  return(matrix_2x2(0,0,0,0)); 
}
//
// Coulomb phase Delta_kappa
//

dcomplex greens_Delta_kappa(int kappa)
{
  double eta, s, W, alphaZ = greens_constant_alpha*greens_get_nuclear_charge();
  
  W = (greens_energy(1, -1)+2.0*E_p)/greens_constant_au_E0+1.0;
   
  eta = alphaZ*W/sqrt(W*W-1.0);
  s = sqrt(kappa*kappa-alphaZ*alphaZ);
  
  dcomplex result = 0.5*argument((-kappa+I*eta/W)/(s+I*eta))-
                   argument(GAMMA(s+I*eta)) + Pi*(greens_angular_l(kappa)+1.0-s)*0.5;
  return(result);
}

//
// two-photon differential cross section
//
const char* _str_greens_TPADRM_dsigma_dOmega = "greens_TPADRM_dsigma_dOmega";
//
//
double greens_TPADRM_dsigma_dOmega(double theta, double phi, int lmax,
                                       int lambda_1, int lambda_2, int electr, int magn)
{
  double result, c1, c2, c3, c4, c5;  int kappa_0;
  dcomplex ME, smd, ccY, cc1, cc2, cc3;
  double m, mu_1, j_1, j, j_0;
  int l, l_1; 
  
  double __Z_save = greens_get_nuclear_charge();
  greens_save_mwe_levels();  msglevel=NoPrint;
  greens_set_nuclear_charge(__nuclear_charge);
  
  kappa_0 = -1;
  j_0 = abs(kappa_0)-0.5;
  kappa_1_max = lmax+lmax+1;
  kappa_max = lmax+1;
  

  result=0.0;
  for (double mu_0 = -(abs(kappa_0)-.5); mu_0 <= (abs(kappa_0)-.5); mu_0 = mu_0 + 1.){
    
    m = lambda_2 + mu_0;
    mu_1 = lambda_1 + lambda_2 + mu_0;
  
  for (double m_s  = -.5; m_s <= .5; m_s = m_s  + 1.){
  
  ME  = 0.0;
  
  for (int kappa_1 = -kappa_1_max; kappa_1 <= kappa_1_max; kappa_1++){ if (kappa_1==0) continue;
    l_1 = greens_angular_l(kappa_1); j_1 = abs(kappa_1)-0.5;
    if (l_1<abs(int(mu_1-m_s))) continue;
    c1 = greens_ClebschGordan(l_1, mu_1 - m_s, 0.5, m_s, j_1, mu_1);
    if (c1 == 0.0) continue;
    ccY = greens_Ylm(l_1, int(mu_1-m_s), theta,phi);
    if (ccY == 0.0) continue;    
    cc1 = exp(cdI * greens_Delta_kappa(kappa_1));
    
  for (int kappa   = -kappa_max; kappa <= kappa_max; kappa++){ if (kappa==0) continue;
    l   = greens_angular_l(kappa);   j = abs(kappa)-0.5;
    
  for (int L_1    = 1; L_1<=lmax; L_1++){
    c2 = greens_ClebschGordan(j, m, L_1, lambda_1, j_1, mu_1);
    if (c2 == 0.0) continue;
  for (int L_2    = 1; L_2<=lmax; L_2++){
    c3 = greens_ClebschGordan(j_0, mu_0, L_2, lambda_2, j, m);
    if (c3 == 0.0) continue;
    
    for(int Lambda_1 = L_1-1; Lambda_1 <= L_1+1; Lambda_1++){
    cc2 = greens_xi(L_1,Lambda_1,lambda_1, electr, magn);
    for(int Lambda_2 = L_2-1; Lambda_2 <= L_2+1; Lambda_2++){
    cc3 = greens_xi(L_2,Lambda_2,lambda_2, electr, magn);
      
      for(int i1=1; i1<=2; i1++){
        c4 = _greens_RME_T(kappa_1, kappa, Lambda_1, L_1, i1);
        if (c4==0) continue;
      
      for(int i2=1; i2<=2; i2++){
        c5 = _greens_RME_T(kappa, kappa_0, Lambda_2, L_2, i2+1);
	if (c5==0) continue;
	
        smd = ccY * cc1 * cc2 * cc3 * c1 * c2 * c3 *  c4 * c5 * pow_minus(i1+i2)
            * pow(I, -l_1+L_1+L_2)*sqrt((L_1+L_1+1.0)*(L_2+L_2+1.0)) / 
	      sqrt((j_1+j_1+1.0)*(j+j +1.0));
              
	 if (smd == 0.0) continue;
         ME = ME + smd*greens_mU(Lambda_1, Lambda_2, kappa_1, kappa, kappa_0).e[i1-1][i2-1];

      }}}}}}}}
      result = result + 32.0*pow(Pi, 3.0)*pow(abs(ME), 2.0);
      }}

  greens_set_nuclear_charge(__Z_save);
  greens_load_mwe_levels();
return(result);
}


double greens_TPADRM_dsigma_dOmega_nonpolarized(double theta, double phi, 
                                                    int lmax, int electr, int magn)
{
  double result, c1, c2, c3, c4, c5;  int kappa_0;
  dcomplex ME, smd, ccY, cc1, cc2, cc3;
  double m, mu_1, j_1, j, j_0;
  int l, l_1;
  
  double __Z_save = greens_get_nuclear_charge();
  greens_save_mwe_levels();  msglevel=NoPrint;
  greens_set_nuclear_charge(__nuclear_charge);
  
  kappa_0 = -1;
  j_0 = abs(kappa_0)-0.5;
  kappa_1_max = lmax+lmax+1;
  kappa_max = lmax+1;
  

  result=0.0;
  for (int lambda_1 = -1; lambda_1<=1; lambda_1++){ if (lambda_1==0) continue;
  for (int lambda_2 = -1; lambda_2<=1; lambda_2++){ if (lambda_2==0) continue;
  for (double mu_0 = -(abs(kappa_0)-.5); mu_0 <= (abs(kappa_0)-.5); mu_0 = mu_0 + 1.){
    
    m = lambda_2 + mu_0;
    mu_1 = lambda_1 + lambda_2 + mu_0;
  
  for (double m_s  = -.5; m_s <= .5; m_s = m_s  + 1.){
  
  ME  = 0.0;
  
  for (int kappa_1 = -kappa_1_max; kappa_1 <= kappa_1_max; kappa_1++){ if (kappa_1==0) continue;
    l_1 = greens_angular_l(kappa_1); j_1 = abs(kappa_1)-0.5;
    if (l_1<abs(int(mu_1-m_s))) continue;
    c1 = greens_ClebschGordan(l_1, mu_1 - m_s, 0.5, m_s, j_1, mu_1);
    if (c1 == 0.0) continue;
    ccY = greens_Ylm(l_1, int(mu_1-m_s), theta,phi);
    if (ccY == 0.0) continue;    
    cc1 = exp(I*greens_Delta_kappa(kappa_1));
    
  for (int kappa   = -kappa_max; kappa <= kappa_max; kappa++){ if (kappa==0) continue;
    l   = greens_angular_l(kappa);   j = abs(kappa)-0.5;
    
  for (int L_1    = 1; L_1<=lmax; L_1++){
    c2 = greens_ClebschGordan(j, m, L_1, lambda_1, j_1, mu_1);
    if (c2 == 0.0) continue;
  for (int L_2    = 1; L_2<=lmax; L_2++){
    c3 = greens_ClebschGordan(j_0, mu_0, L_2, lambda_2, j, m);
    if (c3 == 0.0) continue;
    
    for(int Lambda_1 = L_1-1; Lambda_1 <= L_1+1; Lambda_1++){
    cc2 = greens_xi(L_1,Lambda_1,lambda_1, electr, magn);
    for(int Lambda_2 = L_2-1; Lambda_2 <= L_2+1; Lambda_2++){
    cc3 = greens_xi(L_2,Lambda_2,lambda_2, electr, magn);
      
      for(int i1=1; i1<=2; i1++){
        c4 = _greens_RME_T(kappa_1, kappa, Lambda_1, L_1, i1);
        if (c4==0) continue;
      
      for(int i2=1; i2<=2; i2++){
        c5 = _greens_RME_T(kappa, kappa_0, Lambda_2, L_2, i2+1);
	if (c5==0) continue;
	
        smd =   ccY * cc1 * cc2 * cc3 * c1 * c2 * c3 *  c4 * c5 * pow_minus(i1+i2)
              * pow(cdI, -l_1+L_1+L_2)*sqrt((L_1+L_1+1.0)*(L_2+L_2+1.0))
	      / sqrt((j_1+j_1+1.0)*(j+j +1.0));
              
	 if (smd == 0.0) continue;
         ME = ME + smd*greens_mU(Lambda_1, Lambda_2, kappa_1, kappa, kappa_0).e[i1-1][i2-1];

      }}}}}}}}
      result = result + 8.0*pow(Pi, 3.0)*pow(abs(ME), 2.0);
      }}}}

  greens_set_nuclear_charge(__Z_save);
  greens_load_mwe_levels();
return(result);
}
