//
//
//
bool operator ==(CVECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  bool res = true;
  
  for (size_t i = 0; i<a.dim; i++){
    res = res && (a.e[i] == b.e[i]);
    if (!res) break;}

  return(res);
}
//
//
//
bool operator ==(CVECTOR a, COMPLEX b)
{
  bool res = true;
  
  for (size_t i = 0; i<a.dim; i++){
    res = res && a.e[i] == b;
    if (!res) break;}

  return(res);
}

//
//
//
bool operator ==(COMPLEX a, CVECTOR b)
{
  bool res = true;
  
  for (size_t i = 0; i<b.dim; i++){
    res = res && b.e[i] == a;
    if (!res) break;}

  return(res);
}
//
//
//
bool operator ==(CVECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  bool res = true;
  
  for (size_t i = 0; i<a.dim; i++){
    res = res && (a.e[i] == b.e[i]);
    if (!res) break;}

  return(res);
}

//
//
//
bool operator ==(VECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  bool res = true;
  
  for (size_t i = 0; i<a.dim; i++){
    res = res && (a.e[i] == b.e[i]);
    if (!res) break;}

  return(res);
}

//
//
//
bool operator ==(REAL a, CVECTOR b)
{
  bool res = true;
  
  for (size_t i = 0; i<b.dim; i++){
    res = res && (a == b.e[i]);
    if (!res) break;}

  return(res);
}

//
//
//
bool operator ==(CVECTOR b, REAL a)
{
  bool res = true;
  
  for (size_t i = 0; i<b.dim; i++){
    res = res && (a == b.e[i]);
    if (!res) break;}

  return(res);
}

//
//
//
CVECTOR operator +(CVECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]+b.e[i];
  return(res);  
}
//
//
//
CVECTOR operator +(CVECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]+b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator +(VECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]+b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator +(CVECTOR a, COMPLEX b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]+b;
  return(res);  
}

//
//
//
CVECTOR operator +(CVECTOR a, REAL b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]+b;
  return(res);  
}

//
//
//
CVECTOR operator +(COMPLEX a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = b.e[i]+a;
  return(res);  
}
//
//
//
CVECTOR operator +(REAL a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = b.e[i]+a;
  return(res);  
}

//
//
//
CVECTOR operator -(CVECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator -(CVECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator -(VECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator -(CVECTOR a, COMPLEX b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b;
  return(res);  
}
//
//
//
CVECTOR operator -(CVECTOR a, REAL b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b;
  return(res);  
}

//
//
//
CVECTOR operator -(COMPLEX a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a-b.e[i];
  return(res);  
}
//
//
//
CVECTOR operator -(REAL a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a-b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator -(CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = -b.e[i];
  return(res);  
}

//
//
//
COMPLEX operator *(CVECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  COMPLEX res = 0;
  for(size_t i=0; i<a.dim; i++) res = res+a.e[i]*conjugate(b.e[i]);
  return(res);  
}
//
//
//
COMPLEX operator *(CVECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  COMPLEX res = 0;
  for(size_t i=0; i<a.dim; i++) res = res+a.e[i]*b.e[i];
  return(res);  
}
//
//
//
COMPLEX operator *(VECTOR a, CVECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  COMPLEX res = 0;
  for(size_t i=0; i<a.dim; i++) res = res+a.e[i]*b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator *(CVECTOR a, COMPLEX b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]*b;
  return(res);  
}
//
//
//
CVECTOR operator *(CVECTOR a, REAL b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]*b;
  return(res);  
}

//
//
//
CVECTOR operator *(COMPLEX a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a*b.e[i];
  return(res);  
}
//
//
//
CVECTOR operator *(REAL a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a*b.e[i];
  return(res);  
}

//
//
//
CVECTOR operator /(CVECTOR a, COMPLEX b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]/b;
  return(res);  
}
//
//
//
CVECTOR operator /(CVECTOR a, REAL b)
{
  CVECTOR res = CVECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]/b;
  return(res);  
}

//
//
//
CVECTOR operator /(COMPLEX a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a/b.e[i];
  return(res);  
}
//
//
//
CVECTOR operator /(REAL a, CVECTOR b)
{
  CVECTOR res = CVECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a/b.e[i];
  return(res);  
}

//
//
//
COMPLEX dotprod(CVECTOR a, CVECTOR b){return(a*b);}
//
//
//
COMPLEX dotprod(CVECTOR a, VECTOR b){return(a*b);}
//
//
//
COMPLEX dotprod(VECTOR a, CVECTOR b){return(a*b);}


REAL abs(CVECTOR a){return(sqrt(abs(a*a)));}
//
//
//
VECTOR Re(CVECTOR a)
{
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i].re;
  return(res);  
}
//
//
//
VECTOR Im(CVECTOR a)
{
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i].im;
  return(res);  
}

void print(CVECTOR a)
{
  cout << "\n" << "[";
  for(size_t i=0; i<a.dim-1; i++) {
    cout << a.e[i].re << " + " << a.e[i].im << "*I"<< ", "; }
  cout << a.e[a.dim-1].re << " + " << a.e[a.dim-1].im << "*I"<< "]\n";
}
