
 bool operator ==(COMPLEX a, COMPLEX b){return((a.re==b.re)&&(a.im==b.im));}
 bool operator ==(REAL a, COMPLEX b){return((a==b.re)&&(0.0==b.im));}
 bool operator ==(COMPLEX a, REAL b){return((a.re==b)&&(a.im==0.0));}

 COMPLEX operator +(COMPLEX a, COMPLEX b) {
   return(COMPLEX(a.re+b.re, a.im+b.im));}
 COMPLEX operator +(REAL a, COMPLEX b){return(COMPLEX(a+b.re, b.im));}
 COMPLEX operator +(COMPLEX b, REAL a){return(COMPLEX(a+b.re, b.im));}

 COMPLEX operator -(COMPLEX a, COMPLEX b){return(COMPLEX(a.re-b.re, a.im-b.im));}
 COMPLEX operator -(REAL a, COMPLEX b){return(COMPLEX(a-b.re, -b.im));}
 COMPLEX operator -(COMPLEX b, REAL a){return(COMPLEX(b.re-a, b.im));}

 COMPLEX operator -(COMPLEX a){return(COMPLEX(-a.re, -a.im));}
 
 COMPLEX operator *(COMPLEX a, COMPLEX b){COMPLEX c; REAL P, ac, bd;
   P=(a.re+a.im)*(b.re+b.im); ac=a.re*b.re; bd=a.im*b.im; c.re = ac-bd; 
   c.im = P-ac-bd; return(c);}
 COMPLEX operator *(REAL a, COMPLEX b){return(COMPLEX(a*b.re, a*b.im));}
 COMPLEX operator *(COMPLEX b, REAL a){return(COMPLEX(a*b.re, a*b.im));}

// abs abs for REAL argument
 REAL abs(REAL a){if (a<0.0) {return(-a);} else return(a);}
//
//
//  
COMPLEX operator /(COMPLEX a,COMPLEX b)
{
  COMPLEX c; REAL Q, denom;
  if (abs(b.re)<abs(b.im)) {
    Q=b.re/b.im;
    denom=(b.re*Q+b.im);
    c.re=(a.re*Q+a.im)/denom;
    c.im=(a.im*Q-a.re)/denom;}
else {
    Q=b.im/b.re;
    denom=b.im*Q+b.re;
    c.re=(a.im*Q+a.re)/denom;
    c.im=(a.im-a.re*Q)/denom;};  
  return(c);
}
//
//
//
COMPLEX operator /(REAL a, COMPLEX b)
{
  COMPLEX c; REAL Q, denom;
  if (abs(b.re)<abs(b.im)) {
    Q=b.re/b.im;
    denom=(b.re*Q+b.im);
    c.re=(a*Q)/denom;
    c.im=-a/denom; }
else {
    Q=b.im/b.re;
    denom=b.im*Q+b.re;
    c.re=a/denom;
    c.im=-a*Q/denom;};  
  return(c);
}
//
//
//
COMPLEX operator /(COMPLEX a, REAL b) {return(COMPLEX(a.re/b, a.im/b));}

//
//**********************************************************************
//
COMPLEX exp(COMPLEX a)
{ REAL mag = exp(a.re); return(COMPLEX(mag*cos(a.im),mag*sin(a.im)));}
//
//
//
REAL abs(COMPLEX a)
{
 REAL absb=abs(a.re); REAL absa=abs(a.im);
 if (a==COMPLEX(0.0,0.0)) return(0.0);
  if (absa>absb) {absb=absb/absa; return(absa*sqrt(1.0+absb*absb));} 
else {absa=absa/absb; return(absb*sqrt(1.0+absa*absa));}
}
//
//
//
REAL Re(COMPLEX a){return(a.re);}
REAL Im(COMPLEX a){return(a.im);}
REAL argument(COMPLEX z){
  REAL a=z.re, b=z.im;
  if (a>0) return(atanl(b/a));
  if ((a!=0.0) && (b>=0.0)) return(Pi+atan(b/a));
  if ((a!=0.0) && (b< 0.0)) return(-Pi+atan(b/a));
  if ((a==0.0) && (b> 0.0)) return(Pi2);
  if ((a==0.0) && (b< 0.0)) return(-Pi2);
  return(0.0);}
  
COMPLEX conjugate(COMPLEX a){return(COMPLEX(a.re, -a.im));}

COMPLEX polar(REAL r, REAL phi){return(COMPLEX(r*cos(phi), r*sin(phi)));}
COMPLEX cartesian(REAL x, REAL y){return(COMPLEX(x, y));}

COMPLEX cos(COMPLEX z){return(COMPLEX(cos(z.re)*cosh(z.im),-sin(z.re)*sinh(z.im)));}
COMPLEX sin(COMPLEX z){return(COMPLEX(sin(z.re)*cosh(z.im), cos(z.re)*sinh(z.im)));}

COMPLEX tan(COMPLEX z)
{ REAL denom; REAL twore=z.re+z.re, twoim=z.im+z.im;
  denom=cos(twore)+cosh(twoim);
  return(COMPLEX( sin(twore)/denom, sinh(twoim)/denom ));}

COMPLEX csc(COMPLEX z){return(1.0/sin(z));}
COMPLEX sec(COMPLEX z){return(1.0/cos(z));}
COMPLEX cot(COMPLEX z){return(1.0/tan(z));}

COMPLEX log(COMPLEX z){COMPLEX c; c.re=log(abs(z)); c.im=atan2(z.im,z.re); return(c);}
COMPLEX Clog(REAL z){return(COMPLEX(log(abs(z)), atan2(0.0,z)));}
COMPLEX ln (COMPLEX z){return(log(z));}

REAL sign(REAL z){if (z>0) {return(1.0);} 
                else {if (z==0) {return(0.0);} else return(-1.0);};}

COMPLEX sqrt(COMPLEX z)
{ 
  COMPLEX res; REAL t=sqrt((abs(z.re)+abs(z))*0.5);
  if (z.re>0.0) {res.re=t; res.im=z.im/(t+t);} 
else {res.re=abs(z.im)/(t+t);
      if (z.im>=0.0) {res.im=t;} else {res.im=-t;}}
  return(res);
}

COMPLEX Csqrt(REAL z){COMPLEX c;
  if (z>=0.0) {c.re=sqrt(abs(z)); c.im=0.0;} 
else {c.re=0.0; c.im=sqrt(abs(z));}; return(c);}

COMPLEX pow(COMPLEX z, COMPLEX a) {return(exp(a*log(z)));}
COMPLEX pow(COMPLEX z, REAL a) {return(exp(a*log(z)));}
COMPLEX pow(REAL z, COMPLEX a) {return(exp(a*log(COMPLEX(z,0.0))));}
COMPLEX pow(COMPLEX z, int a)  {return(exp(a*log(z)));}
COMPLEX pow(int z, COMPLEX a)  {return(exp(a*log((REAL)(z))));}
COMPLEX pow(COMPLEX z, long a) {return(exp(a*log(z)));}
COMPLEX pow(long z, COMPLEX a) {return(exp(a*log((REAL)(z))));}

COMPLEX Cpow(REAL z, REAL a) {return(exp(a*log(COMPLEX(z,0.0))));}
COMPLEX Cpow(REAL z, COMPLEX a) {return(exp(a*log(COMPLEX(z,0.0))));}
COMPLEX Cpow(COMPLEX z, REAL a) {return(exp(a*log(z)));}
COMPLEX Cpow(COMPLEX z, COMPLEX a) {return(exp(a*log(z)));}

COMPLEX lnpow(COMPLEX z, COMPLEX a) {return(a*log(z));}
COMPLEX lnpow(REAL z, COMPLEX a) {return(a*log(COMPLEX(z,0.0)));}
COMPLEX lnpow(COMPLEX z, REAL a) {return(a*log(z));}
COMPLEX Clnpow(REAL z, REAL a) {return(a*log(COMPLEX(z,0.0)));}

COMPLEX operator ^(COMPLEX z, long p)
{COMPLEX c=COMPLEX(1.0, 0.0);
 for(long i=0; i<abs(p); i++){c=c*z;} if (p>=0) {return(c);} return(1.0/c);}
COMPLEX operator ^(COMPLEX z, COMPLEX a){return(exp(a*log(z)));}
COMPLEX operator ^(COMPLEX z, REAL a){return(exp(a*log(z)));}
COMPLEX operator ^(REAL z, COMPLEX a){return(exp(a*log(z)));}

COMPLEX acos(COMPLEX z){return(__CI*log(z-__CI*sqrt(1.0-z*z)));}
COMPLEX asin(COMPLEX z){return(__CI*log(sqrt(1.0-z*z)-__CI*z));}
COMPLEX atan(COMPLEX z){return(__CI*0.5*log((__CI+z)/(__CI-z)));}

COMPLEX cosh(COMPLEX z){COMPLEX c; c.re=cosh(z.re)*cos(z.im); c.im=sinh(z.re)*sin(z.im); return(c);}
COMPLEX sinh(COMPLEX z){COMPLEX c; c.re=sinh(z.re)*cos(z.im); c.im=cosh(z.re)*sin(z.im); return(c);}
COMPLEX tanh(COMPLEX z)
{
  COMPLEX c; REAL denom; REAL twore=z.re+z.re, twoim=z.im+z.im;
  denom=cosh(twore)+cos(twoim);
  c.re=sinh(twore)/denom; c.im=sin(twoim)/denom; return(c);
}
