#ifndef __GREENS_DCOMPLEX_HPP
#define __GREENS_DCOMPLEX_HPP

#include "greens_const.hpp"
#include <math.h>

#undef COMPLEX
#undef REAL

#define COMPLEX dcomplex
#define REAL    double
#include "greens_complex_template.hpp"

//
// ------------- complex constants -----------------------------------------------
//
const COMPLEX cdI     = COMPLEX(0.0,1.0);
const COMPLEX I       = COMPLEX(0.0,1.0);

#define __CI     COMPLEX(0.0,1.0)

#endif 
