#include "greens_nonrelme.hpp"
/*!
  The radial bound-bound matrix element due to the Bethe and Solpiter

  \int_0^{\infty}(R_{n1 l1}(r) * r * R_{n2 l2}(r) r^2 \,dr
  \int_0^{\infty}(P_{n1 l1}(r) * r * P_{n2 l2}(r) \,dr
 
  It is sometimes possible to calculate the bound-bound matrix element analytically.
  However, a version with numerical integration is supported.

  Nuclear charge of both states can be controlled by 
  greens_set_nuclear_charge() function.
*/
double greens_nonrel_radial_bound_bound_me (int n1, int l1, int n2, int l2)
{
  double Z, t2, t4, t7, t10, t12, t16, t23, t27, 
                 t30, t36, t41, t43, t45, t48;
		 
  int t28, t29, t34, t35;
  
  if ((n1<1)||(n2<1)) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n);
  if ((l1<0)||(l2<0)) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_l);
  if ((n1<=l1)||(n2<=l2)) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n_or_l);

      Z   = greens_save_nuclear_charge;
      t2  = pow(double(n1),2.0+l2);      
      t4  = factorial(n1-l1-1);
      t7  = factorial(n1+l1);
      t10 = factorial(n2-l2-1);
      t12 = factorial(n2+l2);
      t16 = sqrt(t7*t10/t12/t4);
      t23 = pow(2.0,l1+l2);
      t27 = pow(double(n2),2.0+l1);
      t28 = n2+n1;
      t29 = -4-l1-l2;
      t30 = pow(double(t28),double(t29));
      t34 = l1+1;
      t35 = l2+1;
      t36 = 1.0/t28;
      t41 = greens_F2(-t29,-n1+l1+1,-n2+l2+1,2*t34,t35+t35,(n2+n2)*t36,(n1+n1)*t36);
      t43 = factorial(t35+t35-1.0);
      t45 = factorial(t34+t34-1.0);
      t48 = factorial(-t29-1);
      last_dres = 4.0*t2*t16/t10*t12/Z*t23*t27*t30*t41/t43/t45*t48;

      last_corr=true;
  return(last_dres);
}

//
// Numerical version of the bound-bound matrix element
//
int _nonrel_radial_n1; int _nonrel_radial_l1;
int _nonrel_radial_n2; int _nonrel_radial_l2;

double __nonrel_radial_bound_bound_integrand(double r)
{  return(greens_radial_orbital(_nonrel_radial_n1, _nonrel_radial_l1, r)*r*
          greens_radial_orbital(_nonrel_radial_n2, _nonrel_radial_l2, r));};

/*!
  The radial bound-bound matrix element due to the Bethe and Solpiter

  \int_0^{\infty}(R_{n1 l1}(r) * r * R_{n2 l2}(r) r^2 \,dr
  \int_0^{\infty}(P_{n1 l1}(r) * r * P_{n2 l2}(r) \,dr
 
  It is sometimes possible to calculate the bound-bound matrix element analytically.
  However, a version with numerical integration is supported.

  This matrix element should return the same result as the function
  
  greens_nonrel_radial_bound_bound_me(int n1, int l1, 
                                     int n2, int l2)
  
  Nuclear charge of both states can be controlled by 
  greens_set_nuclear_charge() function.
*/
double greens_nonrel_radial_bound_bound_me(int n1, int l1, 
                                          int n2, int l2, int digits)
{
  if ((n2<1)||(n1<1)) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n);
  if ((l1<0)||(l2<0))   greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_l);
  if ((n2<=l2)||(n1<=l1)) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n_or_l);
  
  _nonrel_radial_n1 = n1; _nonrel_radial_l1 = l1;
  _nonrel_radial_n2 = n2; _nonrel_radial_l2 = l2;

  _GLeg_Piece=5.0/greens_save_nuclear_charge;
  
  last_dres=greens_Gauss_Legendre_Int(__nonrel_radial_bound_bound_integrand, 0.0, digits);
  last_corr=true; return(last_dres);
}

//
// radial matrix element due to the Bethe and Solpiter
// int(R(E1,l1,r)*r*R(n2,l2,r)*r^2, r=0..infinity) or 
// int(P(E1,l1,r)*r*P(n2,l2,r)    , r=0..infinity)
// bound - free, now only numerically, because the analytic matrix element
// is not converged with today greens_F2()
//

double _nonrel_radial_E1;

//
//

double __nonrel_radial_free_bound_integrand(double r)
{  return(greens_radial_orbital(_nonrel_radial_E1, _nonrel_radial_l1, r)*r*
          greens_radial_orbital(_nonrel_radial_n2, _nonrel_radial_l2, r));};

/*!
  The radial free-bound matrix element due to the Bethe and Solpiter

  \int_0^{\infty}(R_{E l1}(r) * r * R_{n2 l2}(r) r^2 \,dr
  \int_0^{\infty}(P_{E l1}(r) * r * P_{n2 l2}(r) \,dr
 
  A version with numerical integration is supported.
 
  Nuclear charge of both states can be controlled by 
  greens_set_nuclear_charge() function.
*/
double greens_nonrel_radial_free_bound_me (double n1, int l1, 
                                             int n2, int l2, int digits)
{
  if (n2<1) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n);
  if (n1<0.0) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_energy);
  if ((l1<0)||(l2<0))   greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_l);
  if (n2<=l2)           greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n_or_l);

  _nonrel_radial_E1 = n1; _nonrel_radial_l1 = l1;
  _nonrel_radial_n2 = n2; _nonrel_radial_l2 = l2;

  _GLeg_Piece=5.0/greens_save_nuclear_charge;

  last_dres=greens_Gauss_Legendre_Int(__nonrel_radial_free_bound_integrand, 0.0, digits);
  last_corr=true;
  return(last_dres);
}

//
// <\psi_f| u_{\lambda} \br | \psi_i>
// 
/*!
  The complete bound-bound matrix element supported
  for transition operator 
   
  u_{\lambda} r

  \int_0^{\infty}(R_{E l1}(r) * r * R_{n2 l2}(r) r^2 \,dr
  \int_0^{\infty}(P_{E l1}(r) * r * P_{n2 l2}(r) \,dr
 
  A version with analytical integration is supported.
 
  Nuclear charge of both states can be controlled by 
  greens_set_nuclear_charge() function.
*/
double greens_nonrel_bound_bound_me(int n_f, int l_f, int m_f, int lambda, 
                                   int n_i, int l_i, int m_i)
{
   double w3jw3j, mR;
   w3jw3j=greens_w3j(l_f, 1, l_i, 0, 0, 0)*
          greens_w3j(l_f, 1, l_i, -m_f, lambda, m_i)*pow_minus(m_f)*
	  sqrt(l_f+l_f+1.0)*sqrt(l_i+l_i+1.0);
   
   if (w3jw3j!=0.0) {mR=greens_nonrel_radial_bound_bound_me(n_f, l_f, n_i, l_i);} else mR=1.0;
   last_dres=w3jw3j*mR;
   return(last_dres);
}


//
// <\psi_f| \bveps_{\lambda} \br | \psi_i>
// 
/*!
  The complete bound-bound matrix element supported
  for transition operator 
   
  u_{\lambda} r

  <\psi_{f}| \bveps_{\lambda} \br | \psi_i> 

  \int_0^{\infty}(R_{E l1}(r) * r * R_{n2 l2}(r) r^2 \,dr
  \int_0^{\infty}(P_{E l1}(r) * r * P_{n2 l2}(r) \,dr
 
  A version with numerical integration is supported.
 
  Nuclear charge of both states can be controlled by 
  greens_set_nuclear_charge() function.
*/
double greens_nonrel_bound_bound_me(int n_f, int l_f, int m_f, int lambda, 
                                   int n_i, int l_i, int m_i, int digits)
{
   double w3jw3j, mR;
   w3jw3j = greens_w3j(l_f, 1, l_i, 0,0,0)*
            greens_w3j(l_f, 1, l_i, -m_f,lambda,m_i)*pow_minus(m_f)*
	    sqrt(l_f+l_f+1.0)*sqrt(l_i+l_i+1.0);
   
   if (w3jw3j!=0.0) {mR=greens_nonrel_radial_bound_bound_me(n_f, l_f, n_i, l_i, digits);} 
 else mR=1.0;
   last_dres=w3jw3j*mR; return(last_dres);
}

//
// <\psi_{f}| \bveps_{\lambda} \br | \psi_i>
//
//
// <\psi_f| \bveps_{\lambda} \br | \psi_i>
// 
/*!
  The complete free-bound matrix element supported
  for transition operator 
   
  u_{\lambda} r
 
  <\psi_{f}| \bveps_{\lambda} \br | \psi_i> 

  A version with numerical integration is supported.
 
  Nuclear charge of both states can be controlled by 
  greens_set_nuclear_charge() function.
*/
double greens_nonrel_free_bound_me(double E_f, int l_f, int m_f, int lambda, 
                                              int n_i, int l_i, int m_i, int digits)
{
   double w3jw3j, mR;
   w3jw3j=greens_w3j(l_f, 1, l_i, 0,0,0)*
          greens_w3j(l_f, 1, l_i, -m_f,lambda,m_i)*
          pow_minus(m_f)*sqrt(l_f+l_f+1.0)*sqrt(l_i+l_i+1.0);
   
   if (w3jw3j!=0.0) {mR=greens_nonrel_radial_free_bound_me(E_f, l_f, n_i, l_i, digits);} else mR=1.0;
   last_dres=w3jw3j*mR;
   return(last_dres);
}




/* 
// radial matrix element due to the Bethe and Solpiter
// int(R(E1,l1,r)*r*R(n2,l2,r)*r^2, r=0..infinity) or 
// int(P(E1,l1,r)*r*P(n2,l2,r)    , r=0..infinity)
// free-bound but analytical unfortunately greens_F2 with defined double series
// is not convergent. Therefore the matrix element is commented... 
// this matrix element could be calculate with some other greens_F2, like maple
// function of these type.
//
double greens_nonrel_radial_free_bound_me (double n1, long l1, long n2, long l2)
{

  double Z, t2, t3, t6, t9, t10, t11, t13, t15, t16, t18, t22, t26, t27, t29, 
                 t31, t57, t60, t62;
  complex t35, t37, t51, t56;
  
  long t36, t47, t48;
  
  if (n2<1) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n);
  if (n1<0.0) greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_energy);
  if ((l1<0)||(l2<0))   greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_l);
  if (n2<=l2)           greens_ERROR(_str_greens_nonrel_radial_me, _str_wrong_n_or_l);

  Z  = greens_save_nuclear_charge;
  t2 = powl(2.0,l1+l2);
 
  t3 = 1.0;
  if (l1>0) {for(long s=1; s<=l1; s++){t3=t3*sqrt(2.0*s*s*n1+Z*Z);}}

  t6 = powl(Z,2+l2);
  t9 = sqrtl(2.0);
  t10 = sqrtl(n1);
  t11 = 1.0/t10;
  t13 = Pi*Z*t9*t11;
  t15 = expl(t13/2.0);
  t16 = expl(t13);
  t18 = sqrtl(t16-1.0);
  t22 = powl(n2,2+l1);
  t26 = factorial(n2+l2);
  t27 = sqrtl(t26);
  t29 = factorial(n2-l2-1);
  t31 = sqrtl(1.0/t29);
  t35 = I*t9*t10*n2+Z;
  t36 = -4-l1-l2;
  t37 = powl(t35,t36);
  t47 = l1+1;
  t48 = l2+1;
  t51 = 1.0/t35;
  t57 = factorial(2*t48-1);
  t60 = factorial(2*t47-1);
  t62 = factorial(-t36-1);

  t56 = greens_F2(-t36, (I*Z*t9+2*l1*t10+2*t10)*t11/2.0, -n2+l2+1,
                 2*t47, 2*t48, 2*I*t9*t10*n2*t51, 2*Z*t51);
      
  last_lres = Re(4.0*t2*t3*t6*t15/t18*t22*t27*t31*t37*t56/t57/t60*t62);
      
      last_corr=true;
      return(last_lres);
}
*/


