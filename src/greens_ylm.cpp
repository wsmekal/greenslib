#include "greens_ylm.hpp"

//
// greens_Ylm()  spherical harmonic
//
dcomplex greens_Ylm(int l, int m, double theta, double phi)
{
/*
    #evaluation of Ylm from Racah
    #
    # Expansion yields polynomial in sin(theta)
    # using 5.2.(17) from Varshalovich et al. (1988).
    #
*/
  dcomplex ylm;
  int L;
  double fact;

  if (l<0) greens_ERROR(_str_greens_Ylm, _str_wrong_l);
  if (l<abs(m)) greens_ERROR(_str_greens_Ylm, _str_wrong_m);
      
  if  (greens_IsEven(l-m)) {L=l; fact=1;} else {L=l-1; fact=cos(theta);};

    if (sin(theta) != 0.0){
      for(int s=abs(m); s<=L; s=s+2){
         ylm = ylm + pow(-1.0,(double)((s+m)/2)) * factorial(l+s) /    // TODO: is (s/m)/2 in integer deliberate
	      (factorial((s+m)/2)*factorial((s-m)/2)*pow(2.0,(double)s)) *
              (factorial((L+m)/2)*factorial((L-m)/2)) / 
	      (factorial((L+s)/2)*factorial((L-s)/2)) *
               pow(sin(theta),(double)(s-abs(m)));}
			 
         ylm = sqrt((l+l+1)/(4*Pi*factorial(l+m)*factorial(l-m))) * ylm * 
	 pow(sin(theta),(double)abs(m)) * fact * exp(I*m*phi);}
    else {if (m == 0) {
         ylm = sqrt((l+l+1)/(4*Pi*factorial(l+m)*factorial(l-m))) * 
	       pow(-1.0,(abs(m)+m)/2.0) * factorial(l+abs(m)) / 
	      (factorial((abs(m)+m)/2.0) * 
	       factorial((abs(m)-m)/2.0)*pow(2.0,(double)abs(m))) * fact;}
	else {ylm = 0.0;}};

  return(ylm);
}
