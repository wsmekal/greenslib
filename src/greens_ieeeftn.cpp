#include "greens_ieeeftn.hpp"

/*!
  Original source code was taken wrom freely available library
  http://www.netlib.org/

*/

/*!
  Check whether the float number is infinite
*/

int greens_isinf(float x)
{
    float_parts w;

    w.r = x;
    return (((GET_EXPONENT_SP(w.i) == EXPONENT_INFNAN_SP) &&
	     (SIGNIFICAND_SP(w.i) == 0)) ? true : false); 
}


/*!
  Check whether the double number is infinite
*/
int greens_isinf(double x)
{
    double_parts w;

    w.r = x;
    return (((GET_EXPONENT_DP(w.i[DP_HIGH]) == EXPONENT_INFNAN_DP) &&
	     (SIGNIFICAND_DP(w.i[DP_HIGH]) == 0) && (w.i[DP_LOW] == 0))
	    ? true : false);
}


/*!
  Check whether the float number is NaN (not a number)
*/

int greens_isnan(float x)
{
#if defined(__alpha)
    if (greens_isden(x))
	return (false);
    else
	return ((x != x) ? true : false);
#else
    return ((x != x) ? true : false);
#endif
}


/*!
  Check whether a double  number is NaN (not a number)
*/

int greens_isnan(double x)
{
#if defined(__alpha)
    if (greens_isden(x))
	return (false);
    else
	return ((x != x) ? true : false);
#else
    return ((x != x) ? true : false);
#endif
}


/*! return the exponent of the base in the representation of x */

short int greens_intxp(float x)
{
    float_parts w;
    register short int e;

    w.r = x;
    e = GET_EXPONENT_SP(w.i);

    if (x == 0.0)			/* handle zero specially */
	e = 0;
    else if (e == EXPONENT_DENORM_SP)	/* have denormalized number */
    {
	w.r *= BASE_TO_THE_T_SP;	/* make normal number */
	e = GET_EXPONENT_SP(w.i) - T_SP;
    }
    return (e);
}


/*! return the exponent of the base in the representation of x */

int greens_intxp(double x) 
{
    double_parts w;
    register int e;

    w.r = x;
    e = GET_EXPONENT_DP(w.i[DP_HIGH]);

    if (x == 0.0)			/* handle zero specially */
	e = 0;
    else if (e == EXPONENT_DENORM_DP)	/* have denormalized number */
    {
	w.r *= BASE_TO_THE_T_DP;	/* make normal number */
	e = GET_EXPONENT_DP(w.i[DP_HIGH]) - T_DP;
    }
    return (e);
}


/*! return (x is denormalized) */

int greens_isden(float x)
{
    return (greens_intxp(x) <= EXPONENT_DENORM_SP) ? true : false;
}


/*! return (x is denormalized) */

int greens_isden(double x)			
{
  return (greens_intxp(x) <= EXPONENT_DENORM_DP) ? true : false;
}
