#include "greens_nonrelgreenfunc.hpp"

//
// ---- nonrelativistic  radial_Greens_function ------------
//  g(E; r,r') many-body (Grant) definition atomic units
//
double greens_radial_function(double E, int l, double r1, double r2)
{
  if (E>0.0) greens_ERROR(_str_greens_radial_Green_function, _str_wrong_E);
  if (l<0) greens_ERROR(_str_greens_radial_Green_function, _str_wrong_l);
  if (r1<0.0) greens_ERROR(_str_greens_radial_Green_function, 
    _str_wrong_coords);
  if (r2<0.0) greens_ERROR(_str_greens_radial_Green_function, 
    _str_wrong_coords);

  double t1, t2, t5, t8, t11, t13, t18, t23, t29,
              rl, rs, err1, err2, right1, right2;
  long t7;
  
  if (r1>r2) {rl=r1; rs=r2; } else {rs=r1; rl=r2;}; 
  if (rs==0.0) rs=rs+greens_dexact; if (rl==0.0) rl=rl+greens_dexact;

      t1 = E+E; t2 = sqrt(-t1); t5 = l+1.0-1.0/t2*greens_save_nuclear_charge;
      t7 = l+1; t8 = pow(2.0,(double)(t7+t7)); t11 = pow(-t1,l+0.5);
      t13 = pow(r1*r2, (double)t7);  t18 = exp(-(r1+r2)*t2);
      t23 = KummerM(t5,t7+t7,(rs+rs)*t2); err1=last_derr; right1=last_corr;
      t29 = Re(KummerU(t5,t7+t7,(rl+rl)*t2)); err2=last_derr; right2=last_corr;
      
  if (err1>err2) {last_derr=err1;} else last_derr=err2; 
                                        last_corr=right1&&right2;
  last_dres=t29*t23*t18*t13*t11*t8*Re(exp(lngamma(t5)-lngamma(double(t7+t7))));
  return(last_dres);
}

//
// ---- nonrelativistic  radial_Greens_function ------------  
//  g(E; r,r')/(rr') normal (Drake) definition atomic units
//
double greens_radial_function_Drake(double E, int l, 
                                           double r1, double r2)
{
  if ((r1==0.0) || (r2==0.0)) return(0.0);
  return(greens_radial_function(E, l, r1, r2)/(r1*r2));
}

