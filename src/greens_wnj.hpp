#ifndef __GREENS_WNJ_HPP
#define __GREENS_WNJ_HPP

#include <math.h>

#include "greens_dcomplex.hpp"
#include "greens_specfunc.hpp"

/*!
  This procedure determines whether j1, j2, and j3 form an
  angular momentum coupling triangle. A boolean variable of either
  true of false is returned.
*/
bool Racah_IsTriangle(int j1, int j2, int j3);
bool Racah_IsTriangle(double j1, double j2, double j3);

/*!
   Quantum number of orbital momentum of large
*/
int greens_angular_l(int kappa);

/*!
   Quantum number of orbital momentum of large or small component
*/
int greens_angular_l(int kappa, int T);

/*!
   Quantum number of orbital momentum of +1 large or -1 small component
*/
int greens_angular_l_fortran(int kappa, int T);

/*!
   Inversion of a superscript
*/
int greens_invert_T(int T);

/*!
   Quantum number of orbital momentum of large component
*/
int greens_angular_l_large(int kappa);
/*!
   Quantum number of orbital momentum of small component
*/
int greens_angular_l_small(int kappa);

/*!
   Wigner 3j-symbol. Integer arguments
*/
double greens_w3j (int j1, int j2, int j3, 
                  int m1, int m2, int m3);

/*!
   Wigner 3j-symbol. Integer and half integer arguments
*/		  
double greens_w3j (double j1, double j2, double j3, 
                  double m1, double m2, double m3);

/*!
   Wigner 6j-symbol. Integer and half integer arguments
*/		  
double greens_w6j(double j1, double j2, double j3, 
                 double j4, double j5, double j6);

/*!
   Wigner 9j-symbol. Integer and half integer arguments
*/		  
double greens_w9j(double j1, double j2, double j3, 
                 double j4, double j5, double j6,
                 double j7, double j8, double j9);

/*!
   Clebsch-Gordan coefficient. Integer and half integer arguments
*/		  
double greens_ClebschGordan(double j1, double m1, double j2, 
                           double m2, double J, double M);

/*!
   Wigner 3j-symbol. Integer and half integer arguments
   Some checks are ommitted
*/		  
double _greens_w3j (double j1, double j2, double j3, 
                   double m1, double m2, double m3);
		   
/*!
   Wigner 6j-symbol. Integer and half integer arguments
   Some checks are ommitted
*/		  
double _greens_w6j(double j1, double j2, double j3, 
                  double l1, double l2, double l3);
		  
/*!
   Wigner 9j-symbol. Integer and half integer arguments
   Some checks are ommitted
*/		  
double _greens_w9j(double j11, double j12, double j13, 
                  double j21, double j22, double j23,
                  double j31, double j32, double j33);

/*!
   Clebsch-Gordan coefficient. Integer and half integer arguments.
   Some checks are ommitted
*/		  
double _greens_ClebschGordan(double j1, double m1, double j2, 
                            double m2, double J, double M);

#endif
