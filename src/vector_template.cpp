//
//
//
bool operator ==(VECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  bool res = true;
  
  for (size_t i = 0; i<a.dim; i++){
    res = res && a.e[i] == b.e[i];
    if (!res) break;}

  return(res);
}
//
//
//
bool operator ==(VECTOR a, REAL b)
{
  bool res = true;
  
  for (size_t i = 0; i<a.dim; i++){
    res = res && a.e[i] == b;
    if (!res) break;}

  return(res);
}

//
//
//
bool operator ==(REAL a, VECTOR b)
{
  bool res = true;
  
  for (size_t i = 0; i<b.dim; i++){
    res = res && b.e[i] == a;
    if (!res) break;}

  return(res);
}

//
//
//
VECTOR operator +(VECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  VECTOR res = a;
  for(size_t i=0; i<a.dim; i++) res.e[i] = res.e[i]+b.e[i];
  return(res);  
}

//
//
//
VECTOR operator +(VECTOR a, REAL b)
{
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]+b;
  return(res);  
}

//
//
//
VECTOR operator +(REAL a, VECTOR b)
{
  VECTOR res = VECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = b.e[i]+a;
  return(res);  
}

//
//
//
VECTOR operator -(VECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b.e[i];
  return(res);  
}

//
//
//
VECTOR operator -(VECTOR a, REAL b)
{
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]-b;
  return(res);  
}

//
//
//
VECTOR operator -(REAL a, VECTOR b)
{
  VECTOR res = VECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a-b.e[i];
  return(res);  
}

//
//
//
VECTOR operator -(VECTOR b)
{
  VECTOR res = VECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = -b.e[i];
  return(res);  
}

//
//
//
REAL operator *(VECTOR a, VECTOR b)
{
  if (b.dim!=a.dim) {
    fprintf(stderr, "wrong dimension \n"); fflush(stderr);
    if (errorbreak==Break) exit(1); return(false);} 
  REAL res = 0;
  for(size_t i=0; i<a.dim; i++) res = res+a.e[i]*b.e[i];
  return(res);  
}

//
//
//
VECTOR operator *(VECTOR a, REAL b)
{
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]*b;
  return(res);  
}

//
//
//
VECTOR operator *(REAL a, VECTOR b)
{
  VECTOR res = VECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a*b.e[i];
  return(res);  
}

//
//
//
VECTOR operator /(VECTOR a, REAL b)
{
  VECTOR res = VECTOR(a.dim);
  for(size_t i=0; i<a.dim; i++) res.e[i] = a.e[i]/b;
  return(res);  
}

//
//
//
VECTOR operator /(REAL a, VECTOR b)
{
  VECTOR res = VECTOR(b.dim);
  for(size_t i=0; i<b.dim; i++) res.e[i] = a/b.e[i];
  return(res);  
}

//
//
//
REAL dotprod(VECTOR a, VECTOR b){return(a*b);}

REAL abs(VECTOR a){return(sqrt(a*a));}
//
//
//
void print(VECTOR a)
{
  std::cout << "\n" << "[";
  for(size_t i=0; i<a.dim-1; i++) {
    std::cout << a.e[i] << ", "; }
  std::cout << a.e[a.dim-1] << "]\n";
}
//
//
//
void printgp(VECTOR a)
{  
  for(size_t i=0; i<a.dim; i++) {
    std::cout << a.e[i] << " "; }
  std::cout << "\n";
}
