#ifndef __GREENS_SPECFUNC_HPP
#define __GREENS_SPECFUNC_HPP

#include <math.h>

#include "greens_dcomplex.hpp"
#include "greens_service.hpp"
#include "greens_minmax.hpp"
#include "greens_nearest.hpp"
#include "greens_ieeeftn.hpp"


  double pow_minus(int m);
dcomplex pow_minus(double m);
dcomplex pow_minus(dcomplex m);

  double pow_minus(long m);

  double GAMMA(double);
dcomplex GAMMA(dcomplex);

dcomplex lngamma(double);
dcomplex lngamma(dcomplex);

double   factorial(int l);
double   factorial(double l);
dcomplex factorial(dcomplex l);
double  _factorial(int l);

  double Psi(double);
dcomplex Psi(dcomplex);

//
//
// -------- Laguerre Polynom ------------------------------------------------
//
double greens_L(long n, double alpha, double z);

// -------------- Confluent hypergeometric function M(a,b,z) or _1F_1(a,b,z)--------------

//
//--------- function G(a,b,z) from Landau V 3. appendix. Asymptotic expansion
//
//-------------------------------- real G ---------------------------------------
//
double greens_G(double a, double b, double z);
//
//---------- half complex  G with Re(smd) limiting using when result approximately=1-----
//
dcomplex  greens_G(dcomplex a, dcomplex b, dcomplex z);

//
//-------------------- real KummerM -------------------------------------------------
//
  double KummerM(double a, double b, double z);
//
//
//--------------- halb complex KummerM ----------------------------------------------
//
dcomplex KummerM(dcomplex a, double b, dcomplex z);
//
// ---------------- Kummer's U ------------------------------------------------------
//
dcomplex KummerU(double a, double b, double z);
//
// ------------ real WhittakerM ;----------------------------------------------------
//
double WhittakerM(double a, double b, double z);
//
// ------------ real WhittakerW -----------------------------------------------------
//
dcomplex WhittakerW(double a, double b, double z);
//
// ------------ real j(n,z) --------------------------------------------------------------
//
double greens_spherical_j(long n, double z);

//
// ------------------ Heaviside(x) -------------------------------------------------
//
double Heaviside(double x);

//
// A hypergeometric function of several variables (two variables) Gradshteyn and Ryzhik
// all variable are real
//
double greens_F2(double alpha, double beta1, double beta2, double gamma1, 
                double gamma2, double x, double y);
//
// A hypergeometric function of several variables (two variables) Gradshteyn and Ryzhik
// variable are real and complex
//
dcomplex greens_F2(double alpha, dcomplex beta1, double beta2, double gamma1, 
                  double gamma2, dcomplex x, dcomplex y);

#endif
