#ifndef __GREENS_CONVERT_HPP
#define __GREENS_CONVERT_HPP

#include "greens_const.hpp"
//
// convert TPCS from cm^4*sec^(-1) to cm^4*W^(-1) 
// :=TPCS[cm^4*sec^(-1)]/(energy of photon)[J];
// greens_convert_TPCS_to_usual(TPCS[cm^4*sec^(-1)], Ep[atomic unit])[cm^4*W^(-1)]
//
double greens_convert_TPCS_to_usual(double TPCS, double Ep);
//
// convert TPCS from cm^4*W to atomic units length^4 * time
// :=TPCS[cm^4*W]/(energy of photon)[J];
// greens_convert_tpsc_l4tm1_to_au(TPCS[cm^4*W], E_ph[atomic units])
// [length^4 *time^-1 in au (Hartree units)]
//
double greens_convert_tpsc_l4t1_to_au(double TPCS, double E_ph);
//
// convert TPCS from atomic units length^4 * time to cgs-units cm^4 sec
// greens_convert_tpsc_l4tm1_to_au(TPCS[au])
// [cm^4 sec (cgs-units)]
//
double greens_convert_tpsc_au_to_cm4sec(double TPCS);

//
// convert length of wave to frequency of wave
// frequency(energy)[atomic] := speed of light [atomic]/lambda[atomic]
// greens_convert_length_to_frequency(lambda[Angstrem])(atomic)
//
double greens_convert_length_to_frequency(double lambda);
//
// convert frequency of wave to length of wave
// greens_convert_frequency_to_length(frequency(energy)[atomic])[Angstrem]
//
double greens_convert_frequency_to_length(double Ep);


#endif
