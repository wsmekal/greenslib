#include "greens_tpcs.hpp"

const char * _str_greens_two_photon_cs = "greens_two_photon_cs";
const char * _str_wrong_framework = "wrong framework";
const char * _str_wrong_polarization = "wrong polarization";

/*!
   Procedure which decided the framework and polarization
   for two-photon ionization cross section
*/

double greens_two_photon_cs(const char* framework, const char* polarization,
                            double E_gamma, const int digits)
{

// nonrelativistic

  if (strcmp(framework, "nonrelativistic")==0){

    if (strcmp(polarization, "circular")==0) {
      return(greens_TPCSN_lw_circ(E_gamma, digits));}

    if (strcmp(polarization, "linear")==0) {
      return(greens_TPCSN_lw_lin(E_gamma, digits));}

    greens_ERROR(_str_greens_two_photon_cs, _str_wrong_polarization);
  }
  
//
// relativistic
//  
  if (strcmp(framework, "relativistic")==0){

    if (strcmp(polarization, "circular")==0) {
      return(greens_TPCSR_lw_circ(E_gamma, digits));}

    if (strcmp(polarization, "linear")==0) {
      return(greens_TPCSR_lw_lin(E_gamma, digits));}

    greens_ERROR(_str_greens_two_photon_cs, _str_wrong_polarization);
  }
  
  
    greens_ERROR(_str_greens_two_photon_cs, _str_wrong_framework);
    return(0.0);
}

