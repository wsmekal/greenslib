#ifndef __GREENS_NUMINT_HPP
#define __GREENS_NUMINT_HPP

#include "greens_service.hpp"
#include "greens_minmax.hpp"
#include "greens_mdclass.hpp"
#include "greens_ieeeftn.hpp"

//
// ---------- Gauss-Legendre Integration int(f(x), x=a..b) ---------------------
//
//
// -------------- real integrand Gauss_Legendre_Int ----------------------------
//
/*!
  GLegMax_order is number of Orders for which the knotes and weigths are 
  available.
*/
#define GLegMax_order 6

 
double greens_integral_GL_order(double (*funct)(double), 
                                      const double Xmin, const double Xmax, 
				      const int Order, const long Raum);
//
//---------------------- double  greens_Gauss_Legendre_Int -----------------
//
extern double _GLeg_Piece;

double greens_integral_GL(double (*funct)(double), 
                                const double Xmin, const double Xmax, 
				const int digits);
//
// int(funct, x=Xmin..infinity)
//

double greens_integral_GL(double (*funct)(double), const double Xmin, 
                               /* double Xmax=infinity */ const int digits);
//
// 
//
double greens_integral_GL(double (*funct)(double), const int digits);

//
// -------------- Double_Int with Gauss-Legendre method -----------------------
//
//
// -------------- real Gauss_Legendre_Double_Int ------------------------------
//:=int(int(f(x,y), x=Xmin..Xmax), y=Ymin..Ymax);
//
double greens_integral_GL(double (*funct)(double, double), 
                          const double Xmin, const double Xmax, 
                          const double Ymin, const double Ymax, 
			  const int digits);
//
//  real Polar Int
//:=int(int(funct(x,y)*r, r=Rmin..Rmax), p=Pmin..Pmax); with 
// transformation x=r*cos(p); y=r*sin(p);
//

//
//  real Polar Int
//:=int(int(funct(x,y)*r, r=Rmin..Rmax), p=Pmin..Pmax); 
// with transformation x=r*cos(p); y=r*sin(p);
//
double greens_integral_GL(double (*funct)(double, double),
                          const double Rmin, const double Rmax, 
                          const double Pmin, const double Pmax, 
		          const int digits);
//
//  real Polar Int with infinity oben limit
//:=int(int(funct(x,y)*r, r=0..infinity), p=0..Pi/4)+
//               int(int(funct(x,y)*r, r=0..infinity), p=Pi/4..Pi/2); 
// with transformation x=r*cos(p); y=r*sin(p);
//
double greens_integral_GL(double (*funct)(double, double), 
                          const int digits);

//
//  real Polar Int with infinity oben limit
//:=int(int(funct(x,y)*r, r=0..infinity), p=0..Pi/4)+
//         int(int(funct(x,y)*r, r=0..infinity), p=Pi/4..Pi/2); 
// with transformation x=r*cos(p); y=r*sin(p);
//
matrix_2x2 greens_integral_GL(matrix_2x2 (*funct)(double, double), 
                             const int digits);
   
#endif
