#ifndef __GREENS_OPCSNL_HPP
#define __GREENS_OPCSNL_HPP

#include <math.h>

#include "greens_const.hpp"
#include "greens_nonrelme.hpp"
#include "greens_nonrelwavefunc.hpp"

/*!
 One photon cross section sigma_1 from state (n,l,m)
 to the state (n_f),  by polarisation of light  lambda={-1,1}
 nonrelativistic long wave approx [sigma_1] cm^2.
*/
double greens_nonrel_OPCS(int n_f, int lambda, int n, int l, int m);

/*!
 One photon cross section sigma_1 from state (n,l,m)
 to the state (E_f),  by polarisation of light  lambda={-1,1}
 nonrelativistic long wave approx [sigma_1] cm^2.
*/
double greens_nonrel_OPCS(double E_f, int lambda, int n, int l, int m, int digits);

#endif
