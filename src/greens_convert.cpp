#include "greens_convert.hpp"
//
// convert TPCS from cm^4*sec^(-1) to cm^4*W^(-1) 
// :=TPCS[cm^4*sec^(-1)]/(energy of photon)[J];
// greens_convert_TPCS_to_usual(TPCS[cm^4*sec^(-1)], E_ph[atomic unit])
// [cm^4*W^(-1)]
//
double greens_convert_TPCS_to_usual(double TPCS, double Ep)
{
   return(TPCS/(greens_Constant_SI_me*greens_Constant_SI_c*
          greens_Constant_SI_c*greens_constant_powalpha2*Ep));
}

//
// convert TPCS from cm^4*W to atomic units length^4 / time
// :=TPCS[cm^4*W]/(energy of photon)[J];
// greens_convert_TPCS_to_usual(TPCS[cm^4*W], E_ph[atomic units])
// [length^4 *time^-1 in au (Hartree units)]
//
double greens_convert_tpsc_l4t1_to_au(double TPCS, double E_ph)
{
   return(TPCS*(greens_Constant_SI_me*greens_Constant_SI_c*
          greens_Constant_SI_c*greens_constant_powalpha2*E_ph) / 1.896792e-50);
}

//
// convert TPCS from atomic units length^4 * time to cgs-units cm^4 sec
// greens_convert_tpsc_l4tm1_to_au(TPCS[au])
// [cm^4 sec (cgs-units)]
//
double greens_convert_tpsc_au_to_cm4sec(double TPCS)
{return(TPCS / 1.896792e-50);}

//
// convert length of wave to frequency of wave
// frequency(energy)[atomic] := speed of light [atomic]/lambda[atomic]
// greens_convert_length_to_frequency(lambda[Angstrem])(atomic)
//
double greens_convert_length_to_frequency(double lambda)
{
   return(2*Pi*greens_constant_au_c/
   (1e-10/greens_Constant_SI_hbar*greens_Constant_SI_c*
    greens_Constant_SI_me*greens_Constant_alpha*lambda));

}

//
// convert frequency of wave to length of wave
// greens_convert_frequency_to_length(frequency(energy)[atomic])[Angstrem]
//
double greens_convert_frequency_to_length(double Ep)
{
   return(2*Pi*greens_constant_au_c/
   (1.0e-10/greens_Constant_SI_hbar*greens_Constant_SI_c*
    greens_Constant_SI_me*greens_Constant_alpha * Ep));
}

//
// 
//
//
double greens_convert(int quantity, int from, int to, double value)
{
  
  return(1.0);
}
