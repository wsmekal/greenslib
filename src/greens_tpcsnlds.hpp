
#ifndef __GREENS_TPCSNLDS_HPP
#define __GREENS_TPCSNLDS_HPP

#include <math.h>

#include "greens_const.hpp"
#include "greens_service.hpp"
#include "greens_linewidthn.hpp"
#include "greens_opcsnl.hpp"

//
// two photon cross section long wave direct summation circular polarized light
//
// [E_p]                   = aeu
// [greens_TPCSN_lwds_circ] = cm^4 sec

double greens_TPCSN_lwds_circ(double E_p, int digits);

//
// two photon cross section long wave direct summation circilar polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_p]                   = aeu
// [F]                     = cm^{-2}sec^{-1}
// [greens_TPCSN_lwds_circ] = cm^4 sec
//
double greens_TPCSN_lwds_circ(double E_p, double F, int digits);

//
// two photon cross section long wave direct summation circilar polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_photon]                 = aeu
// [Intensity]                = cm^{-2} W
// [greens_TPCSN_2I_lwds_circ] = cm^4 W^{-1}
//

double greens_TPCSN_2I_lwds_circ(double E_photon, double Intensity, int digits);

//
// two photon cross section long wave direct summation circilar polarized light
// with energy levels Gamma spontan and Gamma induziert
// because of this cross section depend to the intensity of light
//
// [E_photon]                 = aeu
// [Intensity]                = cm^{-2} W
// [greens_TPCSN_2I_lwds_lin] = cm^4 W^{-1}
//

double greens_TPCSN_2I_lwds_lin(double E_photon, double Intensity, int digits);

#endif
