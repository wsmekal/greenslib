#ifndef __GREENS_RELWAVEFUNC_HPP
#define __GREENS_RELWAVEFUNC_HPP

#include "greens_mdclass.hpp"
#include "greens_const.hpp"
#include "greens_specfunc.hpp"

#include <stdlib.h>

/*!
  The energy of a bound state of hydrogen atom, relativistic case.
  The function returns energy without rest energy mc^2
  
  Nuclear charge can be defined by using greens_set_nuclear_charge()
  function  
*/
double greens_energy(int n, int kappa);

/*!
  The function returns a bound radial orbital.
  (P_{n kappa}(r))
  (Q_{n kappa}(r))  
  There is notation usual for multielectron states.
  
  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function  
*/
spinor2_col greens_radial_spinor(int n, int kappa, double r);

/*!
  The function returns a free radial orbital.
  (P_{E kappa}(r))
  (Q_{E kappa}(r))  
  There is notation usual for multielectron states.
  Energy must be given without rest energy (like in nonrelativistic case)

  Nuclear charge can be defined by using 
  greens_set_nuclear_charge() function  
*/
spinor2_col greens_radial_spinor(double E, int kappa, double r);

#endif
