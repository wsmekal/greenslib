#ifndef __GREENS_NEAREST_HPP
#define __GREENS_NEAREST_HPP

#include "greens_service.hpp"
#include "greens_dcomplex.hpp"

#include <math.h>

double nearest(     double num, float dir);
double deviation(     double num);

dcomplex deviation(dcomplex num);

#endif
 
