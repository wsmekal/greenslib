//
// Vector with REAL components
//
class VECTOR 
{
  public : REAL * e;
  public : size_t dim; 
  VECTOR :: VECTOR(size_t d){ 
  e = new REAL[d];
  dim = d;}

  VECTOR operator = (VECTOR a) {
    if (this->dim!=a.dim) {
      fprintf(stderr, "wrong dimension \n"); fflush(stderr);
      if (errorbreak==Break) exit(1); return(*this);} 
    for(size_t i=0; i<this->dim; i++) this->e[i] = a.e[i];
    return(*this);}

  VECTOR operator = (REAL a) {
    for(size_t i=0; i<this->dim; i++) this->e[i] = a;
    return(*this);}
};
//
// ----------------------- Operators -------------------------------------
//
bool operator ==(VECTOR, VECTOR);
bool operator ==(VECTOR, REAL);
bool operator ==(REAL , VECTOR);
 
VECTOR operator +(VECTOR, VECTOR);
VECTOR operator +(REAL,  VECTOR);
VECTOR operator +(VECTOR, REAL);

VECTOR operator -(VECTOR, VECTOR);
VECTOR operator -(REAL,  VECTOR);
VECTOR operator -(VECTOR, REAL);
VECTOR operator -(VECTOR a);

REAL   operator *(VECTOR a, VECTOR b); // dotprod(a,b)
VECTOR operator *(REAL a,  VECTOR b);
VECTOR operator *(VECTOR b, REAL a);

VECTOR operator /(REAL a,  VECTOR b);
VECTOR operator /(VECTOR a, REAL b);

REAL  dotprod (VECTOR a, VECTOR b); // dotprod(a,b)
REAL  abs(VECTOR a);
void    print(VECTOR a);
void    printgp(VECTOR a);
