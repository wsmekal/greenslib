
#ifndef __GREENS_TPCSRM_HPP
#define __GREENS_TPCSRM_HPP

#include <stdlib.h>

#include "greens_const.hpp"
#include "greens_mdclass.hpp"
#include "greens_numint.hpp"
#include "greens_relwavefunc.hpp"
#include "greens_tpcsrl.hpp"


extern int    _mU_n2, _mU_kappa2, _mU_kappa, _mU_kappa1, _mU_Lambda1, _mU_Lambda2;
extern double _mU_E1, _mU_E, _mU_k; 

//
// Integrand 1 for Two Photon Cross Section relativistic case 
//
//
matrix_2x2 greens_mU(int Lambda1, int Lambda2, double Ep, int kappa1, int kappa, 
                   int kappa2, int digits);

matrix_2x2 greens_mU_lw(double Ep, int kappa1, int kappa,
                   int kappa2, int digits);
//
//
//
// Two Photon Cross Section Relativistic case circular polarized light
//
double greens_TPCSR_e1_circ(double Ep, int digits);

//
// Two Photon Cross Section Relativistic case linear polarized light
//
double greens_TPCSR_e1_lin(double Ep, int digits);

//
//  Two Photon Cross Section Relativistic case circular polarized light
//  (e1+e2)  
//
double greens_TPCSR_e1e2_circ(double Ep, int digits);
//
//  Two Photon Cross Section Relativistic case circular polarized light
//  (e1+m1)  
//
double greens_TPCSR_e1m1_circ(double Ep, int digits);

#endif
