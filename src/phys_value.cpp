#include "phys_value.hpp"


/*!
   The  operators from class phys_dim
*/

bool operator ==(phys_dim a, phys_dim b){
  return((a.L==b.L) && (a.M==b.M) && (a.T==b.T) && (a.C==b.C));}

bool operator ==(double a, phys_dim b){
  return((b.L==0.0) && (b.M==0.0) && (b.T==0.0) && (b.C==0.0));}

bool operator ==(phys_dim a, double b){
  return((a.L==0.0) && (a.M==0.0) && (a.T==0.0) && (a.C==0.0));}

bool operator !=(phys_dim a, phys_dim b){return(!(a==b));}

bool operator !=(double a, phys_dim b){return(!(a==b));}

bool operator !=(phys_dim a, double b){return(!(a==b));}

/*!

*/
phys_dim operator +(phys_dim a, phys_dim b) { 
  if (a==b) return(a);
  fprintf(stderr, "error: +: dimensions are different\n"); exit(1); 
  return(a);};

/*!

*/
phys_dim operator -(phys_dim a, phys_dim b) { 
  if (a==b) return(a);
  fprintf(stderr, "error: -: dimensions are different\n"); exit(1); 
  return(a);};

/*!

*/
phys_dim operator *(phys_dim a, phys_dim b) { 
  phys_dim res; res.L=a.L+b.L; res.M=a.M+b.M; res.T=a.T+b.T; res.C=a.C+b.C;
  return(res);};

/*!

*/
phys_dim operator /(phys_dim a, phys_dim b) { 
  phys_dim res; res.L=a.L-b.L; res.M=a.M-b.M; res.T=a.T-b.T; res.C=a.C-b.C;
  return(res);};

/*!

*/
phys_dim operator /(double a, phys_dim b) { 
  phys_dim res; res.L=-b.L; res.M=-b.M; res.T=-b.T; res.C=-b.C;
  return(res);};

/*!

*/
phys_dim operator /(phys_dim a, double b) { return(a);};

/*!

*/
phys_dim operator ^(phys_dim a, double b) { 
  phys_dim res; res.L=a.L*b; res.M=a.M*b; res.T=a.T*b; res.C=a.C*b;
  return(res);};

/*!

*/
void print(phys_dim a){
 if (a.L!=0.0) {if (a.L==1.0) {std::cout << " L";} else {std::cout <<" L^"<< a.L;}} 
 if (a.M!=0.0) {if (a.M==1.0) {std::cout << " M";} else {std::cout <<" M^"<< a.M;}} 
 if (a.T!=0.0) {if (a.T==1.0) {std::cout << " T";} else {std::cout <<" T^"<< a.T;}} 
 if (a.C!=0.0) {if (a.C==1.0) {std::cout << " C";} else {std::cout <<" C^"<< a.C;}} 
 if (a == 1.0) std::cout <<" 1"; 
 std::cout <<"\n";
}

/*!

*/
void write(phys_dim a){
 if (a.L!=0.0) {if (a.L==1.0) {std::cout << " L";} else {std::cout <<" L^"<< a.L;}} 
 if (a.M!=0.0) {if (a.M==1.0) {std::cout << " M";} else {std::cout <<" M^"<< a.M;}} 
 if (a.T!=0.0) {if (a.T==1.0) {std::cout << " T";} else {std::cout <<" T^"<< a.T;}} 
 if (a.C!=0.0) {if (a.C==1.0) {std::cout << " C";} else {std::cout <<" C^"<< a.C;}} 
 if (a == 1.0) std::cout <<" 1";
}
//
// Class physical units
//
bool operator ==(phys_units a, phys_units b){
  return((a.l==b.l) && (a.m==b.m) && (a.t==b.t) && (a.c==b.c));}

bool operator !=(phys_units a, phys_units b){return(!(a==b));}

//
//
//
void print(phys_units u){
  if (u==pu_SI) {printf("SI\n"); return;}
  if (u==pu_cgs) {printf("cgs\n"); return;}
  if (u==pu_natural) {printf("natural\n"); return;}
  if (u==pu_cgse) {printf("cgse\n"); return;}
  if (u==pu_cgsm) {printf("cgsm\n"); return;}
  if (u==pu_atomic) {printf("atomic\n"); return;}
  printf("unknown\n");
}

//
//
//
void write(phys_units u){
  if (u==pu_SI) {printf("SI"); return;}
  if (u==pu_cgs) {printf("cgs"); return;}
  if (u==pu_natural) {printf("natural"); return;}
  if (u==pu_cgse) {printf("cgse"); return;}
  if (u==pu_cgsm) {printf("cgsm"); return;}
  if (u==pu_atomic) {printf("atomic"); return;}
  printf("unknown");
}

//
// Class physical value
//
phys_value operator + (phys_value a, phys_value b){
  if (a.dim!=b.dim) {
    fprintf(stdout, "error: +: dimensions are different\n"); exit(1);}
  phys_value res = a;
  
  res.v = a.v + b.v * pow(b.units.l/a.units.l, b.dim.L)*
                      pow(b.units.m/a.units.m, b.dim.M)*
		      pow(b.units.t/a.units.t, b.dim.T)*
		      pow(b.units.c/a.units.c, b.dim.C);
  return(res);  
}

//
//
//
phys_value operator - (phys_value a, phys_value b){
  if (a.dim!=b.dim) {
    fprintf(stdout, "error: +: dimensions are different\n"); exit(1);}
  phys_value res = a;
  
  res.v = a.v - b.v * pow(b.units.l/a.units.l, b.dim.L)*
                      pow(b.units.m/a.units.m, b.dim.M)*
		      pow(b.units.t/a.units.t, b.dim.T)*
		      pow(b.units.c/a.units.c, b.dim.C);
  return(res);  
}

//
//
//
phys_value operator * (phys_value a, phys_value b){
  phys_value res; 
  res.units = a.units; res.dim = a.dim*b.dim;
  
  res.v = a.v * b.v * pow(b.units.l/a.units.l, b.dim.L)*
                      pow(b.units.m/a.units.m, b.dim.M)*
		      pow(b.units.t/a.units.t, b.dim.T)*
		      pow(b.units.c/a.units.c, b.dim.C);
  return(res);  
}

//
//
//
phys_value operator * (phys_value a, double b){
  phys_value res; res.units = a.units; res.dim = a.dim; res.v = a.v * b;
  return(res);  
}

//
//
//
phys_value operator * (double a, phys_value b){
  phys_value res; res.units = b.units; res.dim = b.dim; res.v = a * b.v;
  return(res);  
}

//
//
//
phys_value operator / (phys_value a, phys_value b){
  phys_value res; 
  res.units = a.units; res.dim = a.dim/b.dim;
  res.v = a.v / (b.v * 
                 pow(b.units.l/a.units.l, b.dim.L)*
                 pow(b.units.m/a.units.m, b.dim.M)*
	  	 pow(b.units.t/a.units.t, b.dim.T)*
		 pow(b.units.c/a.units.c, b.dim.C));
  return(res);  
}
//
//
//
phys_value operator / (double a, phys_value b){
  phys_value res; res.units = b.units; res.dim = a/b.dim; res.v = a / b.v;
  return(res);  
}

//
//
//
phys_value operator / (phys_value a, double b){
  phys_value res; res.units = a.units; res.dim = a.dim/b; res.v = a.v / b;
  return(res);  
}

//
//
//
phys_value operator ^ (phys_value a, double b){
  phys_value res; res.units = a.units; res.dim = a.dim^b;
  res.v = pow(a.v, b);
  return(res);  
}

//
// print class  phys_value
//
void print(phys_value a){
  std::cout << a.v; write(a.dim); std::cout << " "; print(a.units);
}

//
// print class  phys_value  a  in a defined unit system  u
//
void print(phys_value a, phys_units u){
  std::cout << (a.v * pow(a.units.l/u.l, a.dim.L)*
                 pow(a.units.m/u.m, a.dim.M)*
	  	 pow(a.units.t/u.t, a.dim.T)*
		 pow(a.units.c/u.c, a.dim.C)); 
  write(a.dim); std::cout << " "; print(u);
}

//
// print class  phys_value  a  in units of a defined value b
//
void print(phys_value a, phys_value b){
  if (a.dim!=b.dim) {
    fprintf(stdout, "error: print: dimensions are different\n"); exit(1);}
  
  phys_value res;
  
  res.units.l=b.units.l*b.v;  res.units.m=b.units.m*b.v; 
  res.units.t=b.units.t*b.v;  res.units.c=b.units.c*b.v; 
  
  std::cout << (a.v/ (b.v * pow(b.units.l/a.units.l, b.dim.L)*
                       pow(b.units.m/a.units.m, b.dim.M)*
		       pow(b.units.t/a.units.t, b.dim.T)*
		       pow(b.units.c/a.units.c, b.dim.C)));
  write(b.dim); std::cout << " "; print(res.units); 
}

/*!
 convert a physical value  a  to a defined unit system  u
*/
phys_value convert(phys_value a, phys_units u){
  phys_value res;
  res = (a.v * pow(a.units.l/u.l, a.dim.L)*
               pow(a.units.m/u.m, a.dim.M)*
	       pow(a.units.t/u.t, a.dim.T)*
	       pow(a.units.c/u.c, a.dim.C));
  res.dim=a.dim; res.units=u; return(res);
}

/*!
 Converts a physical value  a  to the units of a defined value b.
*/
phys_value convert(phys_value a, phys_value b){
  phys_value res;
  if (a.dim!=b.dim) {
    fprintf(stdout, "error: convert: dimensions are different\n"); exit(1);}
  res.units.l=b.units.l/b.v; 
  res.units.m=b.units.m/b.v; 
  res.units.t=b.units.t/b.v; 
  res.units.c=b.units.c/b.v; 
    
  res.v   = (a.v/ (b.v * pow(b.units.l/a.units.l, b.dim.L)*
                         pow(b.units.m/a.units.m, b.dim.M)*
		         pow(b.units.t/a.units.t, b.dim.T)*
		         pow(b.units.c/a.units.c, b.dim.C)));
  res.dim = b.dim; return(res);
}

/*!
  Converts the photon energy  E_p to the units of a defined value b
  $E_p = h nu = h c / lambda$ used as conversion law
*/
phys_value convert_photon_energy(phys_value E_p, phys_value b){

  phys_value E_SI, res;

  if (E_p.dim == pd_length)   {E_SI = pv_SI_h*pv_SI_c/E_p;} else {
  if (E_p.dim == 1/pd_length) {E_SI = pv_SI_h*pv_SI_c*E_p;} else {
  if (E_p.dim == pd_time)     {E_SI = pv_SI_h/E_p;}         else {
  if (E_p.dim == 1/pd_time)   {E_SI = pv_SI_h*E_p;}         else {
  if (E_p.dim == pd_energy)   {E_SI = convert(E_p, pu_SI);} else {
    fprintf(stdout, 
    "error: convert_photon_energy: unknown conversion law\n"); exit(1);}}}}}
 
  if (b.dim == pd_length)   {res = pv_SI_h*pv_SI_c/E_SI;} else {
  if (b.dim == 1/pd_length) {res = E_SI/pv_SI_h/pv_SI_c;} else {
  if (b.dim == pd_time)     {res = pv_SI_h/E_SI;}         else {
  if (b.dim == 1/pd_time)   {res = E_SI/pv_SI_h;}         else {
  if (b.dim == pd_energy)   {res = E_SI;}                 else {
    fprintf(stdout, 
    "error: convert_photon_energy: unknown conversion law\n"); exit(1);}}}}}

  return(convert(res, b)); 
}

/*!
 converts the photon intensity  I_i to the units of a defined 
 physical value I_o. The photon energy E_p have to be provided, 
 but sometimes did't used.
 $ I(J/(m^2)/s) = F(1/(m^2)/s) * E_p(J) $ is used as conversion law 
*/
phys_value convert_photon_intensity(phys_value I_i, phys_value E_p,
                                    phys_value I_o){

  phys_value I_SI, res;
  
  if (I_i.dim == (pd_energy/pd_time/pd_square)) 
     {I_SI = convert(I_i, pu_SI);} else {
  if (I_i.dim == (1/pd_time/pd_square)) {
      I_SI = convert(I_i, pu_SI)*convert_photon_energy(E_p, pv_J);} else {
    fprintf(stdout, 
    "error: convert_photon_intensity: unknown conversion law\n"); 
    exit(1);}}

  if (I_o.dim == (pd_energy/pd_time/pd_square)) 
     {res = convert(I_SI, I_o);} else {
  if (I_o.dim == (1/pd_time/pd_square)) {
      res = convert(I_SI/convert_photon_energy(E_p, pv_J), I_o);} else {
    fprintf(stdout, 
    "error: convert_photon_intensity: unknown conversion law\n"); 
    exit(1);}}

  return(res); 
}
