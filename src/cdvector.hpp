#ifndef __CDVECTOR
#define __CDVECTOR

#include <iostream>
#include <stdlib.h>

#include "greens_dcomplex.hpp"
#include "greens_service.hpp"
#include "dvector.hpp"

#undef CVECTOR
#undef VECTOR
#undef REAL
#undef COMPLEX

#define CVECTOR cdvector
#define VECTOR  dvector
#define REAL    double
#define COMPLEX dcomplex

#include "cvector_template.hpp"

#endif 
