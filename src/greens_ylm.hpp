#ifndef __GREENS_YLM_HPP
#define __GREENS_YLM_HPP

#include <math.h>
#include <stdlib.h>

#include "greens_dcomplex.hpp"
#include "greens_specfunc.hpp"

/*!
   The runction computes a spherical harmonic

   Expansion yields polynomial in sin(theta)
   using 5.2.(17) from Varshalovich et al. (1988). 
*/
dcomplex greens_Ylm(int l, int m, double theta, double phi);

#endif
