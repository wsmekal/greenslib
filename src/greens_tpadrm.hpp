#ifndef GREENS_TPADRM_HPP
#define GREENS_TPADRM_HPP

#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "greens_dcomplex.hpp"
#include "greens_specfunc.hpp"
#include "greens_relwavefunc.hpp"
#include "greens_ylm.hpp"
#include "greens_wnj.hpp"
#include "greens_tpcsrm.hpp"



/*!
  The reduced matrix element in form
  <(-1)^i kappa_a | sigma T_{L Lambda}| (-1)^(i+1) kappa_b>
*/
double greens_RME_T(int kappa_a, int kappa_b, int Lambda, int L, int i);

/*!
  The reduced matrix element in form
  <(-1)^i kappa_a | sigma T_{L Lambda}| (-1)^(i+1) kappa_b>
  but with less checking of arguments.
*/
double _greens_RME_T(int kappa_a, int kappa_b, int Lambda, int L, int i);

dcomplex greens_xi(int Lambda, int L, int lambda, int electric, int magnetic);

/*!
  The function \xi_{L\Lambda}^{\lambda} is useful for multipole expansion
  of a plane wave into electric and magnetic parts
*/
dcomplex greens_xi(int L, int Lambda, int lambda, int electric, int magnetic);

/*! 
    General reduced matrix element  
    <kappa_b l_b | sigma T_{L Lambda}| kappa_a l_a>
    of the tensor $T_{L\Lambda}$
*/
double greens_RME_T(int kappa_b, int l_b, int L, int Lambda, int kappa_a, int l_a);


/*!
 The function evaluates a number of relativistic radial integrals and
 save/append it to a text file
*/
int greens_TPADRM_save_TP_integrals(double E_p, int LMAX, int electric, int magnetic, 
                                   int lambda1, int lambda2, 
                                   int digits, char * _str_mU_file);
/*!
 The function loads the radial integrals and all the parameters from  a text file
*/
int greens_TPADRM_load_TP_integrals(char * _str_mU_file);

//
// function the integrals  from arrays
//
matrix_2x2 greens_mU(int Lambda_1, int Lambda_2, int kappa_1, int kappa, int kappa_0);
//
// useful definitions for control for multipol expansion
// and polarisation of light
//
#define DIPOL 1
#define DIPOLPLUSQUADRUPOL 2
#define RIGHT 1
#define LEFT -1

/*
 The function calculates the angular distribution of emitted by two-photon ionisation electrons.
 Polarisation of light -- may be defined for each transition.
 Actually it should be the same.
*/
double greens_TPADRM_dsigma_dOmega(double theta, double phi, int lmax,
                                  int lambda_1, int lambda_2, int electr, int magn);

/*
 The function calculates the angular distribution of emitted by two-photon ionisation electrons 
 Polarisation of light -- nonpolarized.  
*/
double greens_TPADRM_dsigma_dOmega_nonpolarized(double theta, double phi, 
                                               int lmax, int electr, int magn);

#endif
