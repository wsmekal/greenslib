#include "greens_tpcsrl.hpp"
//
// Integrand 1 for Two Photon Cross Section relativistic case 
//
int        _TPCSR_n2, _TPCSR_kappa2, _TPCSR_kappa_nu, _TPCSR_kappa1;
double _TPCSR_E_nu, _TPCSR_E1;
//
matrix_2x2 mIntegrand1(double r1, double r2)
{
  matrix_2x2 g, res; spinor2_col f2, f1;

  f1=greens_radial_spinor(_TPCSR_E1, _TPCSR_kappa1, r1);

  /*
  print(_TPCSR_E1);
  print(_TPCSR_kappa1);
  print(r1);
  print(f1); print();
  greens_WARNING(_str_greens_TPCSR, _str_Debug);
  */

  f2 = greens_radial_spinor(_TPCSR_n2, _TPCSR_kappa2, r2);
  g  = greens_radial_matrix(_TPCSR_E_nu, _TPCSR_kappa_nu, r1, r2);
  
  res.e[0][0]=f1.S*g.e[0][0]*f2.S; res.e[0][1]=f1.S*g.e[0][1]*f2.L;
  res.e[1][0]=f1.L*g.e[1][0]*f2.S; res.e[1][1]=f1.L*g.e[1][1]*f2.L;

  return(res);
}

//
//
// Two Photon Cross Section Relativistic case circular polarization length 
// wave approximation
//
double greens_TPCSR_lw_circ(double E_p, int digits)
{
  double coeff; matrix_2x2 U321, U221, U211;
  double err[3];

  _GLeg_Piece=5.0/greens_save_nuclear_charge;

  _TPCSR_n2   = 1; _TPCSR_kappa2 = -1;
  _TPCSR_E_nu = greens_energy(_TPCSR_n2, _TPCSR_kappa2) + E_p;
  _TPCSR_E1   = _TPCSR_E_nu+E_p;
  
  _TPCSR_kappa1 = 2; _TPCSR_kappa_nu =  1;
  U211=greens_integral_GL(mIntegrand1, digits); err[2]=last_derr;

  _TPCSR_kappa1 = 2; _TPCSR_kappa_nu = -2;
  U221=greens_integral_GL(mIntegrand1, digits); err[1]=last_derr;

  _TPCSR_kappa1 =-3; _TPCSR_kappa_nu = -2;
  U321=greens_integral_GL(mIntegrand1, digits); err[0]=last_derr;

  last_dres=32.0/25.0*pow(U321.e[1][0], 2.0)+
           12.0*pow( U221.e[0][0]/9.0+U221.e[1][0]/15.0
                    -U211.e[0][0]/9.0-U211.e[0][1]/3.0, 2.0);

  coeff = TwoPi*greens_constant_cgs_c/pow(greens_constant_alpha, 3.0)/
          (greens_constant_cgs_a0* pow(greens_constant_cgs_F0, 2.0)*E_p*E_p);
// cgs coeff

  coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2/(E_p*E_p) /
          pow(greens_constant_alpha, 4.0);

  last_derr=max(err[0], max(err[1], err[2]));
  last_dres=last_dres * coeff;
  return(last_dres);
}
//
//
// Two Photon Cross Section Relativistic case linear polarization 
// length wave approximation
//
double greens_TPCSR_lw_lin(double E_p, int digits)
{
  double coeff; matrix_2x2 U321, U221, U211, U121, U111;
  double err[5];

  _GLeg_Piece=5.0/greens_save_nuclear_charge;

  _TPCSR_n2   = 1; _TPCSR_kappa2 = -1;
  _TPCSR_E_nu = greens_energy(_TPCSR_n2, _TPCSR_kappa2) + E_p;
  _TPCSR_E1   = _TPCSR_E_nu+E_p;

  _TPCSR_kappa1 = 2; _TPCSR_kappa_nu =  1;
  U211=greens_integral_GL(mIntegrand1, digits); err[0]=last_derr;

  _TPCSR_kappa1 = 2; _TPCSR_kappa_nu = -2;
  U221=greens_integral_GL(mIntegrand1, digits); err[1]=last_derr;

  _TPCSR_kappa1 =-3; _TPCSR_kappa_nu = -2;
  U321=greens_integral_GL(mIntegrand1, digits); err[2]=last_derr;

  _TPCSR_kappa1 =-1; _TPCSR_kappa_nu = -2;
  U121=greens_integral_GL(mIntegrand1, digits); err[3]=last_derr;

  _TPCSR_kappa1 =-1; _TPCSR_kappa_nu = +1;
  U111=greens_integral_GL(mIntegrand1, digits); err[4]=last_derr;

  coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2/(E_p*E_p);

//  coeff = TwoPi*greens_constant_cgs_c*greens_constant_alpha/
//(greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0*Ep*Ep);
// cgs coeff
  
  last_dres=64.0/75.0*
  pow(U321.e[1][0], 2.0)+pow( U121.e[0][0]*8.0/9.0+U111.e[0][0]/9.0
               +U111.e[0][1]/3.0+U111.e[1][0]/3.0+U111.e[1][1], 2.0)+
  8.0*pow(U221.e[0][0]/9.0+U221.e[1][0]/15.0
         -U211.e[0][0]/9.0-U211.e[0][1]/3.0, 2.0);
  last_dres=last_dres/greens_constant_powalpha4;
  
  last_dres=last_dres*coeff;
  last_derr=err[0]; 
  for(int i=1; i<5; i++){if (last_derr<err[i]) last_derr=err[i];};

  return(last_dres);
}
