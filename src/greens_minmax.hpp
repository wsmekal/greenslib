#ifndef __GREENS_MINMAX_HPP
#define __GREENS_MINMAX_HPP


double min(double x, double y);
double max(double x, double y);
double min(double a, double b, double c);
double max(double a, double b, double c);
double min(double a, double b, double c, double d);
double max(double a, double b, double c, double d);


int min(int x, int y);
int max(int x, int y);
int min(int x, int y, int z);
int max(int x, int y, int z);
int min(int a, int b, int c, int d);
int max(int a, int b, int c, int d);

#endif 
