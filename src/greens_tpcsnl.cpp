#include "greens_tpcsnl.hpp"

//
// Integrand 1 for Two Photon Cross Section Nonrelativistic case 
//
int        _TPCSN_n2, _TPCSN_l2, _TPCSN_l_nu, _TPCSN_l1; 
double _TPCSN_E_nu, _TPCSN_E1;
//
double Integrand1_TPCSN(double r1, double r2)
{ 
  double res1, res2, res3;  
  res1=greens_radial_orbital(_TPCSN_E1, _TPCSN_l1, r1);
  res2=greens_radial_function(_TPCSN_E_nu, _TPCSN_l_nu, r1, r2); 
  res3=greens_radial_orbital(_TPCSN_n2, _TPCSN_l2, r2); 

  return(res1 *r1* res2 *r2* res3);
}

//
// Two Photon Cross Section Nonrelativistic case 
//
// photon energy [E_p] = au
// [greens_TPCSN_lw_circ] = au  length^4 time
// to cgs 1.896792e-50
//

double greens_TPCSN_lw_circ(double E_p, int digits)
{
  double coeff, ME1;
  
  _TPCSN_n2   = 1; _TPCSN_l2 = 0;              _TPCSN_E_nu = greens_energy(_TPCSN_n2)+E_p;
  _TPCSN_l_nu = 1; _TPCSN_E1 = _TPCSN_E_nu+E_p; _TPCSN_l1   = 2;
  
  coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2*E_p*E_p;
    
  _GLeg_Piece=5.0/greens_save_nuclear_charge;
  
  ME1=greens_integral_GL(Integrand1_TPCSN, digits);
  
  last_dres=2.0*ME1*ME1/15.0*coeff;
  return(last_dres);
}

//
// Two Photon Cross Section Nonrelativistic case 2s initial state
//
// photon energy [E_p] = au
// [greens_TPCSN_lw_circ] = au  length^4 time
// to cgs 1.896792e-50
//

double greens_TPCSN_2s_lw_circ(double E_p, int digits)
{
  double coeff, ME1;
  
  _TPCSN_n2   = 2; _TPCSN_l2 = 0; _TPCSN_E_nu = greens_energy(_TPCSN_n2)+E_p;
  _TPCSN_l_nu = 1; _TPCSN_E1 = _TPCSN_E_nu+E_p; _TPCSN_l1   = 2;
  
  coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2*E_p*E_p;
    
  _GLeg_Piece=5.0/greens_save_nuclear_charge;
  
  ME1=greens_integral_GL(Integrand1_TPCSN, digits);
  
  last_dres=2.0*ME1*ME1/15.0*coeff;
  return(last_dres);
}

//
// two photon cross section long wave direct circular polarized light
//
// photon energy [E_p] = au
// [greens_TPCSN_2I_lw_circ] = au
// 
//
double greens_TPCSN_2I_lw_circ(double E_p, int digits)
{
//  double E_p_SI = E_p * 0.436009164840371e-17;
//  last_dres = greens_TPCSN_lw_circ(E_p, digits)/E_p_SI;
  last_dres = greens_TPCSN_lw_circ(E_p, digits)/E_p;
  return(last_dres);
}


//
// Two Photon Cross Section Nonrelativistic case  linear polarized light
//
// photon energy [E_p] = au
// [greens_TPCSN_lw_lin] = au
//

double greens_TPCSN_lw_lin(double E_p, int digits)
{
  double coeff, ME012, ME010, err[2], corr[2];

//  coeff = TwoPi*greens_constant_cgs_c*greens_constant_alpha*E_p*E_p/
//   (greens_constant_cgs_a0*greens_constant_cgs_F0*greens_constant_cgs_F0);

  coeff = 8.0*pow(Pi,3.0)*greens_constant_powalpha2*E_p*E_p;
  _GLeg_Piece=5.0/greens_save_nuclear_charge;
  
  _TPCSN_n2   = 1; _TPCSN_l2 = 0; 
  _TPCSN_E_nu = greens_energy(_TPCSN_n2)+E_p;
  _TPCSN_l_nu = 1; _TPCSN_E1 = _TPCSN_E_nu+E_p; 

  _TPCSN_l1 = 2;
  ME012=greens_integral_GL(Integrand1_TPCSN, digits);
  err[0]=last_derr; corr[0]=last_corr;
  
  _TPCSN_l1 = 0;  
  ME010=greens_integral_GL(Integrand1_TPCSN, digits);
  err[1]=last_derr; corr[1]=last_corr;
  
  last_dres=(ME010*ME010/9.0 + ME012*ME012*4.0/45.0)*coeff;
  last_derr=2.0*(err[0]/9.0 + err[1]*4.0/45.0);
  return(last_dres);
}

//
// two photon cross section long wave linear polarized light
//
// photon energy [E_p] = au
// [greens_TPCSN_2I_lw_lin] = au
//
double greens_TPCSN_2I_lw_lin(double E_p, int digits)
{
  last_dres = greens_TPCSN_lw_lin(E_p, digits)/E_p;
  return(last_dres);
}

