#ifndef __PHYS_VALUE_HPP
#define __PHYS_VALUE_HPP

#include <stdio.h>
#include <math.h>
#include <iostream>
#include "greens_const.hpp"
#include "greens_service.hpp"

/*! 
    The class phys_dim represents a physical dimension. 
    It contains the powers of the general physical dimensions,
    which are L -- Length, M -- Mass, T -- Time, C -- Electrical Charge
*/
class phys_dim {public : double L, M, T, C; 

  phys_dim :: phys_dim() { L = M = T = C = 0.0;}

  phys_dim::phys_dim(double L, double M, double T, double C){
    this->L=L; this->M=M; this->T=T; this->C=C;};

  phys_dim operator = (phys_dim d) {
    this->L=d.L; this->M=d.M; this->T=d.T; this->C=d.C;
    return(*this);}

  phys_dim operator = (double d) {
    this->L=0.0; this->M=0.0; this->T=0.0; this->C=0.0;
    return(*this);};

};

bool operator ==(phys_dim, phys_dim);
bool operator ==(double, phys_dim);
bool operator ==(phys_dim, double);

bool operator !=(phys_dim, phys_dim);
bool operator !=(double, phys_dim);
bool operator !=(phys_dim, double);

phys_dim  operator +(phys_dim, phys_dim);
phys_dim  operator -(phys_dim, phys_dim);
phys_dim  operator *(phys_dim, phys_dim);
phys_dim  operator /(phys_dim, phys_dim);

phys_dim  operator +(double, phys_dim);
phys_dim  operator -(double, phys_dim);
phys_dim  operator *(double, phys_dim);
phys_dim  operator /(double, phys_dim);

phys_dim  operator +(phys_dim, double);
phys_dim  operator -(phys_dim, double);
phys_dim  operator *(phys_dim, double);
phys_dim  operator /(phys_dim, double);

phys_dim  operator ^(phys_dim, double);

void print(phys_dim);
void write(phys_dim);

const phys_dim pd_dimensionless		= phys_dim(0,0,0,0);
const phys_dim pd_length		= phys_dim(1,0,0,0);
const phys_dim pd_mass			= phys_dim(0,1,0,0);
const phys_dim pd_time			= phys_dim(0,0,1,0);
const phys_dim pd_charge		= phys_dim(0,0,0,1);
const phys_dim pd_square		= pd_length^2;
const phys_dim pd_volume		= pd_length^3;
const phys_dim pd_frequency		= 1 / pd_time;
const phys_dim pd_velocity		= pd_length   / pd_time;
const phys_dim pd_c			= pd_velocity;
const phys_dim pd_acceleration		= pd_velocity / pd_time;
const phys_dim pd_force			= pd_mass   * pd_acceleration;
const phys_dim pd_energy		= pd_force  * pd_length;
const phys_dim pd_power			= pd_energy / pd_time;
const phys_dim pd_action		= pd_energy * pd_time;
const phys_dim pd_hbar			= pd_action;
const phys_dim pd_energy_flux		= pd_energy / pd_square / pd_time;
const phys_dim pd_photon_flux		= 1 / pd_square / pd_time;
const phys_dim pd_current		= pd_charge / pd_time;
const phys_dim pd_ionisation_rate	= 1 / pd_time;


const phys_dim pd_2pcs         = (pd_length^4) * pd_time; 
/*!2-photon cross section */
const phys_dim pd_2pwcs        = (pd_length^4) / pd_power; 
/*!2-photon weighted cross section */


/*! 
    The class phys_units represents a system of units. 
    It contains the ratios of this system to SI system of units,
    which are l_r -- length, m_r -- mass, t_r -- time, c_r -- charge
*/
class phys_units {public : double l, m, t, c; 

  phys_units :: phys_units() { l = m = t = c = 1.0;}

  phys_units :: phys_units(double l, double m, double t, double c){
    this->l=l; this->m=m; this->t=t; this->c=c;};

  phys_units operator = (phys_units d) {
    this->l=d.l; this->m=d.m; this->t=d.t; this->c=d.c; return(*this);}
};

//
//
//
bool operator ==(phys_units a, phys_units b);

bool operator !=(phys_units a, phys_units b);

/*!
   Usual systems of units SI, cgs, natural and atomic (Hartree units)
*/
const phys_units  pu_SI     = phys_units(1, 1, 1, 1);
//
const phys_units  pu_cgs    = phys_units(
                       1.0e-2, 1.0e-3, 1.0, 1.0e-1/greens_constant_SI_c);
const phys_units  pu_cgse   = pu_cgs;
//
// ?
//
const phys_units  pu_cgsm   = pu_cgs;

const phys_units  pu_atomic  = phys_units(
                       greens_constant_SI_a0, greens_constant_SI_me, 
                       greens_constant_SI_t0, greens_constant_SI_e0);

const phys_units pu_Hartree  = pu_atomic;

const phys_units pu_natural  = phys_units(
  greens_constant_SI_hbar/(greens_constant_SI_c*greens_constant_SI_me), 
  greens_constant_SI_me, 
  greens_constant_SI_hbar/(pow(greens_constant_SI_c, 2.0)*greens_constant_SI_me),
  greens_constant_SI_e/sqrt(greens_constant_alpha));

void print(phys_units);

/*! 
    The class phys_value represents a physical value  (1 m, 0.4 cm, 50 km/hour)
    It contains the ratios of this system to SI system of units,
    which are provided by a member                   phys_units units;  and
    a physical dimension which provided by a member  phys_dim dim;
     
*/
class phys_value {public : phys_dim   dim;
                  public : phys_units units;
                  public : double v;
      phys_value :: phys_value() {v=0.0; dim = pd_dimensionless; 
                                  units = pu_SI;};

      phys_value :: phys_value(double val) {
        v=val; dim = pd_dimensionless; units = pu_SI;};

      phys_value :: phys_value(double rv, phys_dim pd) {
        v=rv; dim = pd; units = pu_SI;};

      phys_value :: phys_value(double rv, phys_dim pd, phys_units pu) {
        v=rv; dim = pd; units = pu;};

      phys_value operator = (phys_value d) {
        this->v=d.v; this->dim=d.dim; this->units=d.units; return(*this);}
				  
};
//
//
//
bool operator ==(phys_dim, phys_dim);
bool operator ==(double, phys_dim);
bool operator ==(phys_dim, double);
//
//
//
phys_value operator + (phys_value a, phys_value b);
phys_value operator + (double a, phys_value b);
phys_value operator + (phys_value a, double b);

phys_value operator - (phys_value a, phys_value b);
phys_value operator - (double a, phys_value b);
phys_value operator - (phys_value a, double b);

phys_value operator * (phys_value a, phys_value b);
phys_value operator * (double a, phys_value b);
phys_value operator * (phys_value a, double b);

phys_value operator / (phys_value a, phys_value b);
phys_value operator / (double a, phys_value b);
phys_value operator / (phys_value a, double b);

phys_value operator ^ (phys_value a, double b);

void print(phys_value);
void print(phys_value a, phys_units u);
void print(phys_value a, phys_value b);

phys_value convert(phys_value a, phys_units u);
phys_value convert(phys_value a, phys_value b);
phys_value convert_photon_energy(phys_value, phys_value);
phys_value convert_photon_intensity(phys_value I_i, phys_value E_p, 
                                    phys_value I_o);
//
//
//
const phys_value pv_meter        = phys_value(1.0, pd_length, pu_SI);
const phys_value pv_m            = pv_meter;
const phys_value pv_mm           = 1.0e-3  * pv_meter;
const phys_value pv_mkm          = 1.0e-6  * pv_meter;
const phys_value pv_nm           = 1.0e-9  * pv_meter;
const phys_value pv_pm           = 1.0e-12 * pv_meter;
const phys_value pv_km           = 1000.0  * pv_meter;

const phys_value pv_SI_g         = 1.0e-3 * phys_value(1.0, pd_mass, pu_SI);
const phys_value pv_SI_kg        = 1.0 * phys_value(1.0, pd_mass, pu_SI);
const phys_value pv_kg           = pv_SI_kg;

const phys_value pv_SI_s         = phys_value(1.0, pd_time, pu_SI);
const phys_value pv_s            = pv_SI_s;

const phys_value pv_m2           = phys_value(1.0, pd_square, pu_SI);
const phys_value pv_m3           = phys_value(1.0, pd_volume, pu_SI);
const phys_value pv_J            = phys_value(1.0, pd_energy, pu_SI);
const phys_value pv_N            = phys_value(1.0, pd_force, pu_SI);
const phys_value pv_W            = phys_value(1.0, pd_power, pu_SI);
const phys_value pv_C            = phys_value(1.0, pd_charge, pu_SI);
const phys_value pv_J_s          = phys_value(1.0, pd_action, pu_SI);
const phys_value pv_A            = phys_value(1.0, pd_current, pu_SI);
const phys_value pv_m_by_s       = phys_value(1.0, pd_velocity, pu_SI);
const phys_value pv_m_by_s2      = phys_value(1.0, pd_acceleration, pu_SI);
const phys_value pv_1_by_s       = phys_value(1.0, pd_ionisation_rate, pu_SI);
const phys_value pv_Hz           = phys_value(1.0, pd_frequency, pu_SI);

const phys_value pv_cm           = phys_value(1.0, pd_length, pu_cgs);

const phys_value pv_cgs_s        = phys_value(1.0, pd_time, pu_cgs);
const phys_value pv_cm_by_s      = phys_value(1.0, pd_velocity, pu_cgs);
const phys_value pv_cgs_ms       = 1.0e-3 * pv_cgs_s;
const phys_value pv_cgs_g        = phys_value(1.0, pd_mass, pu_cgs);
const phys_value pv_g            = pv_cgs_g;
const phys_value pv_erg          = phys_value(1.0, pd_energy, pu_cgs);
const phys_value pv_dyne         = phys_value(1.0, pd_force, pu_cgs);

const phys_value pv_W_by_m2       = pv_W/(pv_m^2);
const phys_value pv_W_by_cm2      = pv_W/(pv_cm^2);
const phys_value pv_1_by_cm2_by_s = 1/(pv_cm^2)/pv_s;
const phys_value pv_J_by_m2_by_s  = pv_J/(pv_m^2)/pv_s;
const phys_value pv_J_by_cm2_by_s  = pv_J/(pv_cm^2)/pv_s;
const phys_value pv_erg_by_m2_by_s  = pv_erg/(pv_m^2)/pv_s;
const phys_value pv_erg_by_cm2_by_s = pv_erg/(pv_cm^2)/pv_s;

const phys_value pv_SI_c         = 299792458.0 * pv_m_by_s;
const phys_value pv_c            = pv_SI_c;
const phys_value pv_cgs_c        = 29979245800.0 * pv_cm_by_s;
const phys_value pv_atomic_c     = 1.0/greens_constant_alpha*
                                 phys_value(1.0, pd_velocity, pu_atomic);

const phys_value pv_SI_hbar      = phys_value(greens_constant_SI_hbar, 
                                              pd_action, pu_SI);
const phys_value pv_cgs_hbar     = phys_value(greens_constant_cgs_hbar, 
                                              pd_action, pu_cgs);
const phys_value pv_atomic_hbar  = phys_value(greens_constant_au_hbar, 
                                              pd_action, pu_atomic);
					      
const phys_value pv_SI_h         = phys_value(greens_constant_SI_h, 
                                              pd_action, pu_SI);
const phys_value pv_h            = pv_SI_h;

const phys_value pv_Angstrem     = 1.0e-10 * pv_meter;

const phys_value pv_SI_eV        = phys_value(greens_constant_SI_e0,
                                              pd_energy, pu_SI);
const phys_value pv_eV           = pv_SI_eV;
const phys_value pv_meV          = 1.0e-3 * pv_SI_eV;
const phys_value pv_keV          = 1.0e+3 * pv_SI_eV;
const phys_value pv_MeV          = 1.0e+6 * pv_SI_eV;
const phys_value pv_GeV          = 1.0e+9 * pv_SI_eV;
const phys_value pv_TeV          = 1.0e+12* pv_SI_eV;


const phys_value pv_aeu          = phys_value(1.0, pd_energy, pu_atomic);
const phys_value pv_Hartree      = pv_aeu;
const phys_value pv_Rydberg      = 0.5 * pv_eV;
const phys_value pv_Kayser       = 0.5 * pv_eV/ 109737.31534;

#endif
