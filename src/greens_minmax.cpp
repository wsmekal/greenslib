#include "greens_minmax.hpp"

double min(double x, double y) {if (x>y) {return(y);} else return(x);}
double max(double x, double y) {if (x<y) {return(y);} else return(x);}

double min(double a, double b, double c) {return(min(min(a,b),c));}
double max(double a, double b, double c) {return(max(max(a,b),c));}

double min(double a, double b, double c, double d) 
  {return(min(min(a,b),min(c,d)));}
double max(double a, double b, double c, double d) 
  {return(max(max(a,b),max(c,d)));}

int min(int x, int y) {if (x>y) {return(y);} else return(x);}
int max(int x, int y) {if (x<y) {return(y);} else return(x);}
int min(int x, int y, int z) {return(min(min(x,y),z));}
int max(int x, int y, int z) {return(max(max(x,y),z));}
int min(int a, int b, int c, int d) {return(min(min(a,b),min(c,d)));}
int max(int a, int b, int c, int d) {return(max(max(a,b),max(c,d)));}

