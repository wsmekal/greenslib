#ifndef __DVECTOR
#define __DVECTOR

#include <iostream>
#include <stdlib.h>

#include "greens_dcomplex.hpp"
#include "greens_service.hpp"

#undef VECTOR 
#undef REAL
#undef PTRREAL
#undef COMPLEX

#define VECTOR  dvector
#define REAL    double
#define PTRREAL double *
#define COMPLEX dcomplex

#include "vector_template.hpp"

#endif 
