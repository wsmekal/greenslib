
 class COMPLEX  {
      public: REAL re,im;
      COMPLEX::COMPLEX(REAL a=0, REAL b=0){re=a; im=b;};
      COMPLEX operator = (COMPLEX a) {
                 this->re=a.re;
                 this->im=a.im;
                 return(*this);}};

//
// ------------ Operators ------------------------------
//
bool operator ==(COMPLEX, COMPLEX);
bool operator ==(REAL, COMPLEX);
bool operator ==(COMPLEX, REAL);

COMPLEX operator +(COMPLEX, COMPLEX);
COMPLEX operator +(REAL, COMPLEX);
COMPLEX operator +(COMPLEX, REAL);
COMPLEX operator -(COMPLEX, COMPLEX);
COMPLEX operator -(REAL, COMPLEX);
COMPLEX operator -(COMPLEX, REAL);
COMPLEX operator -(COMPLEX a);
COMPLEX operator *(COMPLEX a,COMPLEX b);
COMPLEX operator *(REAL a, COMPLEX b);
COMPLEX operator *(COMPLEX b, REAL a);
REAL abs(REAL a);
COMPLEX operator /(COMPLEX a,COMPLEX b);
COMPLEX operator /(REAL a, COMPLEX b);
COMPLEX operator /(COMPLEX a, REAL b);

//
//------------------ Functions ----------------------------
//
COMPLEX exp(COMPLEX a);
   REAL abs(COMPLEX a);

//
//
// 
    REAL Re(COMPLEX a);
    REAL Im(COMPLEX a);
    REAL argument(COMPLEX a);
 COMPLEX conjugate(COMPLEX a);
 COMPLEX polar(REAL r, REAL phi);
 COMPLEX cartesian(REAL x, REAL y);
//
//
//

COMPLEX cos(COMPLEX z);
COMPLEX sin(COMPLEX z);

COMPLEX tan(COMPLEX z);

COMPLEX csc(COMPLEX z);
COMPLEX sec(COMPLEX z);
COMPLEX cot(COMPLEX z);

COMPLEX log(COMPLEX z);
COMPLEX Clog(REAL z);
COMPLEX log(COMPLEX z);
COMPLEX ln (COMPLEX z);

REAL sign(REAL z);

COMPLEX sqrt(COMPLEX z);

COMPLEX Csqrt(REAL z);

COMPLEX pow(COMPLEX z, COMPLEX a);
COMPLEX pow(COMPLEX z, REAL a);
COMPLEX pow(REAL z, COMPLEX a);
COMPLEX pow(COMPLEX z, int a);
COMPLEX pow(int z, COMPLEX a);
COMPLEX pow(COMPLEX z, long a);
COMPLEX pow(long z, COMPLEX a);

COMPLEX Cpow(REAL z, REAL a);
COMPLEX Cpow(REAL z, COMPLEX a);
COMPLEX Cpow(COMPLEX z, REAL a);
COMPLEX Cpow(COMPLEX z, COMPLEX a);

COMPLEX lnpow(COMPLEX z, COMPLEX a);
COMPLEX lnpow(REAL z, COMPLEX a);
COMPLEX lnpow(COMPLEX z, REAL a);
COMPLEX Clnpow(REAL z, REAL a);

COMPLEX operator ^(COMPLEX z, long p);
COMPLEX operator ^(COMPLEX z, COMPLEX a);
COMPLEX operator ^(COMPLEX z, REAL a);
COMPLEX operator ^(REAL z, COMPLEX a);

COMPLEX acos(COMPLEX z);
COMPLEX asin(COMPLEX z);
COMPLEX atan(COMPLEX z);

COMPLEX cosh(COMPLEX z);
COMPLEX sinh(COMPLEX z);
COMPLEX tanh(COMPLEX z);

 
