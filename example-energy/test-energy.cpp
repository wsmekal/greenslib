#include "greens.h"

int main(void){
double E_nonrel, E_rel;      // energies

print("#Test greens_energy() ");

for(double Z=1.0; Z<93.0; Z=Z+10.0) {
greens_set_nuclear_charge(Z); // set nuclear charge 
  for(int n=1; n<10; n++){
    E_nonrel = -greens_energy(n);        // nonrelativistic energy
    E_rel    = -greens_energy(n, -1);    // relativistic    energy
     
    printf("%E %E %E \n", Z, E_nonrel, E_rel); }}
return 0;}
